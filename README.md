# About this project: TRex V2.x

The [TRex project](https://www.trex.informatik.uni-goettingen.de/trac) is an [Eclipse-based](https://eclipseide.org/) open-source tool for editing, analysing and refactoring [TTCN-3](http://www.ttcn-3.org/) source code. The TRex name stands for **T**TCN-3 **Re**factoring and Metri**cs**. TTCN-3 is a language used for testing distributed systems.

TRex is officially hosted using SVN version control. This repository is a fork to be used for the migrating efforts of missing TRex features from version 0.x to 2.x by the 2023 class of Software maintenance students of the University of Iceland.

# Installation

The following installation instructions are derived from and parts copied from the texts in assignments 1-2 & 14-16 of the Software maintenance course, authored by Helmut Neukirchen (contact details in special chapter). If you run into problems following these steps, take a look at the assignments where you might find solutions to those problems.

The first step, or a prerequisite, is to have the **Eclipse IDE** installed along with its Plug-in Development Environment (PDE):
* If you **don't have Eclipse** installed, then follow this link [here](https://www.eclipse.org/downloads/packages/installer) and follow the instructions with one thing to note: in the step where you select the package to install take care to select the *Eclipse IDE for Eclipse committers* package. You might have to trust unsigned content that the installer is downloading.
* If you **do have Eclipse** installed but not the PDE (Plug-in Development Environment) then start Eclipse and install PDE via *Help -> Install New Software...* and there *Work with -> -All Available Sites-* and install "Eclipse Java Development Tools" (you might already have JDT) and take care to install "Eclipse Plug-in Development Environment".

Next, a final prerequisite, you need to install the **Xtext** Eclipse plugin and the **Xtext Deprecated Language Generator**: 
1. In your web browser open https://www.eclipse.org/Xtext/ and click on blue *Download*-button and on the next page right click on the blue *Releases*-button and copy link address. 
2. Then in Eclipse click on *Help -> Install New Software... -> Add...* and there you use the copied URL as *Location* and as *Name* you can for example use "Xtext update site" *-> Add*. It might take a few minutes to fetch children. Abort and redo if it takes longer (using *Work with*: the Xtext update site). Take care to have enabled *Group items by category*, *Show only the latest versions of available software* and *Contact all update sites during install to find required software*. 
3. Then, from the category *Xtext*, checkmark *Xtext Complete SDK* and *Xtext Deprecated Language Generator* then click *Next -> Next* on the installation details page and *Finish* to accept the license. It might take several minutes to install this and you should see the progress at the bottom right corner of Eclipse (it might seem stuck at 48% but isn't necessarily). 
4. Finally *Restart Now* and Xtext and the Xtext deprecated language generator have been installed.

Next, **clone this repository** with HTTPS (see blue *Clone*-button on repository home page for the URL) in Eclipse using *File -> Import... -> Git -> Projects from Git -> Next -> Clone URI -> Next ->* Select defaults. Take care to not install Marketplace solutions if Eclipse offers.

You should now see multiple **red error markers** start to appear as what you imported into Eclipse attempts to build. We can fix the multiple red error markers by making **Xtext generate missing code**: In the Package explorer, in project *de.ugoe.cs.swe.TTCN3* navigate to file */src/de/ugoe/cs/swe/GenerateTTCN3.mwe2* and apply *Run as -> MWE2 Workflow*. (It will complain about errors in the project: but we fix exactly these errors by running this.) This process takes several minutes and consumes a lot of RAM and is finished when *"Done."* is printed on the console view. Now the red error markers should disappear after building has finished.

You should now have installed this project.

# Configuration and running

After installation you should be able to run TRex 2.x by selecting in the Package Explorer the project *de.ugoe.cs.swe.TTCN3* and then the context menu *Run As -> Run Configurations...* Under *Eclipse Application* you should find amongst other things the pre-defined *TRex Complete* configuration. Right-click and duplicate the *TRex Complete* configuration and create your own custom configuration (with a name of your choosing, e.g. "My TRex 2") where you'll need to do two changes:
1. In the *Plug-ins* tab change *Launch with:* to *plug-ins selected below*. Click on *Deselect All* and then select only plug-ins from your workspace. Unselect *Include optional dependencies when computing required Plug-ins*, then *Select Required* and then *Validate Plug-ins*. After that you might still get warnings, enable the relevant plug-ins manually until only the com.google.inject -> missing constraint *sun.misc* is remaining, ignore that one and click *Continue*.
2. In the *Arguments* tab under *VM arguments* delete "-XX:MaxPermSize=1g".

Click on *Apply* to apply these changes to your run configuration and click on *Run* to run TRex 2.x which should open in a second Eclipse window.

# Contact

This repository is owned by Helmut Neukirchen. His contact details [here](https://www.hi.is/staff/helmut) and GitLab user here: @helmut.neukirchen

# License

The TRex source code is published using the [Eclipse Public License (EPL) v1.0 open-source license]( https://www.eclipse.org/org/documents/epl-v10.php).

# Wiki

For further reading visit our [wiki](https://gitlab.com/helmut.neukirchen/trexv2.x_2023/-/wikis/home).
