/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-05 20:19:01 +0100 (Sat, 05 Mar 2016) $
// $Rev: 15516 $
/******************************************************************************/

module IMS_Procedures_Common {
  import from CommonDefs all;
  import from CommonIP all;
  import from IMS_Component all;
  import from IMS_ASP_TypeDefs all;
  import from IMS_ASP_Templates all;
  import from IP_ASP_TypeDefs all;
  import from IMS_IP_Config all;
  import from IMS_CommonDefs all;
  import from IMS_CommonFunctions all;
  import from IMS_CommonParameters all;
  import from IMS_CommonTemplates all;
  import from IMS_SIP_Templates all;
  import from LibSip_Common all;
  import from LibSip_SIPTypesAndValues all;
  
  type enumerated IMS_TCP_CloseHandling_Type { justWait, waitAndClose, dontWait };  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
                                                                                       @sic R5s140801: dontWait sic@ */

  /*
   * @desc      common function to handle closing down TCP connections used for SIP signalling
   * @param     p_Handling
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_TCP_CloseHandling(IMS_TCP_CloseHandling_Type p_Handling) runs on IMS_PTC
  {
    f_Delay(tsc_IMS_WaitForTcpClose);

    select (p_Handling) {
      case (waitAndClose) {
        f_IMS_CloseTCP();
      }
      case else {
        // do nothing
      }
    }
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      Release of IMS registration and security plus resetting of IMS information
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_GlobalRelease() runs on IMS_PTC
  {
    f_IMS_RegisterInfo_Release();  /* release registration information at IPCAN */
    f_IMS_PTC_Reset();             /* reset ImsInfo and release security
                                      @sic R5s150031: common function f_IMS_PTC_Reset sic@ */
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      check whether public user id matches with any public user id in the given list
   * @param     p_PublicUserIdentityList
   * @param     p_SipUrl
   * @return    boolean
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_PublicUserId_Match(CharStringList_Type p_PublicUserIdentityList,
                                    SipUrl p_SipUrl) return boolean
  {
    var integer i;
    
    for (i:=0; i < lengthof(p_PublicUserIdentityList); i:=i+1) {
      if (f_SIP_MatchURI(p_PublicUserIdentityList[i], p_SipUrl)) {
        return true;
      }
    }
    return false;
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      return server port of the network side depending on whether it is protected or not
   * @return    integer
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_PTC_GetPort_ps() runs on IMS_PTC return integer
  { /* @sic R5s130756 additional changes: p_IsProtected removed (f_IMS_PTC_Security_IsStarted used instead) sic@ */
    var IMS_ProtectedPorts_Type v_Protected;
    var integer v_Port_ps := tsc_IMS_PortNumber_5060;

    if (f_IMS_PTC_Security_IsStarted()) {
      v_Protected := f_IMS_PTC_Security_GetProtectedPorts();
      v_Port_ps := v_Protected.Port_ps;       // @sic R5s120907 change 2 sic@
    }
    return v_Port_ps;
  }

  /*
   * @desc      return server port of the UE side depending on whether it is protected or not
   * @return    integer
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_PTC_GetPort_us() runs on IMS_PTC return integer
  {
    var IMS_ProtectedPorts_Type v_Protected;
    var integer v_Port_us := tsc_IMS_PortNumber_5060;

    if (f_IMS_PTC_Security_IsStarted()) {
      v_Protected := f_IMS_PTC_Security_GetProtectedPorts();
      v_Port_us := v_Protected.Port_us;
    }
    return v_Port_us;
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      Usage of tags is described in RFC 3261 clause 19.3:
   *            when there has been a to-tag in a request the same to-tag is used in the corresponding response;
   *            when there is no to-tag a new to-tag and the response is for a dialog the stored to-tag is used
   *            when there still no to-tag a new to-tag is generated for the reponse
   * @param     p_RequestMessageHeader
   * @return    charstring
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_Response_ToTagTX(template (value) MessageHeader p_RequestMessageHeader) runs on IMS_PTC return charstring
  { /* @sic R5-131897, R5-132063 - new generic function sic@ */
    /* @sic R5s140017 MCC160 implementation: f_IMS_PTC_ImsInfo_DialogGetToTag, f_IMS_PTC_ImsInfo_DialogSetToTag replaced by f_IMS_PTC_ImsInfo_DialogGetLocalTag, f_IMS_PTC_ImsInfo_DialogSetLocalTag sic@ */
    var template (omit) charstring v_ToTag := f_MessageHeader_GetToTag(p_RequestMessageHeader);
    var charstring v_Method;

    if (not isvalue(v_ToTag)) {
      v_Method := valueof(p_RequestMessageHeader.cSeq.method);
      if (v_Method == "INVITE") {   /* @sic R5s130497 change 1 - MCC160 implementation sic@ */
        // => response belongs to a dialog
        v_ToTag := f_IMS_PTC_ImsInfo_DialogGetLocalTag();  // check if there is already a tag generated for this dialog (essential e.g. in case of call-forwarding when the tag changes; see testcase 15.8)
        if (not isvalue(v_ToTag)) {
          v_ToTag := f_IMS_GenerateTag(v_Method);
          f_IMS_PTC_ImsInfo_DialogSetLocalTag(valueof(v_ToTag));
        }
      } else {
        v_ToTag := f_IMS_GenerateTag(v_Method);
      }
    }
    return valueof(v_ToTag);
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      Generate To header to be used in a response: in general the To header consists of the address and the to-tag
   * @param     p_RequestMessageHeader
   * @param     p_ToTag             (default value: omit)
   * @return    template (value) To
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_Response_ToHeaderTX(template (value) MessageHeader p_RequestMessageHeader,
                                     template (omit) charstring p_ToTag := omit) runs on IMS_PTC return template (value) To
  { /* @sic R5-153986: parameter p_ToTag to allow different ToTags for different dialogs created by subscription to even packages sic@ */
    var SipUrl v_SipUrl := f_Addr_Union_GetSipUrl(p_RequestMessageHeader.toField.addressField);
    var charstring v_ToTag;

    if (ispresent(p_ToTag)) {
      v_ToTag := valueof(p_ToTag);
    } else {
      v_ToTag := f_IMS_Response_ToTagTX(p_RequestMessageHeader);
    }

    return cs_ToWithTag(v_SipUrl, v_ToTag);
  }

  /*
   * @desc      generate template for To header depending on whether the previous request has had a to-tog or not
   * @param     p_RequestMessageHeader
   * @param     p_Is100Response     (default value: false)
   * @return    template (present) To
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_Response_ToHeaderRX(template (value) MessageHeader p_RequestMessageHeader,
                                     boolean p_Is100Response := false) return template (present) To
  { /* @sic R5s130685 change 1: new parameter p_Is100Response sic@ */
    var template (omit) charstring v_ToTagFromRequest := f_MessageHeader_GetToTag(p_RequestMessageHeader);
    var template (value) SipUrl v_SipUrl := f_Addr_Union_GetSipUrl(p_RequestMessageHeader.toField.addressField);   // @sic R5s130453 change 1 sic@
    var template (present) To v_To;

    if (ispresent(v_ToTagFromRequest)) {
      v_To := cr_ToWithTag(v_SipUrl, valueof(v_ToTagFromRequest));      /* @sic R5-132063  sic@ */
    }
    else if (not p_Is100Response) {
      v_To := cr_ToWithTag(v_SipUrl);
    }
    else {
      v_To := cr_ToDef(v_SipUrl);                                       /* @sic R5s130685 change 1 sic@ */
    }
    return v_To;
  }

  /*
   * @desc      generate template for From header (the from-tag has to be the same as in the corresponding request)
   * @param     p_RequestMessageHeader
   * @return    template (present) From
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_Response_FromHeaderRX(template (value) MessageHeader p_RequestMessageHeader) return template (present) From
  {
    var charstring v_FromTagFromRequest := f_MessageHeader_GetFromTag(p_RequestMessageHeader);
    var template (value) SipUrl v_SipUrl := f_Addr_Union_GetSipUrl(p_RequestMessageHeader.fromField.addressField);

    return cr_FromWithTag(v_SipUrl, v_FromTagFromRequest);
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      Route set acc. A.2.3 and A.3.1
   * @param     p_CallType          (default value: NORMAL_CALL)
   * @return    template (value) RouteBody_List
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT)
   */
  function f_IMS_RouteSet_MO_Call_TX(IMS_CallType_Type p_CallType := NORMAL_CALL) runs on IMS_PTC return template (value) RouteBody_List
  { /* @sic R5s150692 change 5: parameter p_CallType sic@ */
    var template (omit) GenericParam v_SigCompParam := f_IMS_PTC_ImsInfo_GetSigCompParam(); // Compression parameter to be included in the last route parameter (see 34.229-1 cl. 13.2.4)
    var template (omit) integer v_Port_ps := omit;
    var template (value) RouteBody_List v_RouteBodyList;
    var template (value) RouteBody v_RouteBodyPCSCF;

    if (f_IMS_PTC_Security_IsStarted()) {   // A1
      v_Port_ps := f_IMS_PTC_GetPort_ps();
    } // else: in case of GIBA or emergency call without registration SS server port in unprotected (and therefore left out)

    v_RouteBodyPCSCF := f_RouteBodyWithParamsTX(cs_SipUri_HostPort_lr(f_IMS_PTC_Pcscf_Get(), v_Port_ps), v_SigCompParam);

    select (p_CallType) {
      case (NORMAL_CALL) {
        v_RouteBodyList := {
          cs_RouteBody(f_SIP_BuildSipUri_lr_TX("sip:pcscf.other.com")),
          cs_RouteBody(f_SIP_BuildSipUri_lr_TX("sip:scscf.other.com")),
          cs_RouteBody(f_SIP_BuildSipUri_lr_TX("sip:orig@" & px_IMS_Scscf)),
          v_RouteBodyPCSCF
        };
      }
      case (CONFERENCE_CALL) {                          /* @sic R5-155179 sic@ */
        v_RouteBodyList := {
          cs_RouteBody(f_SIP_BuildSipUri_lr_TX("sip:orig@" & px_IMS_Scscf)),  /* @sic R5-160796 sic@ */
          v_RouteBodyPCSCF
        };
      }
      case (REGULAR_EMERGENCY, ASSERTED_EMERGENCY) {    /* @sic R5s150692 change 5, R5-153472 sic@ */
        v_RouteBodyList := {
          cs_RouteBody(f_SIP_BuildSipUri_lr_TX("sip:orig@" & tsc_IMS_Ecscf)),
          v_RouteBodyPCSCF
        };
      }
      case else {                                       /* @sic R5-155179 sic@ */
        FatalError(__FILE__, __LINE__, "");
      }
    }
    return v_RouteBodyList;
  }

  /*
   * @desc      Route set acc. A.2.9
   * @return    template (value) RouteBody_List
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT)
   */
  function f_IMS_RouteSet_MT_Call_TX() runs on IMS_PTC return template (value) RouteBody_List
  {
    var template (omit) GenericParam v_SigCompParam := f_IMS_PTC_ImsInfo_GetSigCompParam(); // Compression parameter to be included in the last route parameter (see 34.229-1 cl. 13.2.4)
    var integer v_Port_ps := f_IMS_PTC_GetPort_ps();
    var template (value) RouteBody_List v_RouteBodyList;

    v_RouteBodyList := {
      f_RouteBodyWithParamsTX(cs_SipUri_HostPort_lr(f_IMS_PTC_Pcscf_Get(), v_Port_ps), v_SigCompParam),
      cs_RouteBody(f_SIP_BuildSipUri_lr_TX("sip:term@scscf1.3gpp.org")),
      cs_RouteBody(f_SIP_BuildSipUri_lr_TX("sip:orig@scscf2.3gpp.org")),
      cs_RouteBody(f_SIP_BuildSipUri_lr_TX("sip:pcscf2.3gpp.org"))
    };

    return v_RouteBodyList;
  }

  //============================================================================
  // Check functions
  //----------------------------------------------------------------------------

  type enumerated CheckToTag_Type {noToTag, dialogToTag};  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */

  /*
   * @desc      check the To header e.g. whether it has unexpected tag
   * @param     p_MessageHeader
   * @param     p_CheckType
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_MessageHeader_CheckTo(MessageHeader p_MessageHeader,
                                       CheckToTag_Type p_CheckType) runs on IMS_PTC
  {
    var SemicolonParam_List v_ToParams := {};
    var template (omit) charstring v_ToTagDialog;
    var template (omit) charstring v_ToTagReceived;

    if (ispresent(p_MessageHeader.toField.toParams)) {
      v_ToParams := p_MessageHeader.toField.toParams;
    }
    select (p_CheckType) {
      case (noToTag) {
        if (match(v_ToParams, cr_SemicolonParam_List_WithAnyTag)) {
          f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "Invalid params in 'To'");
        }
      }
      case (dialogToTag) {
        v_ToTagDialog := f_IMS_PTC_ImsInfo_DialogGetRemoteTag();       /* @sic R5s140017 MCC160 implementation: use remote tag sic@ */
        v_ToTagReceived := f_MessageHeader_GetToTag(p_MessageHeader);
        if (not ispresent(v_ToTagReceived)) {
          f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "Missing to-tag");
        }
        if (ispresent(v_ToTagDialog) and (valueof(v_ToTagDialog) != valueof(v_ToTagReceived))) {
          f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "Invalid to-tag");
        }
      }
    }
  }

  /*
   * @desc      Check Via header
   * @param     p_MessageHeader
   * @param     p_Protocol
   * @param     p_AllowFQDN         (default value: true)
   * @param     p_CheckRport        (default value: false)
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_MessageHeader_Request_CheckVia(MessageHeader p_MessageHeader,
                                                InternetProtocol_Type p_Protocol,
                                                boolean p_AllowFQDN := true,
                                                boolean p_CheckRport := false) runs on IMS_PTC
  { /* p_AllowFQDN  ..  shall be set to false e.g. for condition A6 of A.2.1 */
    var template integer v_Port := *;
    var ViaBody v_ViaBody := p_MessageHeader.via.viaBody[0];
    var charstring v_ProtocolString := f_IMS_ConvertTransportProtocolToString(p_Protocol);

    if (not f_IMS_PTC_CheckHostPortForIPAddrOrFQDN(v_ViaBody.sentBy, v_Port, p_AllowFQDN)) {
      f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "Invalid host and/or port in 'via'");
    }
    if (f_StringToLower(v_ProtocolString) != f_StringToLower(v_ViaBody.sentProtocol.transport)) {
      f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "Invalid transport protocol in 'via'");
    }
    if (p_CheckRport and (p_Protocol != tcp)) {  // @sic R5-133358 rport is optional for tcp sic@
      if (not match(v_ViaBody.viaParams, cr_SemicolonParam_List_OneSpecificParamNoValue("rport"))) {
        f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "'rport' is missing in 'via'");
      }
    }
  }

  /*
   * @desc      Check whether Via header of a response sent by the UE matches Via header sent to the UE in the corresponding request
   *            (taking into consideration that the format of IP address representation may change)
   * @param     p_RequestHeader
   * @param     p_ResponseHeader
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_MessageHeader_Response_CheckVia(MessageHeader p_RequestHeader,
                                                 MessageHeader p_ResponseHeader) runs on IMS_PTC
  {
    var integer v_LengthOfViaBody_Request := lengthof(p_RequestHeader.via.viaBody);
    var integer v_LengthOfViaBody_Response := lengthof(p_ResponseHeader.via.viaBody);
    var boolean v_Mismatch := false;
    var integer i;

    if (v_LengthOfViaBody_Request != v_LengthOfViaBody_Response) {
      v_Mismatch := true;
    } else {
      for (i:=0; i < v_LengthOfViaBody_Request; i:=i+1) {
        if (not f_IPAddresses_Compare(p_RequestHeader.via.viaBody[i].sentBy.host, p_ResponseHeader.via.viaBody[i].sentBy.host)) {
          v_Mismatch := true;
          break;
        }
      }
    }
    if (v_Mismatch) {
      f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "Mismatch in via header");
    }
  }

  /*
   * @desc      check whether the first entry in the route body matches the P-CSCF address
   * @param     p_RouteBodyList
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function fl_RouteBody_CheckPcscfAddr(RouteBody_List p_RouteBodyList) runs on IMS_PTC
  { /* @sic R5s150039: new function to deal with RecordRoute header as well as with Route header sic@ */
    var charstring v_PcscfAddr := p_RouteBodyList[0].nameAddr.addrSpec.components.sip.hostPort.host;
    
    if (not f_IMS_PTC_Pcscf_CheckAndSet(v_PcscfAddr)) {
      f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "Invalid host in 'route body'");
    }
  }

  /*
   * @desc      Check Route header of a given request sent by the UE:
   *            The first entry has to correspond to the IP address of the P-CSCF
   *            => in case of IPv6 the presentation of the address may differ from the address stored at the IMS PTC
   * @param     p_RequestHeader
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_MessageHeader_Request_CheckRoute(MessageHeader p_RequestHeader) runs on IMS_PTC
  {
    fl_RouteBody_CheckPcscfAddr(p_RequestHeader.route.routeBody);                  /* @sic R5s150039: fl_RouteBody_CheckPcscfAddr sic@ */
  }

  /*
   * @desc      Check RecordRoute header of a given request sent by the UE:
   *            The first entry has to correspond to the IP address of the P-CSCF
   *            => in case of IPv6 the presentation of the address may differ from the address stored at the IMS PTC
   * @param     p_RequestHeader
   * @status    APPROVED (IMS, LTE_A_IRAT, LTE_IRAT)
   */
  function f_IMS_MessageHeader_Response_CheckRecordRoute(MessageHeader p_RequestHeader) runs on IMS_PTC
  {
    fl_RouteBody_CheckPcscfAddr(p_RequestHeader.recordRoute.routeBody);            /* @sic R5s150039: fl_RouteBody_CheckPcscfAddr sic@ */
  }

  /*
   * @desc      Check Contact header
   * @param     p_MessageHeader
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_MessageHeader_CheckContactAddr(MessageHeader p_MessageHeader) runs on IMS_PTC
  { /* @sic R5-153760: p_AllowGRUU, p_AllowFQDN removed sic@ */
    var template integer v_Port := *;
    var charstring v_Method := p_MessageHeader.cSeq.method;
    var boolean v_GRUUsInSIP;
    var SipUrl v_SipUrl;

    if (not match(p_MessageHeader.contact, cr_Contact_Wildcard)) {      // contact may be wildcard for de-registration
      v_SipUrl := f_MessageHeader_GetContactSipUrl(p_MessageHeader);
      v_GRUUsInSIP := pc_IMS_GRUUsInSIP and (v_Method != "REGISTER");     /* @sic R5s150908 sic@ */
      if (not f_IMS_PTC_CheckSipUrlForIPAddrOrFQDNOrGRUU(v_SipUrl, v_Port, v_GRUUsInSIP)) {
        f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "Invalid SipUrl in 'contact'");
      }
    }
  }

  //============================================================================
  // Dialog handling
  //----------------------------------------------------------------------------
  /*
   * @desc      returns either next Cseq value of ? if there has been no request sent by the UE so far
   *            the respective CSeq at IMS PTC gets updated with the incremeted value accordingly
   * @param     p_CseqType          (default value: dialogRemote)
   * @param     p_DialogIndex       (default value: omit)
   * @return    template (present) integer
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT)
   */
  function f_IMS_RequestInDialog_CSeqValueRX(IMS_PTC_CseqType_Type p_CseqType := dialogRemote,
                                             template (omit) IMS_PTC_DialogIndex_Type p_DialogIndex := omit) runs on IMS_PTC return template (present) integer
  {
    var template (present) integer v_CSeqValue := ?;

    if (isvalue(f_IMS_PTC_ImsInfo_CseqGet(p_CseqType, p_DialogIndex))) {
      v_CSeqValue := f_IMS_PTC_ImsInfo_CseqIncr(p_CseqType, p_DialogIndex);
    }
    return v_CSeqValue;
  }

  /*
   * @desc      check whether the CSeq value of the received request matches the incremented value of the previous request (if any)
   *            the respective CSeq at IMS PTC gets updated with the incremeted value accordingly
   * @param     p_RequestMessageHeader
   * @param     p_CseqType          (default value: dialogRemote)
   * @param     p_DialogIndex       (default value: omit)
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT)
   */
  function f_IMS_RequestInDialog_IncrAndCheckCSeqValue(MessageHeader p_RequestMessageHeader,
                                                       IMS_PTC_CseqType_Type p_CseqType := dialogRemote,
                                                       template (omit) IMS_PTC_DialogIndex_Type p_DialogIndex := omit) runs on IMS_PTC
  {
    var template (present) integer v_CSeqValueExpected := f_IMS_RequestInDialog_CSeqValueRX(p_CseqType, p_DialogIndex);
    var integer v_CSeqValueReceived := p_RequestMessageHeader.cSeq.seqNumber;

    if (not match(v_CSeqValueReceived, v_CSeqValueExpected)) {
      f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "unexpected sequence number");
    }
  }

  /*
   * @desc      Build the From header for requests being received within a dialog (e.g. PRACK or BYE)
   * @param     p_DialogIndex       (default value: omit)
   * @return    template (present) From
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT)
   */
  function f_IMS_RequestInDialog_FromHeaderRX(template (omit) IMS_PTC_DialogIndex_Type p_DialogIndex := omit) runs on IMS_PTC return template (present) From
  { /* @sic R5s140017 MCC160 implementation sic@ */
    /* @sic R5s140121 change 2.1: "runs on" added sic@ */
    var charstring v_FromTag := valueof(f_IMS_PTC_ImsInfo_DialogGetRemoteTag(p_DialogIndex));     // will cause a runtime error when to-tag has not been set before
    var template (present) SipUrl v_SipUrl := f_IMS_PTC_ImsInfo_GetSipUriRX(dialogRemoteId, -, p_DialogIndex);  /* @sic R5s150835: p_DialogIndex sic@ */
    return cr_FromWithTag(v_SipUrl, v_FromTag);
  }

  /*
   * @desc      Build the To header for requests being received within a dialog (e.g. PRACK or BYE)
   * @param     p_DialogIndex       (default value: omit)
   * @return    template (present) To
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT)
   */
  function f_IMS_RequestInDialog_ToHeaderRX(template (omit) IMS_PTC_DialogIndex_Type p_DialogIndex := omit) runs on IMS_PTC return template (present) To
  { /* @sic R5s140017 MCC160 implementation sic@ */
    var charstring v_ToTag := valueof(f_IMS_PTC_ImsInfo_DialogGetLocalTag(p_DialogIndex));     /* will cause a runtime error when to-tag has not been set before
                                                                                                  NOTE: this runtime error is intended as something else is wrong */
    var template (present) SipUrl v_SipUrl := f_IMS_PTC_ImsInfo_GetSipUriRX(dialogLocalId, -, p_DialogIndex);   /* @sic R5s150382 change 1: p_DialogIndex sic@ */
    return cr_ToWithTag(v_SipUrl, v_ToTag);
  }

  /*
   * @desc      Build the From header for requests being sent within a dialog (e.g. PRACK or BYE)
   * @param     p_DialogIndex       (default value: omit)
   * @return    template (value) From
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_RequestInDialog_FromHeaderTX(template (omit) IMS_PTC_DialogIndex_Type p_DialogIndex := omit) runs on IMS_PTC return template (value) From
  { /* @sic R5s140017 MCC160 implementation sic@ */
    /* @sic R5s140121 change 4.1: "runs on" added sic@ */
    var charstring v_FromTag := valueof(f_IMS_PTC_ImsInfo_DialogGetLocalTag(p_DialogIndex));     // will cause a runtime error when to-tag has not been set before
    var template (value) SipUrl v_SipUrl := f_IMS_PTC_ImsInfo_GetSipUriTX(dialogLocalId, -, p_DialogIndex);   /* @sic R5s150835: p_DialogIndex sic@ */
    return cs_From(v_SipUrl, v_FromTag);
  }

  /*
   * @desc      Build the To header for requests being sent within a dialog (e.g. PRACK or BYE)
   * @param     p_DialogIndex       (default value: omit)
   * @return    template (value) To
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_RequestInDialog_ToHeaderTX(template (omit) IMS_PTC_DialogIndex_Type p_DialogIndex := omit) runs on IMS_PTC return template (value) To
  { /* @sic R5s140017 MCC160 implementation sic@ */
    var charstring v_ToTag := valueof(f_IMS_PTC_ImsInfo_DialogGetRemoteTag(p_DialogIndex));     // will cause a runtime error when to-tag has not been set before
    var template (value) SipUrl v_SipUrl := f_IMS_PTC_ImsInfo_GetSipUriTX(dialogRemoteId, -, p_DialogIndex);   /* @sic R5s150835: p_DialogIndex sic@ */
    return cs_ToWithTag(v_SipUrl, v_ToTag);
  }
  
  /*
   * @desc      check to-tag of received response and store value if necessary (mainly called after receiving 180 or 183 response);
   *            store coantact URI (-> GRUU)
   * @param     p_ResponseMessageHeader
   * @param     p_DialogIndex       (default value: omit)
   * @status    APPROVED (IMS, LTE_A_IRAT, LTE_IRAT)
   */
  function f_IMS_Dialog_CompleteCreationForMTCall(MessageHeader p_ResponseMessageHeader,
                                                  template (omit) IMS_PTC_DialogIndex_Type p_DialogIndex := omit) runs on IMS_PTC
  { /* @sic R5s140017 MCC160 implementation: f_IMS_PTC_ImsInfo_DialogGetToTag, f_IMS_PTC_ImsInfo_DialogSetToTag replaced by f_IMS_PTC_ImsInfo_DialogGetRemoteTag, f_IMS_PTC_ImsInfo_DialogSetRemoteTag sic@ */
    /* @sic R5s150908: f_IMS_Dialog_SetRemoteTag renamed to f_IMS_Dialog_CompleteCreationForMTCall; handling of contact URI sic@ */
    var template (omit) charstring v_DialogRemoteTag := f_IMS_PTC_ImsInfo_DialogGetRemoteTag(p_DialogIndex);
    var template (omit) charstring v_ReceivedToTag := f_MessageHeader_GetToTag(p_ResponseMessageHeader);
    var SipUrl v_ContactUrl := f_MessageHeader_GetContactSipUrl(p_ResponseMessageHeader);
    var charstring v_ReceivedToTagValue;
    
    if (ispresent(v_ReceivedToTag)) {
      v_ReceivedToTagValue := valueof(v_ReceivedToTag);
      if (ispresent(v_DialogRemoteTag)) {  // @sic R5s140173 sic@
        if (not match(v_ReceivedToTagValue, v_DialogRemoteTag)) {
          f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "invalid To-tag");
        }
      } else {
        f_IMS_PTC_ImsInfo_DialogSetRemoteTag(v_ReceivedToTagValue);
      }
    }
    f_IMS_PTC_ImsInfo_SetContactUrl(v_ContactUrl, dialog, p_DialogIndex);
  }

  //============================================================================
  // Generic Common Messages
  //----------------------------------------------------------------------------
  /*
   * @desc      200 OK for other requests than REGISTER or SUBSCRIBE (see TS 34.229, A.3.1)
   * @param     p_MessageHeader_Request
   * @param     p_ContentType       (default value: omit)
   * @param     p_PreconditionIndication   (default value: NoPreconditionIndication)
   * @return    template (present) MessageHeader
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_OtherResponse_200_MessageHeaderRX(template (value) MessageHeader p_MessageHeader_Request,
                                                   template ContentType p_ContentType := omit,
                                                   IMS_PreconditionIndication_Type p_PreconditionIndication := NoPreconditionIndication) runs on IMS_PTC return template (present) MessageHeader
  { /* NOTE: Message is sent by the UE => conditions A2, A4 and A5 are applicable;
     *       For A4 (GIBA) pAccessNetworkInfo is optional */
    /* @sic R5-130683 sic@ */
    /* @sic R5s130756 additional changes: p_IsGIBA removed sic@ */
    /* @sic R5-160790, R5-160903, R5-160793: p_PreconditionIndication sic@ !!!! on a long term p_PreconditionIndication shall be removed as the 200 OK should always contain a Require header when it contains SDP !!!! */
    var template (present) MessageHeader v_MessageHeader_Response := cr_MessageHeader_Dummy;
    var template (present) PAccessNetworkInfo v_PAccessNetworkInfo := cr_PAccessNetworkInfoDef(f_IMS_PTC_RanType_GetString());  /* acc. to A.3.1 condition A5: access network technology and, if applicable, the cell ID */
    var template Require v_Require := *;
    var boolean v_A5 := true;    /* true => response sent by the UE within a dialog */
    var boolean v_A2A4 := false; /* true => Response sent by UE for INVITE/UPDATE */
    var boolean v_RequestIsInvite := false;
    var boolean v_RequestIsCancel := false;
    var charstring v_Method := valueof(p_MessageHeader_Request.cSeq.method);
    var integer v_Port_us;

    if ((p_PreconditionIndication != NoPreconditionIndication) and f_ContentType_IsSDP(p_ContentType)) { /* @sic R5-160794 sic@
                                                                                                            NOTE 1: in general a SIP response carries an SDP answer (i.e. no offer) and therefore uses a Require but no Supported header
                                                                                                            NOTE 2: when the SDP is optional, ContentType is optional too (-> "ifprsent") and  f_ContentType_IsSDP returns false */
      v_Require := cr_Require(tsc_OptionTagList_precondition);
    }
    
    select (v_Method) {
      case ("NOTIFY", "MESSAGE") {
        v_A5 := false;
      }
      case ("UPDATE") {
        v_A2A4 := true;
      }
      case ("INVITE") {
        v_A2A4 := true;
        v_RequestIsInvite := true;
      }
      case ("CANCEL") {
        v_RequestIsCancel := true;
      }
    }
    v_MessageHeader_Response.via          := f_Via_ResponseRX(p_MessageHeader_Request.via);        /* @sic R5s140350 sic@ */
    v_MessageHeader_Response.fromField    := f_IMS_Response_FromHeaderRX(p_MessageHeader_Request); /* @sic R5s130453 sic@ */
    v_MessageHeader_Response.toField      := f_IMS_Response_ToHeaderRX(p_MessageHeader_Request);   /* @sic R5s130183 change 4 sic@   @sic R5-131897, R5-132063 - new generic function sic@ */
    v_MessageHeader_Response.callId       := p_MessageHeader_Request.callId;
    v_MessageHeader_Response.sessionId    := p_MessageHeader_Request.sessionId;   /* @sic R5s130195 BASELINE MOVING 2013 - SessionId sic@ */
    v_MessageHeader_Response.cSeq         := p_MessageHeader_Request.cSeq;
    v_MessageHeader_Response.contentType  := p_ContentType;
    v_MessageHeader_Response.require      := v_Require;                           /* @sic R5-160790, R5-160903, R5-160793 sic@ */
    
    if (not f_IMS_PTC_SecurityScheme_IsGiba() and not v_RequestIsCancel) {     /* @sic R5-130683: pAccessNetworkInfo is optional for GIBA; R5s140092 change 12: CANCEL  sic@
                                                                                  @sic R5-142223: new condition A8: Any response sent by the UE, except for CANCEL requests sic@ */
      v_MessageHeader_Response.pAccessNetworkInfo := v_PAccessNetworkInfo;
    }
    else {
      v_MessageHeader_Response.pAccessNetworkInfo := v_PAccessNetworkInfo ifpresent;
    }
    
    if (v_RequestIsInvite) {  /* @sic R5-135004 Change 3 sic@
                                 PROSE ISSUE: prose is not clear about whether there shall be any RecordRoute header for responses to requests other than INVITE */
      v_MessageHeader_Response.recordRoute := p_MessageHeader_Request.recordRoute;     // @sic R5-135004: same as in the INVITE sent to the UE sic@
    }
    if (v_A2A4) {
      v_Port_us := f_IMS_PTC_GetPort_us();  /* @sic R5s130985 Change 2 sic@ */
      v_MessageHeader_Response.contact := cr_Contact(f_Contact_SipUri_HostPortRX(?, v_Port_us));    /* host address needs to be checked after receiving
                                                                                                       @sic R5s150721, R5-153760: cr_SipUri_HostPort replaced by f_Contact_SipUri_HostPortRX sic@ */
    }
    return v_MessageHeader_Response;
  }

  /*
   * @desc      to receive response from the UE and check Via header
   * @param     p_RequestHeader
   * @param     p_Protocol
   * @param     p_Response
   * @param     p_IMS_DATA_RSP_ByRef (by reference)
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  altstep a_IMS_ReceiveResponse(template (value) MessageHeader p_RequestHeader,
                                template (present) InternetProtocol_Type p_Protocol,
                                template (present) IMS_Response_Type p_Response,
                                out IMS_DATA_RSP p_IMS_DATA_RSP_ByRef) runs on IMS_PTC
  {
    var IMS_DATA_RSP v_IMS_DATA_RSP;

    [] IMS_Client.receive(car_IMS_DATA_RSP(cr_IMS_RoutingInfo(p_Protocol), p_Response)) -> value v_IMS_DATA_RSP
      {
        f_IMS_MessageHeader_Response_CheckVia(valueof(p_RequestHeader), v_IMS_DATA_RSP.Response.msgHeader);
        p_IMS_DATA_RSP_ByRef := v_IMS_DATA_RSP;
      }
  }
  /*
   * @desc      wrapper function for a_IMS_ReceiveResponse
   * @param     p_RequestHeader
   * @param     p_Protocol
   * @param     p_Response
   * @return    IMS_DATA_RSP
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)
   */
  function f_IMS_ReceiveResponse(template (value) MessageHeader p_RequestHeader,
                                 template (present) InternetProtocol_Type p_Protocol,
                                 template (present) IMS_Response_Type p_Response) runs on IMS_PTC return IMS_DATA_RSP
  {
    var IMS_DATA_RSP v_IMS_DATA_RSP_ByRef;
    a_IMS_ReceiveResponse(p_RequestHeader, p_Protocol, p_Response, v_IMS_DATA_RSP_ByRef);
    return v_IMS_DATA_RSP_ByRef;
  }

  /*
   * @desc      wrapper altstep to receive 200 OK
   * @param     p_RequestHeader
   * @param     p_Protocol
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT)
   */
  altstep a_IMS_ReceiveResponse200OK(template (value) MessageHeader p_RequestHeader,
                                     template (present) InternetProtocol_Type p_Protocol) runs on IMS_PTC
  { // NOTE: in general the responses e.g. for different NOTIFY messages differ in the CSeq value only
    var template (present) MessageHeader v_MessageHeader_Response := f_IMS_OtherResponse_200_MessageHeaderRX(p_RequestHeader);
    var IMS_DATA_RSP v_IMS_DATA_RSP_ByRef;  // Dummy
    
    [] a_IMS_ReceiveResponse(p_RequestHeader, p_Protocol, cr_Response(c_statusLine200, v_MessageHeader_Response), v_IMS_DATA_RSP_ByRef);
  }





}
