/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-05 20:03:08 +0100 (Sat, 05 Mar 2016) $
// $Rev: 15515 $
/******************************************************************************/

module IMS_SDP_MessagesVideo {
  import from LibSip_Common all;
  import from LibSip_SDPTypes all;
  import from IMS_Component all;
  import from IMS_SDP_Templates all;

  type enumerated VideoCallTypeMO_Type {C25, AddVideo, CallHold};                       /* @status    APPROVED (IMS, LTE, LTE_A_R12) */
  type enumerated VideoCallTypeMT_Type {C26, AddVideo};                                 /* @status    APPROVED (IMS) */
  type enumerated VideoCallMT_MessageTX_Type {Invite, Update, InviteRemoveVideo};       /* @status    APPROVED (IMS) */
  type enumerated VideoCallMT_MessageRX_Type {Response183, Response200};                /* @status    APPROVED (IMS) */

  /*
   * @desc      returns true if media attributes for tcap and pcfg are needed in DL
   * @param     p_SDP_Media_Desc
   * @return    boolean
   * @status    APPROVED (IMS, LTE, LTE_A_R12)
   */
  function fl_IMS_Video_TcapPcfgRequired(SDP_media_desc p_SDP_Media_Desc) return boolean
  {
    return (p_SDP_Media_Desc.media_field.transport == c_rtpAvp);
  }

  //============================================================================
  // Video - SDP Messages Common
  //----------------------------------------------------------------------------
  /*
   * @desc      Common function for video SDP messges in UL
   * @param     p_TimeDescription   (default value: cr_SDP_Time_0_0)
   * @param     p_SDP_Media_Field_Audio
   * @param     p_SDP_MediaAttributes_Audio
   * @param     p_SDP_PrecondionAttributes_Audio
   * @param     p_SDP_Media_Field_Video
   * @param     p_SDP_MediaAttributes_Video
   * @param     p_SDP_PrecondionAttributes_Video
   * @return    template (present) SDP_Message
   * @status    APPROVED (IMS, LTE, LTE_A_R12)
   */
  function f_IMS_BuildSDP_AudioVideo_RX(template (present) SDP_time p_TimeDescription := cr_SDP_Time_0_0,
                                        template (present) SDP_media_field p_SDP_Media_Field_Audio,
                                        template (present) SDP_attribute_list p_SDP_MediaAttributes_Audio,
                                        template (present) SDP_attribute_list p_SDP_PrecondionAttributes_Audio,
                                        template (present) SDP_media_field p_SDP_Media_Field_Video,
                                        template (present) SDP_attribute_list p_SDP_MediaAttributes_Video,
                                        template (present) SDP_attribute_list p_SDP_PrecondionAttributes_Video) runs on IMS_PTC return template (present) SDP_Message
  {
    var charstring v_AddrType := f_IMS_PTC_NW_Address_GetTypeStr();
    var template (present) charstring v_IPAddrOrigin := ?;
    var template (present) charstring v_IPAddrAudio := ?;
    var template (present) charstring v_IPAddrVideo := ?;
    var template (present) SDP_attribute_list v_SDP_Attribute_List_Audio := f_SDP_Attributes_Concat_RX(p_SDP_MediaAttributes_Audio, p_SDP_PrecondionAttributes_Audio);
    var template (present) SDP_attribute_list v_SDP_Attribute_List_Video := f_SDP_Attributes_Concat_RX(p_SDP_MediaAttributes_Video, p_SDP_PrecondionAttributes_Video);
    var template (present) SDP_media_desc v_SDP_Media_Desc_Audio;
    var template (present) SDP_media_desc v_SDP_Media_Desc_Video;
    var template SDP_bandwidth_list v_BandwidthList_Audio := f_SDP_SDP_Media_Desc_BandwidthList(p_SDP_Media_Field_Audio);  /* results in * if the audio stream shall be released */
    var template SDP_bandwidth_list v_BandwidthList_Video := f_SDP_SDP_Media_Desc_BandwidthList(p_SDP_Media_Field_Video);  /* results in * if the video stream shall be released */
    var template (present) SDP_media_desc_list v_SDP_Media_Desc_List;

    v_SDP_Attribute_List_Audio := superset(all from v_SDP_Attribute_List_Audio);
    v_SDP_Attribute_List_Video := superset(all from v_SDP_Attribute_List_Video);

    v_SDP_Media_Desc_Audio := cr_SDP_Media_Desc(p_SDP_Media_Field_Audio,
                                                cr_SDP_Connection(v_AddrType, v_IPAddrAudio),
                                                v_BandwidthList_Audio,
                                                v_SDP_Attribute_List_Audio);
    v_SDP_Media_Desc_Video := cr_SDP_Media_Desc(p_SDP_Media_Field_Video,
                                                cr_SDP_Connection(v_AddrType, v_IPAddrVideo),
                                                v_BandwidthList_Video,
                                                v_SDP_Attribute_List_Video);
    v_SDP_Media_Desc_List := { v_SDP_Media_Desc_Audio, v_SDP_Media_Desc_Video };
    
    return cr_SDP_Message_Common(v_AddrType, v_IPAddrOrigin, ?, -, p_TimeDescription, v_SDP_Media_Desc_List);
  }

  /*
   * @desc      Common function for video SDP messges in DL
   * @param     p_SessionBandwidthAS  (default value: 30)
   * @param     p_MediaAudio
   * @param     p_Bandwidth_List_Audio
   * @param     p_SDP_MediaAttributes_Audio
   * @param     p_SDP_PrecondionAttributes_Audio
   * @param     p_MediaVideo
   * @param     p_Bandwidth_List_Video
   * @param     p_SDP_MediaAttributes_Video
   * @param     p_SDP_PrecondionAttributes_Video
   * @return    template (value) SDP_Message
   * @status    APPROVED (IMS, LTE, LTE_A_R12)
   */
  function f_IMS_BuildSDP_AudioVideo_TX(integer p_SessionBandwidthAS := 30,
                                        template (value) SDP_media_field p_MediaAudio,
                                        template (value) SDP_bandwidth_list p_Bandwidth_List_Audio,
                                        template (omit) SDP_attribute_list p_SDP_MediaAttributes_Audio,
                                        template (omit) SDP_attribute_list p_SDP_PrecondionAttributes_Audio,
                                        template (value) SDP_media_field p_MediaVideo,
                                        template (value) SDP_bandwidth_list p_Bandwidth_List_Video,
                                        template (omit) SDP_attribute_list p_SDP_MediaAttributes_Video,
                                        template (omit) SDP_attribute_list p_SDP_PrecondionAttributes_Video)
    runs on IMS_PTC
    return template (value) SDP_Message
  {
    var charstring v_SessionId := f_IMS_PTC_ImsInfo_DialogGetSessIdTX();   /* @sic R5-150707 sic@ */
    var charstring v_SessionVersion := f_IMS_PTC_ImsInfo_DialogIncrAndGetSessVersionTX();
    var charstring v_AddrType := f_IMS_PTC_NW_Address_GetTypeStr();
    var charstring v_IPAddrConnection := f_IMS_PTC_RemoteAddress_GetAddrStr();
    var charstring v_IPAddrOrigin := v_IPAddrConnection;
    var template (omit) SDP_attribute_list v_SDP_Attribute_List_Audio := f_SDP_Attributes_Concat_TX(p_SDP_MediaAttributes_Audio, p_SDP_PrecondionAttributes_Audio);
    var template (omit) SDP_attribute_list v_SDP_Attribute_List_Video := f_SDP_Attributes_Concat_TX(p_SDP_MediaAttributes_Video, p_SDP_PrecondionAttributes_Video);
    var template (value) SDP_media_desc v_SDP_Media_Desc_AudioTx;
    var template (value) SDP_media_desc v_SDP_Media_Desc_VideoTx;
    var template (value) SDP_media_desc_list v_SDP_Media_Desc_List;

    v_SDP_Media_Desc_AudioTx := cs_SDP_Media_Desc(p_MediaAudio,
                                                  p_Bandwidth_List_Audio,
                                                  v_SDP_Attribute_List_Audio);
    v_SDP_Media_Desc_VideoTx := cs_SDP_Media_Desc(p_MediaVideo,
                                                  p_Bandwidth_List_Video,
                                                  v_SDP_Attribute_List_Video);
    
    v_SDP_Media_Desc_List := { v_SDP_Media_Desc_AudioTx, v_SDP_Media_Desc_VideoTx };

    return cs_SDP_Message_Common(cs_SDP_Origin(v_SessionId, v_SessionVersion, v_AddrType, v_IPAddrOrigin),
                                 cs_SDP_Connection(v_AddrType, v_IPAddrConnection),
                                 cs_SDP_Bandwidth_List_AS(p_SessionBandwidthAS),
                                 -,
                                 v_SDP_Media_Desc_List);
  }

  //============================================================================
  // Video - SDP Messages MO Call (C.25 and 17.1)
  //----------------------------------------------------------------------------
  /*
   * @desc      Build SDP record to be received from the UE at step 2 according to 34.229-1 Annex C.25
   * @return    template (present) SDP_Message
   * @status    APPROVED (IMS, LTE, LTE_A_R12)
   */
  function f_IMS_BuildSDP_AnnexC25_Step2() runs on IMS_PTC return template (present) SDP_Message
  { /* @sic R5-151796: "a=inactive" removed from media attributes for audio and video sic@ */
    var template (present) SDP_attribute_list v_SDP_MediaAttributes_Audio := {
      cr_SDP_Attribute_rtpmap(-, cr_RTPMAP_AMR_8000),
      cr_SDP_Attribute_fmtp(-, cr_AMR_Fmtp_DefaultParameters),
      cr_SDP_Attribute_rtpmap(-, cr_RTPMAP_TelephoneEvent),
      cr_SDP_Attribute_ptime,
      cr_SDP_Attribute_maxptime
    };
    var template (present) SDP_attribute_list v_SDP_MediaAttributes_Video := {
      // The tcap/pcfg attributes need to be checked later when we know whether we have RTP/AVP or RTP/AVPF
      cr_SDP_Attribute_rtpmap(-, cr_RTPMAP_H264_90000),
      cr_SDP_Attribute_fmtp(-, cr_Video_Fmtp_DefaultParametersMO)
    };
    var template (present) SDP_attribute_list v_SDP_PrecondionAttributes := cr_SDP_PrecondionAttributes(c_none, c_none, c_mandatory, c_optional);

    return f_IMS_BuildSDP_AudioVideo_RX(cr_SDP_Time_Any,
                                        cr_SDP_Media_Audio,
                                        v_SDP_MediaAttributes_Audio,
                                        v_SDP_PrecondionAttributes,
                                        cr_SDP_Media_Video(-, (c_rtpAvp, c_rtpAvpf)),
                                        v_SDP_MediaAttributes_Video,
                                        v_SDP_PrecondionAttributes);
  }

  /*
   * @desc      Build SDP record to be received from the UE at step 2 according to 34.229-1 test case 17.1
   * @return    template (present) SDP_Message
   * @status    APPROVED (IMS, LTE, LTE_A_R12)
   */
  function f_IMS_BuildSDP_AddVideoMO_Step2() runs on IMS_PTC return template (present) SDP_Message
  {
    var template (present) SDP_attribute_list v_SDP_MediaAttributes_Audio := {
      cr_SDP_Attribute_rtpmap(-, cr_RTPMAP_AMR_8000),
      cr_SDP_Attribute_fmtp                                 // => any parameters for audio
    };
    var template (present) SDP_attribute_list v_SDP_PrecondionAttributes_Audio := cr_SDP_PrecondionAttributes(c_sendrecv, c_sendrecv, c_mandatory, c_optional);

    var template (present) SDP_attribute_list v_SDP_MediaAttributes_Video := {
      // The tcap/pcfg attributes need to be checked later when we know whether we have RTP/AVP or RTP/AVPF
      cr_SDP_Attribute_rtpmap(-, cr_RTPMAP_H264_90000),
      cr_SDP_Attribute_fmtp(-, cr_Video_Fmtp_DefaultParametersMO)
    };
    var template (present) SDP_attribute_list v_SDP_PrecondionAttributes_Video := cr_SDP_PrecondionAttributes(c_none, c_none, c_mandatory, c_optional);

    return f_IMS_BuildSDP_AudioVideo_RX(cr_SDP_Time_Any,
                                        cr_SDP_Media_Audio,
                                        v_SDP_MediaAttributes_Audio,
                                        v_SDP_PrecondionAttributes_Audio,
                                        cr_SDP_Media_Video(-, (c_rtpAvp, c_rtpAvpf)),
                                        v_SDP_MediaAttributes_Video,
                                        v_SDP_PrecondionAttributes_Video);
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      Build SDP record to be sent to the UE at step 4 according to 34.229-1 Annex C.25 or test case 17.1
   * @param     p_VideoCallType
   * @param     p_SDP_Media_AudioRx
   * @param     p_SDP_Media_VideoRx
   * @return    template (value) SDP_Message
   * @status    APPROVED (IMS, LTE, LTE_A_R12)
   */
  function f_IMS_BuildSDP_MOCallAudioVideo_Step4(VideoCallTypeMO_Type p_VideoCallType,
                                                 SDP_media_desc p_SDP_Media_AudioRx,
                                                 SDP_media_desc p_SDP_Media_VideoRx)
    runs on IMS_PTC return template (value) SDP_Message
  { /* @sic R5-144402: p_IPAddrOrigin removed sic@ */
    var boolean v_TcapPcfg := fl_IMS_Video_TcapPcfgRequired(p_SDP_Media_VideoRx);
    var charstring v_FmtAudio := f_SDP_MediaDescr_GetAMR_8000_1(p_SDP_Media_AudioRx);
    var charstring v_FmtVideo := f_SDP_MediaDescr_GetH264_90000(p_SDP_Media_VideoRx);
    var template (omit) SDP_attribute v_FmtpAttributeVideo;
    var template (value) SDP_bandwidth_list v_Bandwidth_List_Audio := p_SDP_Media_AudioRx.bandwidth;
    var template (value) SDP_bandwidth_list v_Bandwidth_List_Video := p_SDP_Media_VideoRx.bandwidth;
    var template (omit) SDP_attribute_list v_SDP_MediaAttributes_Audio := f_SDP_MediaAttributes_AudioDef(v_FmtAudio);   /* @sic R5-144693: new function f_SDP_MediaAttributes_AudioDef sic@ */
    var template (omit) SDP_attribute_list v_SDP_MediaAttributes_Video := {};
    var template (omit) SDP_attribute_list v_SDP_PrecondionAttributes_Audio;
    var template (omit) SDP_attribute_list v_SDP_PrecondionAttributes_Video;
    var template (value) SemicolonParam_List v_FmtpParamListVideo := {};

    v_SDP_PrecondionAttributes_Video := f_SDP_Attributes_Add_TX(cs_SDP_PrecondionAttributes(c_none, c_none, c_mandatory, c_mandatory), cs_SDP_Attribute_conf_qos);

    select (p_VideoCallType) {
      case (C25) {
        v_SDP_PrecondionAttributes_Audio := v_SDP_PrecondionAttributes_Video;   // same preconditions for audio and video
      }
      case (AddVideo) {
        v_SDP_PrecondionAttributes_Audio := cs_SDP_PrecondionAttributes(c_sendrecv, c_sendrecv, c_mandatory, c_mandatory);
      }
    }
    
    v_FmtpAttributeVideo := f_SDP_AttributeList_GetAttribute(p_SDP_Media_VideoRx.attributes, cr_SDP_Attribute_fmtp(v_FmtVideo));   // @sic R5s141336 change 2.2 - MCC160 implementation sic@ */
    if (isvalue(v_FmtpAttributeVideo)) {
      v_FmtpParamListVideo :=  valueof(v_FmtpAttributeVideo.fmtp.params.paramList);      /* @sic R5-150734, R5-150736 sic@ */
    }

    v_SDP_MediaAttributes_Video := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes_Video, cs_SDP_Attribute_rtpmap(v_FmtVideo, cs_RTPMAP_H264_90000));
    v_SDP_MediaAttributes_Video := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes_Video, cs_SDP_Attribute_fmtp(v_FmtVideo, cs_Fmtp_ParamList(v_FmtpParamListVideo)));
    v_SDP_MediaAttributes_Video := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes_Video, cs_SDP_Attribute_acfg(1, "t=1"), v_TcapPcfg);

    if (p_VideoCallType == C25) {                                   /* NOTE: there is nothing specified in 17.1 regarding "a=inactive" yet */
      if (f_SDP_AttributeList_CheckAttribute(p_SDP_Media_VideoRx.attributes, cr_SDP_Attribute_inactive)) {               /* @sic R5-150694 sic@ */
        v_SDP_MediaAttributes_Video := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes_Video, cs_SDP_Attribute_inactive);
      }
      if (f_SDP_AttributeList_CheckAttribute(p_SDP_Media_AudioRx.attributes, cr_SDP_Attribute_inactive)) {               /* @sic R5-151796/R5-151797 sic@ */
        v_SDP_MediaAttributes_Audio := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes_Audio, cs_SDP_Attribute_inactive);
      }
    }

    return f_IMS_BuildSDP_AudioVideo_TX(-,
                                        cs_SDP_Media_Audio(v_FmtAudio),
                                        v_Bandwidth_List_Audio,
                                        v_SDP_MediaAttributes_Audio,
                                        v_SDP_PrecondionAttributes_Audio,
                                        cs_SDP_Media_Video(v_FmtVideo),
                                        v_Bandwidth_List_Video,
                                        v_SDP_MediaAttributes_Video,
                                        v_SDP_PrecondionAttributes_Video);
  }

  //----------------------------------------------------------------------------
  /*
   * @desc      to receive 2nd SDP answer sent by the UE in C.25 step 5/7
   * @param     p_VideoCallType
   * @param     p_SDP_MessageInvite
   * @return    template (present) SDP_Message
   * @status    APPROVED (IMS, LTE, LTE_A_R12)
   */
  function f_IMS_BuildSDP_MOCallAudioVideo_Step5or7(VideoCallTypeMO_Type p_VideoCallType,
                                                    SDP_Message p_SDP_MessageInvite) runs on IMS_PTC return template (present) SDP_Message
  { /* @sic R5-153610: p_SDP_MessageInvite instead of p_FmtAudio, p_FmtVideo sic@ */
   
    var SDP_media_desc v_SDP_Media_InviteAudio := p_SDP_MessageInvite.media_list[0];
    var SDP_media_desc v_SDP_Media_InviteVideo := p_SDP_MessageInvite.media_list[1];
    var charstring v_FmtAudio := f_SDP_MediaDescr_GetAMR_8000_1(v_SDP_Media_InviteAudio);  // @sic R5-153610 sic@
    var charstring v_FmtVideo := f_SDP_MediaDescr_GetH264_90000(v_SDP_Media_InviteVideo);  // @sic R5-153610 sic@

    var template (present) SDP_attribute_list v_SDP_MediaAttributes_Audio := {
      cr_SDP_Attribute_rtpmap(v_FmtAudio, cr_RTPMAP_AMR_8000),     // @sic R5s130681 change 2 sic@
      cr_SDP_Attribute_fmtp(v_FmtAudio)
    };
    var template (present) SDP_attribute_list v_SDP_MediaAttributes_Video;
    var template (present) SDP_attribute_list v_SDP_PrecondionAttributes_Audio;
    var template (present) SDP_attribute_list v_SDP_PrecondionAttributes_Video;
    var template (present) charstring v_DirectionRemote_Audio;
    var template (present) charstring v_DirectionRemote_Video;
    var template (present) charstring v_StrengthRemote := c_optional;

    v_SDP_MediaAttributes_Video := {    /* @sic R5s141336 change 3 sic@ */
      cr_SDP_Attribute_rtpmap(v_FmtVideo, cr_RTPMAP_H264_90000),
      cr_SDP_Attribute_fmtp(v_FmtVideo, cr_Video_Fmtp_DefaultParametersMO)  /* @sic R5-150734, R5-150736: same parameters for C.25 and 17.1 sic@ */
    };

    select (p_VideoCallType) {
      case (C25) {
        if (f_SDP_AttributeList_CheckAttribute(v_SDP_Media_InviteAudio.attributes, cr_SDP_Attribute_inactive)) {               /* @sic R5-153610 sic@ */
          v_SDP_MediaAttributes_Audio := f_SDP_Attributes_Add_RX(v_SDP_MediaAttributes_Audio, cr_SDP_Attribute_sendrecv);
        }
        if (f_SDP_AttributeList_CheckAttribute(v_SDP_Media_InviteVideo.attributes, cr_SDP_Attribute_inactive)) {               /* @sic R5-153610 sic@ */
          v_SDP_MediaAttributes_Video := f_SDP_Attributes_Add_RX(v_SDP_MediaAttributes_Video, cr_SDP_Attribute_sendrecv);
        }
        v_DirectionRemote_Audio := c_none;
        v_DirectionRemote_Video := c_none;
        v_StrengthRemote := (c_optional, c_mandatory);             /* @sic R5s141221 change 4.1; R5s150236 sic@ */
      }
      case (AddVideo) {
        v_DirectionRemote_Audio := c_sendrecv;
        v_DirectionRemote_Video := c_none;
      }
      case (CallHold) {
        v_DirectionRemote_Audio := c_sendrecv;
        v_DirectionRemote_Video := c_sendrecv;
        v_StrengthRemote := (c_optional, c_mandatory);             /* @sic R5s150755 sic@ */
      }
    }
    v_SDP_PrecondionAttributes_Video := cr_SDP_PrecondionAttributes(c_sendrecv, v_DirectionRemote_Audio, c_mandatory, v_StrengthRemote);
    v_SDP_PrecondionAttributes_Audio := cr_SDP_PrecondionAttributes(c_sendrecv, v_DirectionRemote_Video, c_mandatory, v_StrengthRemote);

    return f_IMS_BuildSDP_AudioVideo_RX(-,
                                        cr_SDP_Media_Audio(v_FmtAudio),
                                        v_SDP_MediaAttributes_Audio,
                                        v_SDP_PrecondionAttributes_Audio,
                                        cr_SDP_Media_Video(v_FmtVideo),
                                        v_SDP_MediaAttributes_Video,
                                        v_SDP_PrecondionAttributes_Video);
  }

  //============================================================================
  // Video - SDP Messages MT Call (C.26 and 17.2)
  //----------------------------------------------------------------------------
  /*
   * @desc      Build SDP message to be sent to the UE at step 1/7 of Annex C.26 or at step 1/6 of 17.2
   * @param     p_VideoCallType
   * @param     p_MessageType
   * @param     p_FmtAudio
   * @param     p_FmtVideo
   * @param     p_DirectionRemote   (default value: c_none)
   * @return    template (value) SDP_Message
   * @status    APPROVED (IMS)
   */
  function f_IMS_BuildSDP_MTCallAudioVideo_TX(VideoCallTypeMT_Type p_VideoCallType,
                                              VideoCallMT_MessageTX_Type p_MessageType,
                                              charstring p_FmtAudio,
                                              charstring p_FmtVideo,
                                              charstring p_DirectionRemote := c_none)
    runs on IMS_PTC
    return template (value) SDP_Message
  {
    var template (value) SDP_media_field v_MediaVideo := cs_SDP_Media_Video(p_FmtVideo);
    var template (omit) SDP_attribute_list v_SDP_MediaAttributes_Audio := f_SDP_MediaAttributes_AudioCommon(p_FmtAudio);   /* @sic R5-144693: new function f_SDP_MediaAttributes_AudioCommon sic@ */
    var template (omit) SDP_attribute_list v_SDP_MediaAttributes_Video := {
      cs_SDP_Attribute_rtpmap(p_FmtVideo, cs_RTPMAP_H264_90000),
      cs_SDP_Attribute_fmtp(p_FmtVideo, cs_Fmtp_ParamList(cs_Fmtp_VideoParamsDef))
    };
    var integer v_SessionBandwidthAS;
    var template (value) SDP_bandwidth_list v_Bandwidth_List_Audio;
    var template (value) SDP_bandwidth_list v_Bandwidth_List_Video := cs_SDP_Bandwidth_List_Media(315, 0, 2500);
    var template (value) SDP_attribute_list v_SDP_PrecondionAttributes_Audio;
    var template (value) SDP_attribute_list v_SDP_PrecondionAttributes_Video;
    var boolean v_IsNotRemoveVideo := (p_MessageType != InviteRemoveVideo);
    var boolean v_IsUpdate := false;
    
    if (p_MessageType != InviteRemoveVideo) {
      v_SDP_MediaAttributes_Video := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes_Video, cs_SDP_Attribute_rtcp_fb("*", "trr-int 5000"), v_IsNotRemoveVideo);  /* @sic R5s141329 change 2.2: attribute is for video sic@ */
      v_SDP_MediaAttributes_Video := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes_Video, cs_SDP_Attribute_rtcp_fb("*", "nack"),         v_IsNotRemoveVideo);  /* @sic R5s141329 change 2.2: attribute is for video sic@ */
      v_SDP_MediaAttributes_Video := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes_Video, cs_SDP_Attribute_rtcp_fb("*", "nack pli"),     v_IsNotRemoveVideo);  /* @sic R5s141329 change 2.2: attribute is for video sic@ */
      v_SDP_MediaAttributes_Video := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes_Video, cs_SDP_Attribute_rtcp_fb("*", "ccm fir"),      v_IsNotRemoveVideo);  /* @sic R5s141329 change 2.2: attribute is for video sic@ */
      v_SDP_MediaAttributes_Video := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes_Video, cs_SDP_Attribute_rtcp_fb("*", "ccm tmmbr"),    v_IsNotRemoveVideo);  /* @sic R5s141329 change 2.2: attribute is for video sic@ */
    }

    select (p_MessageType) {
      case (Invite) {
        v_SDP_PrecondionAttributes_Video := cs_SDP_PrecondionAttributes(c_none, c_none, c_mandatory, c_mandatory);
      }
      case (Update) {
        v_SDP_PrecondionAttributes_Video := cs_SDP_PrecondionAttributes(c_sendrecv, p_DirectionRemote, c_mandatory, c_mandatory);
        v_IsUpdate := true;
      }
      case (InviteRemoveVideo) {
        v_SDP_PrecondionAttributes_Video := cs_SDP_PrecondionAttributes(c_sendrecv, c_sendrecv, c_mandatory, c_mandatory);
        v_MediaVideo.ports.port_number := 0;
      }
    }

    if (not (v_IsUpdate and (p_VideoCallType == AddVideo))) { // @sic R5-151956: there are no ptime, maxptime for UPDATE of 17.2 sic@
      // additional attributes for audio: ptime, maxptime
      v_SDP_MediaAttributes_Audio := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes_Audio, cs_SDP_Attribute_ptime);
      v_SDP_MediaAttributes_Audio := f_SDP_Attributes_Add_TX(v_SDP_MediaAttributes_Audio, cs_SDP_Attribute_maxptime);
    }

    select (p_VideoCallType) {
      case (C26) {
        v_SessionBandwidthAS := 30;                                           // @sic R5-151956 sic@
        v_Bandwidth_List_Audio := cs_SDP_Bandwidth_List_Media(30, 0, 2000);   // @sic R5-151956 sic@
        // @sic R5-153610: "sendrecv" media attribute removed from UPDATE (Step 7) for audio and Video sic@
        v_SDP_PrecondionAttributes_Audio := v_SDP_PrecondionAttributes_Video;   // same precondions for video and audio
      }
      case (AddVideo) {
        v_SessionBandwidthAS := 37;                                           // @sic R5-151956 sic@
        v_Bandwidth_List_Audio := cs_SDP_Bandwidth_List_Media(37, 0, 2500);   // @sic R5-151956 sic@
        // acc. 34.229-1 in contrast to C.26 there is no sendrecv attribute for audio or video
        v_SDP_PrecondionAttributes_Audio :=  cs_SDP_PrecondionAttributes(c_sendrecv, c_sendrecv, c_mandatory, c_mandatory);    // audio is already there
      }
    }
    
    return f_IMS_BuildSDP_AudioVideo_TX(v_SessionBandwidthAS,
                                        cs_SDP_Media_Audio(p_FmtAudio),
                                        v_Bandwidth_List_Audio,
                                        v_SDP_MediaAttributes_Audio,
                                        v_SDP_PrecondionAttributes_Audio,
                                        v_MediaVideo,
                                        v_Bandwidth_List_Video,
                                        v_SDP_MediaAttributes_Video,
                                        v_SDP_PrecondionAttributes_Video);
  }

  /*
   * @desc      SDP message received in step 4 (C.26) or step 3 (17.2)
   * @param     p_VideoCallType
   * @param     p_MessageType
   * @param     p_FmtAudio
   * @param     p_FmtVideo
   * @return    template (present) SDP_Message
   * @status    APPROVED (IMS)
   */
  function f_IMS_BuildSDP_MTCallAudioVideo_RX(VideoCallTypeMT_Type p_VideoCallType,
                                              VideoCallMT_MessageRX_Type p_MessageType,
                                              charstring p_FmtAudio,
                                              charstring p_FmtVideo)
    runs on IMS_PTC
    return template (present) SDP_Message
  {
    var template (present) SDP_attribute_list v_SDP_MediaAttributes_Audio := {
      cr_SDP_Attribute_rtpmap(p_FmtAudio, cr_RTPMAP_AMR_8000),
      cr_SDP_Attribute_fmtp(p_FmtAudio)
    };
    var template (present) SDP_attribute_list v_SDP_MediaAttributes_Video := {
      cr_SDP_Attribute_rtpmap(p_FmtVideo, cr_RTPMAP_H264_90000),
      cr_SDP_Attribute_fmtp(p_FmtVideo, cr_Video_Fmtp_DefaultParametersMT)  /* @sic R5s141329 - MCC160 implementation sic@
                                                                               @sic R5-150703: same parameters for C.26 and 17.2 sic@ */
    };
    var template (present) SDP_attribute_list v_SDP_PrecondionAttributes_Audio;
    var template (present) SDP_attribute_list v_SDP_PrecondionAttributes_Video;

    select (p_MessageType) {
      case (Response183) {
        v_SDP_PrecondionAttributes_Video := f_SDP_Attributes_Add_RX(cr_SDP_PrecondionAttributes((c_none, c_sendrecv), c_none, c_mandatory, c_mandatory), cr_SDP_Attribute_conf_qos);
      }
      case (Response200) {
        v_SDP_PrecondionAttributes_Video := cr_SDP_PrecondionAttributes(c_sendrecv, c_sendrecv, c_mandatory, c_mandatory);
      }
    }

    select (p_VideoCallType) {
      case (C26) {
        v_SDP_PrecondionAttributes_Audio := v_SDP_PrecondionAttributes_Video;
      }
      case (AddVideo) {
        v_SDP_PrecondionAttributes_Audio := cr_SDP_PrecondionAttributes(c_sendrecv, c_sendrecv, c_mandatory, c_mandatory);
      }
    }

    return f_IMS_BuildSDP_AudioVideo_RX(-,
                                        cr_SDP_Media_Audio(p_FmtAudio),
                                        v_SDP_MediaAttributes_Audio,
                                        v_SDP_PrecondionAttributes_Audio,
                                        cr_SDP_Media_Video(p_FmtVideo),
                                        v_SDP_MediaAttributes_Video,
                                        v_SDP_PrecondionAttributes_Video);
  }

}
