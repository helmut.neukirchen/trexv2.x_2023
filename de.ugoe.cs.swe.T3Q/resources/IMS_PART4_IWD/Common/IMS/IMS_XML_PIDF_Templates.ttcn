/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-05 20:03:08 +0100 (Sat, 05 Mar 2016) $
// $Rev: 15515 $
/******************************************************************************/

module IMS_XML_PIDF_Templates {
  import from urn_ietf_params_xml_ns_pidf language "XSD" all;
  import from urn_ietf_params_xml_ns_pidf_geopriv10 language "XSD" all;
 // import from urn_ietf_params_xml_ns_pidf all;
//  import from urn_ietf_params_xml_ns_pidf_geopriv10 all;
  import from XSD all;
  
  group Alias_Definitions {

  type Geopriv_1               Geolocation_Type;                   /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT) */

  type Presence_1              PIDF_Presence_Type;                 /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT) */
  type Tuple                   PIDF_Tuple_Type;
  type Tuple.note_list         PIDF_Tuple_Note_List_Type;
  type Status                  PIDF_Status_Type;
  type Status.elem_list        PIDF_Status_ElemList_Type;          /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT) */
  type Basic                   PIDF_Basic_Type;


  } with { encode "XML"};

  //----------------------------------------------------------------------------
  
  template (present) Geolocation_Type cr_Geolocation_Any :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT) */
    location_info := ?,
    usage_rules := ?,
    method := *,
    provided_by := *,
    elem_list := ?
  };

  //----------------------------------------------------------------------------

  template (present) PIDF_Status_Type cr_PIDF_Status_Any (template PIDF_Basic_Type p_PIDF_Basic := *,
                                                          template (present) PIDF_Status_ElemList_Type p_PIDF_Status_ElemList := ?) :=
  { /* @status    */
    basic := p_PIDF_Basic,
    elem_list := p_PIDF_Status_ElemList
  };
  
  template (present) PIDF_Tuple_Type cr_PIDF_Tuple_Any (template AnyURI p_Id := ?,
                                                        template (present) PIDF_Status_Type p_PIDF_Status := ?,
                                                        template Contact p_Contact := *,
                                                        template (present) PIDF_Tuple_Note_List_Type p_PIDF_Notes := ?,
                                                        template DateTime p_Timestamp := *) :=
  { /* @status    */
    id := p_Id,
    status := p_PIDF_Status,
    elem_list := {},
    contact := p_Contact,
    note_list := p_PIDF_Notes,
    timestamp := p_Timestamp
  };

  template (present) PIDF_Presence_Type cr_PIDF_Presence_Any (template AnyURI p_Entity := ?) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT) */
    entity := p_Entity,
    tuple_list := ?,
    note_list := ?,
    elem_list := {}
  };

}
