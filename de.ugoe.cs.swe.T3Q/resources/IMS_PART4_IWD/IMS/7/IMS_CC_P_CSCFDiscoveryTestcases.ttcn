/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2015-11-06 11:15:57 +0100 (Fri, 06 Nov 2015) $
// $Rev: 14687 $
/******************************************************************************/

module IMS_CC_P_CSCFDiscoveryTestcases {

  import from NAS_CommonTypeDefs all;
  import from IMS_Component all;
  import from IMS_Procedures_Registration all;
  import from IMS_PTC_CoordMsg all;

  import from IMS_CC_CommonFunctions all;
  import from IMS_CC_IPCAN_Coordination all;
  import from IMS_CC_PCO_Handling all;

  /****************************************************************************/
  /*
   * @desc      REFERENCE TS 34.229-1 clause 7.1
   * @status    APPROVED (IMS)
   */
  function f_TC_7_1_IMS1() runs on IMS_PTC
  {
    var NAS_ProtocolConfigOptions_Type v_ConfigOptionsRx;
    var template (value) NAS_ProtocolConfigOptions_Type v_ConfigOptionsTx;
    
    f_IMS_CC_Preamble(IPCAN_SignallingOnly, IMS_NULL); // @sic R5s150786 sic@
    f_IMS_CC_StartSignalling(IPCAN_InitialRegistration);
    
    f_IMS_TestBody_Set(true);
    // @siclog "Step 1" siclog@
    v_ConfigOptionsRx := f_IMS_IPCAN_WaitForPCOs(IPCAN);
    // Step 1
    if (
      (not f_ProtocolConfigOptions_Check(v_ConfigOptionsRx, tsc_ConfigOptions_P_CSCF_IPv6_Request)) and
      (not f_ProtocolConfigOptions_Check(v_ConfigOptionsRx, tsc_ConfigOptions_P_CSCF_IPv4_Request))) {  // @sic CR R5s120530 TODO: Prose CR needed sic@
      f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "Step 1");
    } else {
      f_IMS_PreliminaryPass(__FILE__, __LINE__, "Step 1");
    }
    // Step 2
    // @siclog "Step 2" siclog@
    v_ConfigOptionsTx := f_IMS_CC_ProtocolConfigOption_GetResponse(v_ConfigOptionsRx);
    f_IMS_IPCAN_SendPCOs(IPCAN, v_ConfigOptionsTx);
    
    // Steps 3 to 4
    // @siclog "Step 3 - 4" siclog@
    f_IMS_Registration();
    
    f_IMS_TestBody_Set(false);
    
    f_IMS_CC_Postamble(IPCAN_InitialRegistration);
  }
  
}
