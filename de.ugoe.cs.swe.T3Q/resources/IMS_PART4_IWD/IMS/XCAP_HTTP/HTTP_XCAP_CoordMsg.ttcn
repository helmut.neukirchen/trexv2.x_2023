/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-04 21:03:50 +0100 (Fri, 04 Mar 2016) $
// $Rev: 15513 $
/******************************************************************************/

module HTTP_XCAP_CoordMsg {

  import from CommonDefs all;
  import from IP_ASP_TypeDefs all;

  type record HTTP_ConfigurationParams_Type { /* @status    APPROVED (IMS) */
    IP_SocketList_Type XcapServerAddrAndPort_List,     /* @sic R5s150148 sic@ */
    IP_SocketList_Type BsfServerAddrAndPort_List,      /* @sic R5s150148 sic@ */
    IP_DrbInfo_Type DrbRoutingInfo,
    HTTP_AuthenticationMethod_Type AuthenticationMethod,
    charstring XcapUsername,                /* Username to be used for Authorisation */
    charstring XcapUserId,                  /* public user indentity to be used for XCAP signalling (XUI) */
    charstring XcapRootUri
  };

  type enumerated HTTP_AuthenticationMethod_Type { /* @status    APPROVED (IMS) */
    noAuthentication,
    httpDigestAuthentication,
    httpDigestAndGbaAuthentication
  };

  type record HTTP_TransactionParams_Type { /* @status    APPROVED (IMS) */
    float                               MaxDuration,
    float                               InactivityDuration
  };

  type union IMS_HTTP_Command_Type {    /* @status    APPROVED (IMS) */
    HTTP_ConfigurationParams_Type       Configure,
    HTTP_TransactionParams_Type         TransactionStart,
    Null_Type                           DocumentGet,
    charstring                          DocumentSet
  };
    
  type union IMS_HTTP_Response_Type {   /* @status    APPROVED (IMS) */
    charstring Error,
    charstring Result
  };

  type port IMS_XCAP_CO_ORD_PORT message
  { /* @status    APPROVED (IMS) */
    out IMS_HTTP_Command_Type;
    in  IMS_HTTP_Response_Type;
  };
  

  type port XCAP_IMS_CO_ORD_PORT message
  { /* @status    APPROVED (IMS) */
    in  IMS_HTTP_Command_Type;
    out IMS_HTTP_Response_Type;
  };
  
}
