/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2015-11-27 22:15:29 +0100 (Fri, 27 Nov 2015) $
// $Rev: 14773 $
/******************************************************************************/

module EUTRA_AuxiliaryCapCheckFunctions
{

//=========================================================================
// Auxiliary functions for checking of capabilities
// for the time being only those functions needed outside module RRC_Others
// are contained here. More functions may be transferred in the future.
//=========================================================================

    import from CommonDefs all;
    import from EUTRA_Parameters all;
    import from EUTRA_RRC_ASN1_Definitions language "ASN.1:2002" all;
    import from Parameters all;

  /*
   * @desc      convert the PICS parameters pc_FeatrGrp_X_F from single booleans to a bitstring of size 32.
   *            Size 32 is chosen because the signaled element FDD featureGroupIndicators in UE-EUTRA-Capability is of size 32.
   * @return    B32_Type
   * @status    APPROVED (LTE, UTRAN)
   */
  function fl_DeriveFeatrGrpCapFromPics_r9_FDD () return B32_Type
  {
    var B32_Type v_FeatureGroupIndicators := f_ConvertBoolToBit(pc_FeatrGrp_1_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_2_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_3_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_4_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_5_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_6_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_7_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_8_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_9_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_10_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_11_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_12_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_13_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_14_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_15_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_16_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_17_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_18_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_19_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_20_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_21_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_22_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_23_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_24_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_25_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_26_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_27_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_28_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_29_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_30_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_31_F) & // @sic R5s130237 sic@
                                             '0'B;
    return v_FeatureGroupIndicators;
  } // fl_DeriveFeatrGrpCapFromPics_r9_FDD
    /*
   * @desc      convert the PICS parameters pc_FeatrGrp_X_T from single booleans to a bitstring of size 32.
   *            Size 32 is chosen because the signaled element TDD featureGroupIndicators in UE-EUTRA-Capability is of size 32.
   * @return    B32_Type
   * @status    APPROVED (LTE, UTRAN)
   */
  function fl_DeriveFeatrGrpCapFromPics_r9_TDD () return B32_Type
  {
    var B32_Type v_FeatureGroupIndicators := f_ConvertBoolToBit(pc_FeatrGrp_1_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_2_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_3_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_4_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_5_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_6_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_7_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_8_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_9_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_10_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_11_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_12_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_13_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_14_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_15_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_16_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_17_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_18_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_19_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_20_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_21_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_22_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_23_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_24_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_25_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_26_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_27_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_28_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_29_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_30_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_31_T) & // @sic R5s130237 sic@
                                             '0'B;
    return v_FeatureGroupIndicators;
  } // fl_DeriveFeatrGrpCapFromPics_r9_TDD

  /*
   * @desc      convert the PICS parameters additional pc_FeatrGrp_X_F from single booleans to a bitstring of size 32.
   *            Size 32 is chosen because the signaled element additional featureGroupIndicators FDD in UE-EUTRA-Capability is of size 32.
   * @return    B32_Type
   * @status    APPROVED (LTE)
   */
  function fl_DeriveFeatrGrpCapFromPics_r9Add_FDD () return B32_Type
  {
    var B32_Type v_FeatureGroupIndicators := f_ConvertBoolToBit(pc_FeatrGrp_33_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_34_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_35_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_36_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_37_F) & // @sic R5s130237 sic@
                                             f_ConvertBoolToBit(pc_FeatrGrp_38_F) & // @sic R5s130237 sic@
                                             f_ConvertBoolToBit(pc_FeatrGrp_39_F) & // @sic R5s130237 sic@
                                             f_ConvertBoolToBit(pc_FeatrGrp_40_F) & // @sic R5s130237 sic@
                                             f_ConvertBoolToBit(pc_FeatrGrp_41_F) & // @sic R5s130237 sic@
                                             '00000000000000000000000'B;
    return v_FeatureGroupIndicators;
  } // fl_DeriveFeatrGrpCapFromPics_r9Add_FDD

  /*
   * @desc      convert the PICS parameters additional pc_FeatrGrp_X_T from single booleans to a bitstring of size 32.
   *            Size 32 is chosen because the signaled element additional featureGroupIndicators TDD in UE-EUTRA-Capability is of size 32.
   * @return    B32_Type
   * @status    APPROVED (LTE)
   */
  function fl_DeriveFeatrGrpCapFromPics_r9Add_TDD () return B32_Type
  {
    var B32_Type v_FeatureGroupIndicators := f_ConvertBoolToBit(pc_FeatrGrp_33_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_34_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_35_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_36_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_37_T) & // @sic R5s130237 sic@
                                             f_ConvertBoolToBit(pc_FeatrGrp_38_T) & // @sic R5s130237 sic@
                                             f_ConvertBoolToBit(pc_FeatrGrp_39_T) & // @sic R5s130237 sic@
                                             f_ConvertBoolToBit(pc_FeatrGrp_40_T) & // @sic R5s130237 sic@
                                             f_ConvertBoolToBit(pc_FeatrGrp_41_T) & // @sic R5s130237 sic@
                                             '00000000000000000000000'B;
    return v_FeatureGroupIndicators;
  } // fl_DeriveFeatrGrpCapFromPics_r9Add_TDD
  
 /*
   * @desc      convert the PICS parameters pc_FeatrGrp_X_F from single booleans to a bitstring of size 32.
   *            Size 32 is chosen because the signaled element FDD featureGroupIndicators in UE-EUTRA-Capability is of size 32.
   * @return    B32_Type
   * @status    APPROVED (LTE)
   */
  function fl_DeriveFeatrGrpCapFromPics_r10_FDD () return B32_Type
  {
    var B32_Type v_FeatureGroupIndicators := f_ConvertBoolToBit(pc_FeatrGrp_101_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_102_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_103_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_104_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_105_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_106_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_107_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_108_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_109_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_110_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_111_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_112_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_113_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_114_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_115_F) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_116_F) &
                                             '0000000000000000'B;
    return v_FeatureGroupIndicators;
  } // fl_DeriveFeatrGrpCapFromPics_r10_FDD

 /*
   * @desc      convert the PICS parameters pc_FeatrGrp_X_T from single booleans to a bitstring of size 32.
   *            Size 32 is chosen because the signaled element FDD featureGroupIndicators in UE-EUTRA-Capability is of size 32.
   * @return    B32_Type
   * @status    APPROVED (LTE)
   */
  function fl_DeriveFeatrGrpCapFromPics_r10_TDD () return B32_Type
  {
    var B32_Type v_FeatureGroupIndicators := f_ConvertBoolToBit(pc_FeatrGrp_101_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_102_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_103_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_104_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_105_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_106_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_107_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_108_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_109_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_110_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_111_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_112_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_113_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_114_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_115_T) &
                                             f_ConvertBoolToBit(pc_FeatrGrp_116_T) &
                                             '0000000000000000'B;
    return v_FeatureGroupIndicators;
  } // fl_DeriveFeatrGrpCapFromPics_r10_TDD

  /*
   * @desc      Function to convert the PICS parameters pc_eBandX_Supp from single booleans to a bitstring.
   *            The leftmost bit represent eBand1 support ('1'B if supported according to PICS pc_eBand1_Supp
   *            and '0'B if not supported according to PICS pc_eBand1_Supp).
   * @return    bitstring
   * @status    APPROVED (LTE, LTE_A_IRAT, POS, UTRAN)
   */
  function f_DeriveSuppEutraBandsFromPics() return bitstring
  {
    var bitstring v_Bitstring := f_ConvertBoolToBit(pc_eBand1_Supp) &
                                 f_ConvertBoolToBit(pc_eBand2_Supp) &
                                 f_ConvertBoolToBit(pc_eBand3_Supp) &
                                 f_ConvertBoolToBit(pc_eBand4_Supp) &
                                 f_ConvertBoolToBit(pc_eBand5_Supp) &
                                 f_ConvertBoolToBit(pc_eBand6_Supp) &
                                 f_ConvertBoolToBit(pc_eBand7_Supp) &
                                 f_ConvertBoolToBit(pc_eBand8_Supp) &
                                 f_ConvertBoolToBit(pc_eBand9_Supp) &
                                 f_ConvertBoolToBit(pc_eBand10_Supp) &
                                 f_ConvertBoolToBit(pc_eBand11_Supp) &
                                 f_ConvertBoolToBit(pc_eBand12_Supp) &
                                 f_ConvertBoolToBit(pc_eBand13_Supp) &
                                 f_ConvertBoolToBit(pc_eBand14_Supp) &
                                 '0'B &//Band 15 is reserved
                                 '0'B &//Band 16 is reserved
                                 f_ConvertBoolToBit(pc_eBand17_Supp) &
                                 f_ConvertBoolToBit(pc_eBand18_Supp) &
                                 f_ConvertBoolToBit(pc_eBand19_Supp) &
                                 f_ConvertBoolToBit(pc_eBand20_Supp) & // @sic R5-103122 sic@
                                 f_ConvertBoolToBit(pc_eBand21_Supp) & // @sic R5-103122 sic@
                                 f_ConvertBoolToBit(pc_eBand22_Supp) & // @sic R5-115190 sic@
                                 f_ConvertBoolToBit(pc_eBand23_Supp) & // @sic R5-113669 sic@
                                 f_ConvertBoolToBit(pc_eBand24_Supp) &  // @sic R5-112489 sic@
                                 f_ConvertBoolToBit(pc_eBand25_Supp) &  // @sic R5-113156 sic@
                                 f_ConvertBoolToBit(pc_eBand26_Supp) &  // @sic R5-121224 sic@
                                 f_ConvertBoolToBit(pc_eBand27_Supp) &  // @sic R5s130320 sic@
                                 f_ConvertBoolToBit(pc_eBand28_Supp) &  // @sic R5s130320 sic@
                                 f_ConvertBoolToBit(pc_eBand29_Supp) &  // @sic R5s130320 sic@
                                 f_ConvertBoolToBit(pc_eBand30_Supp) &  // @sic R5-144079 sic@
                                 f_ConvertBoolToBit(pc_eBand31_Supp) &  // @sic R5s133307 sic@
                                 f_ConvertBoolToBit(pc_eBand32_Supp) &  // @sic R5-151966 sic@
                                 f_ConvertBoolToBit(pc_eBand33_Supp) &
                                 f_ConvertBoolToBit(pc_eBand34_Supp) &
                                 f_ConvertBoolToBit(pc_eBand35_Supp) &
                                 f_ConvertBoolToBit(pc_eBand36_Supp) & //@sic R5s100020 sic@
                                 f_ConvertBoolToBit(pc_eBand37_Supp) &
                                 f_ConvertBoolToBit(pc_eBand38_Supp) &
                                 f_ConvertBoolToBit(pc_eBand39_Supp) &
                                 f_ConvertBoolToBit(pc_eBand40_Supp) &
                                 f_ConvertBoolToBit(pc_eBand41_Supp) & //@sic R5-106554 sic@
                                 f_ConvertBoolToBit(pc_eBand42_Supp) & //@sic R5-110537 sic@
                                 f_ConvertBoolToBit(pc_eBand43_Supp);  //@sic R5-110537 sic@
    return v_Bitstring;
  } // f_DeriveSuppEutraBandsFromPics
  
  /*
   * @desc      Function which returns the total number of supported bands according to the set PICS parameters pc_eBandX_Supp.
   * @param     p_Bitstring
   * @return    integer
   */
  function f_GetNrOfSuppEutraBands(bitstring p_Bitstring) return integer
  {
    var integer v_NrOfSuppEutraBands := 0;
    var integer i;

    for (i := 0; i < lengthof(p_Bitstring); i := i + 1) {
      if (f_Bitstring_BitIsSet(p_Bitstring, i)) {
        v_NrOfSuppEutraBands := v_NrOfSuppEutraBands + 1;
      }
    }
    return v_NrOfSuppEutraBands;
  } // f_GetNrOfSuppEutraBands
  
}
