/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-07 17:18:18 +0100 (Mon, 07 Mar 2016) $
// $Rev: 15549 $
/******************************************************************************/

module EUTRA_ASP_VirtualNoiseDefs {
  /* ASP definitions for virtual noise generation in EUTRA cells.
     The noise is configured for an already existing EUTRA cell.
     For UEs with 2 antenna connectors the AWGN (Additive white Gaussian noise) signals applied to each receiver antenna connector shall be uncorrelated. */

  import from CommonDefs all;
  import from EUTRA_CommonDefs all;

  type record EUTRA_VngConfigInfo_Type {
    Dl_Bandwidth_Type           Bandwidth,      /* Bandwidth to be used for the noise
                                                 * (in general the same bandwidth as for the associated EUTRA cell) */
    integer                     NocLevel        /* Noc level; calculation is according to 36.523-3 cl 7.22 */
  };

  type union EUTRA_VngConfigRequest_Type {     /* configure/activate noise for a given cell;
                                                 * NOTE: it is assumed the the associated EUTRA cell has been created beforehand */
    EUTRA_VngConfigInfo_Type    Configure,      /* configuration of the virtual noise generator;
                                                 * regardless of the power level the noise generator is off before it gets activated for this cell;
                                                 * in case the configuration needs to be changed during a test, the noise generator shall be deactivated for this cell */
    Null_Type                   Activate,       /* noise is activated (switched on) for the given cell acc. to the previous configuration;
                                                 * while being active the configuration shall not be modified */
    Null_Type                   Deactivate      /* deactivate noise for given cell */
  };

  type Null_Type EUTRA_VngConfigConfirm_Type;

  type record EUTRA_VNG_CTRL_REQ {
    ReqAspCommonPart_Type       Common,         /* CellId : as for the assiciated EUTRA cell
                                                 * RoutingInfo : None
                                                 * TimingInfo : Now
                                                 * ControlInfo : CnfFlag:=true; FollowOnFlag:=false */
    EUTRA_VngConfigRequest_Type Request
  };


  type record EUTRA_VNG_CTRL_CNF {
    CnfAspCommonPart_Type       Common,         /* TimingInfo is ignored by TTCN (apart from EnquireTiming)
                                                 * => SS may set TimingInfo to "None" */
    EUTRA_VngConfigConfirm_Type Confirm
  };

  type port EUTRA_VNG_PORT message {            /* EUTRA PTC: Port for virtual noise generator */
    out EUTRA_VNG_CTRL_REQ;
    in  EUTRA_VNG_CTRL_CNF;
  };
}
