/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-05 20:03:08 +0100 (Sat, 05 Mar 2016) $
// $Rev: 15515 $
/******************************************************************************/

module IMS_ASP_Templates {
  import from LibSip_SIPTypesAndValues all;
  import from IP_ASP_TypeDefs all;
  import from IMS_ASP_TypeDefs all;

  //****************************************************************************
  // ASPs to send/receive IMS messages
  //----------------------------------------------------------------------------

  template (value) IMS_RoutingInfo_Type cs_IMS_RoutingInfo(InternetProtocol_Type p_Protocol,
                                                           template (omit) IMS_SecurityContextEnum_Type p_SecurityContextEnum := omit,
                                                           template (omit) IP_AddrInfo_Type p_UE_Address := omit,
                                                           template (omit) IP_AddrInfo_Type p_NW_Address := omit) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)  */
    Protocol := p_Protocol,
    Security := p_SecurityContextEnum,
    UE_Address := p_UE_Address,
    NW_Address := p_NW_Address
  };

  template (present) IMS_RoutingInfo_Type cr_IMS_RoutingInfo(template (present) InternetProtocol_Type p_Protocol := ?,
                                                             template IMS_SecurityContextEnum_Type p_SecurityContextEnum := *) :=  // @sic R5s130133 additional change 1; R5s130187 sic@
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)  */
    Protocol := p_Protocol,
    Security := p_SecurityContextEnum,
    UE_Address := *,       // @sic R5s120907 change 1 sic@
    NW_Address := *        // @sic R5s120907 change 1 sic@
  };

  //----------------------------------------------------------------------------

  template (present) IMS_DATA_REQ cr_IMS_DATA_REQ_Any(template (present) IMS_RoutingInfo_Type p_RoutingInfo := ?,
                                                      template (present) IMS_Request_Type p_IMS_Request := ?) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN) */
    /* @sic @sic R5s140710 - MCC160 Implementation sic@ */
    RoutingInfo := p_RoutingInfo,
    Request := p_IMS_Request
  };

  template (present) IMS_DATA_REQ car_IMS_Register_Request(template (present) REGISTER_Request p_RegisterRequest) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)  */
    RoutingInfo := cr_IMS_RoutingInfo,
    Request := {
      Register := p_RegisterRequest
    }
  };
  
  template (present) IMS_DATA_RSP cr_IMS_DATA_RSP_Any := ?;  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)  */

  template (present) IMS_DATA_REQ car_IMS_Subscribe_Request(template (present) SUBSCRIBE_Request p_SubscribeRequest) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)  */
    RoutingInfo := cr_IMS_RoutingInfo,
    Request := {
      Subscribe := p_SubscribeRequest
    }
  };
  
  template (value) IMS_DATA_REQ cas_IMS_Notify_Request(template (value) IMS_RoutingInfo_Type p_RoutingInfo,
                                                       template (value) NOTIFY_Request p_NotifyRequest) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)  */
    RoutingInfo := p_RoutingInfo,
    Request := {
      Notify := p_NotifyRequest
    }
  };
  
  template (present) IMS_DATA_REQ car_IMS_Notify_Request(template (present) NOTIFY_Request p_NotifyRequest) :=
  {
    RoutingInfo := cr_IMS_RoutingInfo,
    Request := {
      Notify := p_NotifyRequest
    }
  };
  
  //----------------------------------------------------------------------------

  template (value) IMS_DATA_REQ cas_IMS_DATA_REQ(template (value) IMS_RoutingInfo_Type p_RoutingInfo,
                                                 template (value) IMS_Request_Type p_Request) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)  */
    RoutingInfo := p_RoutingInfo,
    Request := p_Request
  };

  template (value) IMS_DATA_RSP cas_IMS_DATA_RSP(template (value) IMS_RoutingInfo_Type p_RoutingInfo,
                                                 template (value) IMS_Response_Type p_Response) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS, UTRAN)  */
    RoutingInfo := p_RoutingInfo,
    Response := p_Response
  };

  template (present) IMS_DATA_RSP car_IMS_DATA_RSP(template (present) IMS_RoutingInfo_Type p_RoutingInfo,
                                                   template (present) IMS_Response_Type p_Response) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS)  */
    RoutingInfo := p_RoutingInfo,
    Response := p_Response
  };

  //----------------------------------------------------------------------------

  template (present) IMS_DATA_REQ car_IMS_Ack_Request(template (present) ACK_Request p_AckRequest) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT) */
    RoutingInfo := cr_IMS_RoutingInfo,
    Request := {
      Ack := p_AckRequest
    }
  };

  template (present) IMS_DATA_REQ car_IMS_Prack_Request(template (present) PRACK_Request p_PrackRequest) :=
  { /* @status    APPROVED (IMS, LTE, LTE_A_IRAT, LTE_A_R12) */
    RoutingInfo := cr_IMS_RoutingInfo,
    Request := {
      Prack := p_PrackRequest
    }
  };
  
  template (present) IMS_DATA_REQ car_IMS_Refer_Request(template (present) REFER_Request p_ReferRequest) :=
  { /* @status    APPROVED (IMS) */
    RoutingInfo := cr_IMS_RoutingInfo,
    Request := {
      Refer := p_ReferRequest
    }
  };

  template (value) IMS_DATA_REQ cas_IMS_Prack_Request(template (value) IMS_RoutingInfo_Type p_RoutingInfo,
                                                      template (value) PRACK_Request p_PrackRequest) :=
  { /* @status    APPROVED (IMS, LTE_A_IRAT, LTE_IRAT) */
    RoutingInfo := p_RoutingInfo,
    Request := {
      Prack := p_PrackRequest
    }
  };

  template (value) IMS_DATA_REQ cas_IMS_Ack_Request(template (value) IMS_RoutingInfo_Type p_RoutingInfo,
                                                    template (value) ACK_Request p_AckRequest) :=
  { /* @status    APPROVED (IMS, LTE_A_IRAT, LTE_IRAT) */
    RoutingInfo := p_RoutingInfo,
    Request := {
      Ack := p_AckRequest
    }
  };
  
  template (present) IMS_DATA_REQ car_IMS_Update_Request(template (present) UPDATE_Request p_UpdateRequest := ?) :=
  { /* @status    APPROVED (IMS, LTE, LTE_A_IRAT, LTE_A_R12) */
    RoutingInfo := cr_IMS_RoutingInfo,
    Request := {
      Update := p_UpdateRequest
    }
  };

  template (value) IMS_DATA_REQ cas_IMS_Update_Request(template (value) IMS_RoutingInfo_Type p_RoutingInfo,
                                               template (value) UPDATE_Request p_UpdateRequest) :=
  { /* @status    APPROVED (IMS, LTE_A_IRAT, LTE_IRAT) */
    RoutingInfo := p_RoutingInfo,
    Request := {
      Update := p_UpdateRequest
    }
  };

  
  template (value) IMS_DATA_REQ cas_IMS_Cancel_Request(template (value) IMS_RoutingInfo_Type p_RoutingInfo,
                                                       template (value) CANCEL_Request p_CancelRequest ) :=
  { /* @status    APPROVED (IMS, LTE_A_IRAT) */
    RoutingInfo := p_RoutingInfo,
    Request := {
      Cancel := p_CancelRequest
    }
  };

  template (present) IMS_DATA_REQ car_IMS_Invite_Request(template (present) INVITE_Request p_InviteRequest := ?) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12, LTE_IRAT) */
    RoutingInfo := cr_IMS_RoutingInfo,
    Request := {
      Invite := p_InviteRequest
    }
  };

  template (value) IMS_DATA_REQ cas_IMS_Invite_Request(template (value) IMS_RoutingInfo_Type p_RoutingInfo,
                                                       template (value) INVITE_Request p_InviteRequest ) :=
  { /* @status    APPROVED (IMS, LTE_A_IRAT, LTE_IRAT) */
    RoutingInfo := p_RoutingInfo,
    Request := {
      Invite := p_InviteRequest
    }
  };
  
  template (present) IMS_DATA_REQ car_IMS_Bye_Request(template (present) BYE_Request p_ByeRequest := ?) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R12) */
    RoutingInfo := cr_IMS_RoutingInfo,
    Request := {
      Bye := p_ByeRequest
    }
  };
  
  template (value) IMS_DATA_REQ cas_IMS_BYE_Request(template (value) IMS_RoutingInfo_Type p_RoutingInfo,
                                                    template (value) BYE_Request p_ByeRequest ) :=
  { /* @status    APPROVED (IMS, LTE_A_IRAT, LTE_IRAT) */
    RoutingInfo := p_RoutingInfo,
    Request := {
      Bye := p_ByeRequest
    }
  };

  template (present) IMS_DATA_REQ car_IMS_Publish_Request(template (present) PUBLISH_Request p_PublishRequest := ?) :=
  { /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT, POS) */
    RoutingInfo := cr_IMS_RoutingInfo,
    Request := {
      Publish := p_PublishRequest
    }
  };
  
  template (present) IMS_DATA_REQ car_IMS_Message_Request(template (present) MESSAGE_Request p_MessageRequest := ?) :=
  { /* @status    APPROVED (IMS, LTE_A_IRAT) */
    RoutingInfo := cr_IMS_RoutingInfo,
    Request := {
      Message := p_MessageRequest
    }
  };
  
  template (value) IMS_DATA_REQ cas_IMS_Message_Request(template (value) IMS_RoutingInfo_Type p_RoutingInfo,
                                                        template (value) MESSAGE_Request p_MessageRequest ) :=
  { /* @status    APPROVED (IMS, LTE_A_IRAT) */
    RoutingInfo := p_RoutingInfo,
    Request := {
      Message := p_MessageRequest
    }
  };
  
  template (value) IMS_DATA_REQ cas_IMS_Refer_Request(template (value) IMS_RoutingInfo_Type p_RoutingInfo,
                                                      template (value) REFER_Request p_ReferRequest ) :=
  {
    RoutingInfo := p_RoutingInfo,
    Request := {
      Refer := p_ReferRequest
    }
  };
  
}
