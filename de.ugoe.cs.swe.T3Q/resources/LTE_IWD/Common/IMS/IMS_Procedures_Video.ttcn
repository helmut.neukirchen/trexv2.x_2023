/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2016-03-05 20:03:08 +0100 (Sat, 05 Mar 2016) $
// $Rev: 15515 $
/******************************************************************************/

module IMS_Procedures_Video {
  import from LibSip_Common all;
  import from LibSip_SDPTypes all;
  import from LibSip_SIPTypesAndValues all;
  import from IMS_Component all;
  import from IMS_SDP_Templates all;
  import from IMS_SDP_Messages all;
  import from IMS_SDP_MessagesVideo all;
  import from IMS_MessageBody_Templates all;
  import from IMS_ASP_TypeDefs all;
  import from IMS_ASP_Templates all;
  import from IMS_SIP_Templates all;
  import from IMS_CommonFunctions all;
  import from IMS_Procedures_CallControl all;
  import from IMS_CommonParameters all;
  import from IMS_CommonDefs all;

  //============================================================================
  /*
   * @desc      Check whether media description contains valid attributes for tcap and pcfg
   * @param     p_SDP_Media_Desc
   * @status    APPROVED (IMS, LTE, LTE_A_R12)
   */
  function f_IMS_SDP_CheckMediaDescr_TcapPcfg(SDP_media_desc p_SDP_Media_Desc) runs on IMS_PTC
  {
    var template (omit) SDP_attribute v_Attribute;
    if (fl_IMS_Video_TcapPcfgRequired(p_SDP_Media_Desc)) {
      v_Attribute := f_SDP_AttributeList_GetAttribute(p_SDP_Media_Desc.attributes, cr_SDP_Attribute_tcap(1, c_rtpAvpf));
      if (not isvalue (v_Attribute)) {
        f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "missing tcap attribute");
      }
      v_Attribute := f_SDP_AttributeList_GetAttribute (p_SDP_Media_Desc.attributes, cr_SDP_Attribute_pcfg(1, "t=1"));
      if (not isvalue (v_Attribute)) {
        f_IMS_SetVerdictFailOrInconc(__FILE__, __LINE__, "missing pcfg attribute");
      }
    }
  }
  //============================================================================
  // Video - MO Call setup
  //----------------------------------------------------------------------------
  /*
   * @desc      Step 2 of C.25 or test case 17.1
   * @param     p_VideoCallType
   * @param     p_OtherCalleeUri    (default value: omit)
   * @return    IMS_InviteRequestWithSdp_Type
   * @status    APPROVED (IMS, LTE, LTE_A_R12)
   */
  function f_IMS_MOCallSetup_Video_Step2(VideoCallTypeMO_Type p_VideoCallType,
                                         template SipUrl p_OtherCalleeUri := omit) runs on IMS_PTC return IMS_InviteRequestWithSdp_Type
  { /* @sic R5w140112: return type changed to IMS_InviteRequestWithSdp_Type to carry decoded SDP offer */
    /* @sic R5s150204: p_OtherCalleeUri charstring -> SipUrl sic@ */
    var template (present) SDP_Message v_SDP_Message_Step2;
    var INVITE_A_2_1_Context_Type v_Context;
    var IMS_DATA_REQ v_IMS_INVITE_REQ;
    var SDP_Message v_SDP_Message;
    var boolean v_A11 := pc_IMS_Video_FeatureTag;
    var boolean v_A12 := false;   // as we have video but not voice

    select (p_VideoCallType) {
      case (C25) {
        v_SDP_Message_Step2 := f_IMS_BuildSDP_AnnexC25_Step2();
        v_Context := A_2_1_A4;
      }
      case (AddVideo) {
        v_SDP_Message_Step2 := f_IMS_BuildSDP_AddVideoMO_Step2();
        v_Context := A_2_1_A5;
      }
    }

    // Step 2. Receive INVITE Request
    v_IMS_INVITE_REQ := f_IMS_INVITE_ReceiveRequest(v_Context, tsc_OptionTagList_precondition, p_OtherCalleeUri, -, -, -, v_A11, v_A12);  /* @sic R5s150180: p_A10 removed sic@ */
    v_SDP_Message := f_IMS_SIP_SdpMessageBody_DecodeMatchAndCheckSDP(v_IMS_INVITE_REQ.Request.Invite.messageBody, v_SDP_Message_Step2);

    // check media description:
    f_IMS_SDP_CheckMediaDescr_VoiceCodec(v_SDP_Message.media_list[0], AMR_8000);  /* @sic R5-151968: f_IMS_SDP_CheckMediaDescr_AMR_8000_1 replaced by f_IMS_SDP_CheckMediaDescr_VoiceCodec sic@ */
    f_IMS_SDP_CheckMediaDescr_H264_90000(v_SDP_Message.media_list[1]);
    f_IMS_SDP_CheckMediaDescr_TcapPcfg(v_SDP_Message.media_list[1]);

    return f_IMS_InviteRequestWithSdpMO(v_IMS_INVITE_REQ, v_SDP_Message);
  }
  
  //----------------------------------------------------------------------------
  /*
   * @desc      Steps 3..4 of C.25 or test case 17.1
   * @param     p_VideoCallType
   * @param     p_InviteRequestWithSdp
   * @param     p_ContactUri        (default value: px_IMS_CalleeContactUri)
   * @param     p_AditionalContactParams (default value: omit)
   * @param     p_CallType          (default value: NORMAL_CALL)
   * @return    MessageHeader
   * @status    APPROVED (IMS, LTE, LTE_A_R12)
   */
  function f_IMS_MOCallSetup_Video_Step3_4(VideoCallTypeMO_Type p_VideoCallType,
                                           IMS_InviteRequestWithSdp_Type p_InviteRequestWithSdp,
                                           charstring p_ContactUri := px_IMS_CalleeContactUri,
                                           template (omit) SemicolonParam_List p_AditionalContactParams := omit,
                                           IMS_CallType_Type p_CallType := NORMAL_CALL) runs on IMS_PTC return MessageHeader
  { /* return MessageHeader of the 183 response */
    /* @sic R5-155179: p_CallType sic@ */
    var template (value) IMS_RoutingInfo_Type v_RoutingInfo_DL := f_IMS_RoutingInfo_ULtoDL(p_InviteRequestWithSdp.RoutingInfo);
    var INVITE_Request v_InviteRequest := p_InviteRequestWithSdp.Invite;
    var SDP_Message v_SDP_MessageStep2 := p_InviteRequestWithSdp.SdpOffer;
    var SDP_media_desc v_SDP_Media_AudioRx := v_SDP_MessageStep2.media_list[0];
    var SDP_media_desc v_SDP_Media_VideoRx := v_SDP_MessageStep2.media_list[1];
    var template (value) SDP_Message v_SDP_MessageStep4 := f_IMS_BuildSDP_MOCallAudioVideo_Step4(p_VideoCallType, v_SDP_Media_AudioRx, v_SDP_Media_VideoRx);
    var template (value) MessageHeader v_MessageHeader_Response183;

    // Step 3. SS sends a 100 Trying provisional response.
    IMS_Server.send(cas_IMS_DATA_RSP(v_RoutingInfo_DL, cs_Response(c_statusLine100, f_IMS_InviteResponse_100_MessageHeaderTX(v_InviteRequest))));
                    
    // Step 4. Send 183 Session In Progress.
    
    select (p_VideoCallType) {
      case (C25) {
        v_MessageHeader_Response183 := f_IMS_InviteResponse_183_MessageHeaderTX(v_InviteRequest, tsc_OptionTagList_precondition, p_ContactUri, p_AditionalContactParams, p_CallType); /* @sic R5-142951 sic@ */
      }
      case (AddVideo) {
        v_MessageHeader_Response183 := f_IMS_InviteResponse_183_MessageHeaderTX(v_InviteRequest, -, p_ContactUri, p_AditionalContactParams, p_CallType);
        v_MessageHeader_Response183.supported := cs_Supported(tsc_OptionTagList_precondition);
      }
    }
    IMS_Server.send(cas_IMS_DATA_RSP(v_RoutingInfo_DL, cs_Response(c_statusLine183, v_MessageHeader_Response183, cs_MessageBody_SDP(v_SDP_MessageStep4))));
    
    return valueof(v_MessageHeader_Response183);
  }
   
  /*
   * @desc      steps 5-8 of video call setup procedure acc. to C.25
   * @param     p_VideoCallType
   * @param     p_InviteRequestWithSdp
   * @param     p_MessageHeader_Prev183Response
   * @param     p_CallType          (default value: NORMAL_CALL)
   * @param     p_PreconditionIndication  (default value: NoPreconditionIndication)
   * @return    SDP_Message
   * @status    APPROVED (IMS, LTE, LTE_A_R12)
   */
  function f_IMS_MOCallSetup_Video_Steps5_8(VideoCallTypeMO_Type p_VideoCallType,
                                            IMS_InviteRequestWithSdp_Type p_InviteRequestWithSdp,
                                            template (value) MessageHeader p_MessageHeader_Prev183Response,
                                            IMS_CallType_Type p_CallType := NORMAL_CALL,
                                            IMS_PreconditionIndication_Type p_PreconditionIndication := NoPreconditionIndication)
    runs on IMS_PTC return SDP_Message
  { /* @sic R5-155179: p_CallType sic@ */
    /* @sic R5-160794 (17.1): p_PreconditionIndication sic@ */

    var SDP_Message v_SDP_LatestOffer;
    var SDP_Message v_SDP_Offer := p_InviteRequestWithSdp.SdpOffer;
    var template (present) SDP_Message v_SDP_Message_Step5or7 := f_IMS_BuildSDP_MOCallAudioVideo_Step5or7(p_VideoCallType, v_SDP_Offer);

    v_SDP_LatestOffer := f_IMS_MOCallSetup_AnnexC21C25_Steps5_8(p_InviteRequestWithSdp, p_MessageHeader_Prev183Response, -, v_SDP_Message_Step5or7, -, p_CallType, -, -, p_PreconditionIndication);
    return v_SDP_LatestOffer;
  }

  //============================================================================
  // Annex C25 - Steps
  //----------------------------------------------------------------------------
  /*
   * @desc      steps 5..13 of C.25
   * @param     p_InviteRequestWithSdp
   * @param     p_MessageHeader_Prev183Response
   * @param     p_ContactUri        (default value: px_IMS_CalleeContactUri)
   * @return    SDP_Message
   * @status    APPROVED (IMS, LTE, LTE_A_R12)
   */
  function f_IMS_MOCallSetup_AnnexC25_Steps5_13(IMS_InviteRequestWithSdp_Type p_InviteRequestWithSdp,
                                                MessageHeader p_MessageHeader_Prev183Response,
                                                charstring p_ContactUri := px_IMS_CalleeContactUri)
    runs on IMS_PTC return SDP_Message
  {
    var SDP_Message v_SDP_LatestOffer;
    var boolean v_SrvccAlerting := false;               /* as we have no voice call */

    v_SDP_LatestOffer := f_IMS_MOCallSetup_Video_Steps5_8(C25, p_InviteRequestWithSdp, p_MessageHeader_Prev183Response);
    f_IMS_MOCallSetup_AnnexC21C25_Step9_11(p_InviteRequestWithSdp, p_ContactUri, v_SrvccAlerting);
    f_IMS_MOCallSetup_AnnexC21C25_Step12_13(p_InviteRequestWithSdp, p_ContactUri);
    return v_SDP_LatestOffer;
  }
  
  //============================================================================
  // Video - MT Call setup
  //----------------------------------------------------------------------------
  /*
   * @desc      send INVITE for MT video call
   * @param     p_VideoCallType
   * @param     p_MessageType
   * @param     p_FmtAudio
   * @param     p_FmtVideo
   * @param     p_ContactUrl        (default value: omit)
   * @return    IMS_InviteRequestWithSdp_Type
   * @status    APPROVED (IMS)
   */
  function f_IMS_MTCallSetup_Video_SendInvite(VideoCallTypeMT_Type p_VideoCallType,
                                              VideoCallMT_MessageTX_Type p_MessageType,
                                              charstring p_FmtAudio,
                                              charstring p_FmtVideo,
                                              template (omit) SipUrl p_ContactUrl := omit) runs on IMS_PTC return IMS_InviteRequestWithSdp_Type
  {
    var boolean v_SrvccAlerting := false;
    var SipUrl v_ContactUrl;
    var template (value) SDP_Message v_SDP_Message_Invite;
    var template (value) MessageHeader v_MessageHeader_Invite;
    var template (value) INVITE_Request v_InviteRequest;
    var IMS_InviteRequestWithSdp_Type v_InviteRequestWithSdp;
    var INVITE_A_2_1_Context_Type v_Context := A_2_1_A4;

    select (p_VideoCallType) {
      case (C26) { v_Context := A_2_1_A4; }
      case else  { v_Context := A_2_1_A5; }
    }
    if (isvalue(p_ContactUrl)) {
      v_ContactUrl := valueof(p_ContactUrl);
    } else {
      v_ContactUrl := f_IMS_PTC_ImsInfo_GetContactUrl();
    }

    v_SDP_Message_Invite := f_IMS_BuildSDP_MTCallAudioVideo_TX(p_VideoCallType, p_MessageType, p_FmtAudio, p_FmtVideo);
    v_MessageHeader_Invite  := f_IMS_InviteRequest_MessageHeaderTX(v_Context, v_SrvccAlerting);
    v_InviteRequest := cs_INVITE_Request(v_ContactUrl, v_MessageHeader_Invite, cs_MessageBody_SDP(v_SDP_Message_Invite));
    v_InviteRequestWithSdp := f_IMS_InviteRequestWithSdpMT(v_InviteRequest, v_SDP_Message_Invite);

    // Step 1 - Send INVITE
    IMS_Client.send(cas_IMS_Invite_Request(v_InviteRequestWithSdp.RoutingInfo, v_InviteRequest));

    return v_InviteRequestWithSdp;
  }

  /*
   * @desc      return SessionInProgress183 and its SDP message received in step 4 (C.26) or step 3 (17.2)
   * @param     p_VideoCallType
   * @param     p_InviteRequestWithSdp
   * @param     p_FmtAudio
   * @param     p_FmtVideo
   * @return    IMS_ResponseWithSdp_Type
   * @status    APPROVED (IMS)
   */
  function f_IMS_MTCallSetup_Video_Receive183(VideoCallTypeMT_Type p_VideoCallType,
                                              IMS_InviteRequestWithSdp_Type p_InviteRequestWithSdp,
                                              charstring p_FmtAudio,
                                              charstring p_FmtVideo) runs on IMS_PTC return IMS_ResponseWithSdp_Type
  {
    var INVITE_Request v_InviteRequest := p_InviteRequestWithSdp.Invite;
    var template (present) SDP_Message v_SDP_Message183 := f_IMS_BuildSDP_MTCallAudioVideo_RX(p_VideoCallType, Response183, p_FmtAudio, p_FmtVideo);

    return f_IMS_MTCallSetup_Common_Steps3_4(v_InviteRequest, v_SDP_Message183, p_InviteRequestWithSdp.RoutingInfo.Protocol);
  }

}
