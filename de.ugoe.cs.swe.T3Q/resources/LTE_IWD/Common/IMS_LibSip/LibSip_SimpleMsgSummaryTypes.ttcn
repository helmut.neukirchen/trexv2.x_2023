/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
/******************************************************************************/

/**
 * @author STF 406, 471
 * @version $Id: LibSip_SimpleMsgSummaryTypes.ttcn 488 2010-11-08 10:17:19Z pintar $
 * @desc This module provides the SMS type system for SIP tests.
 *              This module is part of LibSipV3.
*/
module LibSip_SimpleMsgSummaryTypes {
    // RFC 3842
    group SMSConstants {

        // IMS ETSI name MWI application
        const charstring c_imsEtsiMwiApplication := "application/simple-message-summary";

        // msg_status line := "Message-Waiting"
        const charstring c_messageWaiting := "Message-Waiting";

        // msg_status line := "Message-Account"
        const charstring c_messageAccount := "Message-Account";

        // msg_summary line := "Voice-Message"
        const charstring c_voiceMessage := "Voice-Message";

    }

    group SMSTypes {
        type record SimpleMsgSummary {
            // Simple_message_summary
            Msg_status_line msg_status_line,
            Msg_account msg_account optional,
            Msg_summary_line_list msg_summary_line_list optional,
            Opt_msg_headers opt_msg_headers optional
        }

        type record Msg_status_line {
            charstring msg_type,
            charstring msg_status
        }

        type record Msg_account {
            charstring msg_type_account,
            charstring account_URI
        }

        type set of Msg_summary_line Msg_summary_line_list;

        type record Msg_summary_line {
            charstring msg_context_class,
            charstring msgs,
            charstring urgent_msgs optional
        }

        type set of charstring Opt_msg_headers;


    // type integer msgcount length(1); //msgs and urgent_msgs can be dividet into subtypes
    }
}
