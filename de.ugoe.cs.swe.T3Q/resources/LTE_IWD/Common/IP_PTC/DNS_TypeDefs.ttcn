/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2016, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_16wk10
// $Date: 2014-12-01 15:13:04 +0100 (Mon, 01 Dec 2014) $
// $Rev: 12662 $
/******************************************************************************/

module DNS_TypeDefs {

  import from CommonDefs all;
  
  const B4_Type tsc_DNSOPCODE_QUERY  := '0000'B;
  const B4_Type tsc_DNSOPCODE_IQUERY := '0001'B;
  const B4_Type tsc_DNSOPCODE_STATUS := '0010'B;


  const B4_Type tsc_DNSRCODE_NoError        := '0000'B;
  const B4_Type tsc_DNSRCODE_FormatError    := '0001'B;
  const B4_Type tsc_DNSRCODE_ServerFailure  := '0010'B;
  const B4_Type tsc_DNSRCODE_NameError      := '0011'B;
  const B4_Type tsc_DNSRCODE_NotImplemented := '0100'B;
  const B4_Type tsc_DNSRCODE_Refused        := '0101'B;


  const O2_Type tsc_DNSTYPE_A     := '0001'O;
  const O2_Type tsc_DNSTYPE_NS    := '0002'O;
  const O2_Type tsc_DNSTYPE_MD    := '0003'O;
  const O2_Type tsc_DNSTYPE_MF    := '0004'O;
  const O2_Type tsc_DNSTYPE_CNAME := '0005'O;
  const O2_Type tsc_DNSTYPE_SOA   := '0006'O;
  const O2_Type tsc_DNSTYPE_MB    := '0007'O;
  const O2_Type tsc_DNSTYPE_MG    := '0008'O;
  const O2_Type tsc_DNSTYPE_MR    := '0009'O;
  const O2_Type tsc_DNSTYPE_NULL  := '000A'O;
  const O2_Type tsc_DNSTYPE_WKS   := '000B'O;
  const O2_Type tsc_DNSTYPE_PTR   := '000C'O;
  const O2_Type tsc_DNSTYPE_HINFO := '000D'O;
  const O2_Type tsc_DNSTYPE_MINFO := '000E'O;
  const O2_Type tsc_DNSTYPE_MX    := '000F'O;
  const O2_Type tsc_DNSTYPE_TXT   := '0010'O;
                                   
  const O2_Type tsc_DNSTYPE_AAAA  := '001C'O;      // IPv6 address, RFC 3596
  const O2_Type tsc_DNSTYPE_SRV   := '0021'O;      // Server selection, RFC 2782
  const O2_Type tsc_DNSTYPE_NAPTR := '0023'O;       // Naming Authority Pointer, RFC 3403
  
  const O2_Type tsc_DNSTYPE_AXFR  := '00FC'O;      // RFC 1035 RFC 3.2.3
  const O2_Type tsc_DNSTYPE_MAILB := '00FD'O;
  const O2_Type tsc_DNSTYPE_MAILA := '00FE'O;
  const O2_Type tsc_DNSTYPE_ALL   := '00FF'O;
  
  const O2_Type tsc_DNSCLASS_IN   := '0001'O;

  type record DNSMessage { // RFC 1035 clause 4
    DNS_Header dnsHeader,
    DNS_Questions questions optional,
    DNS_RRs answer optional,
    DNS_RRs authority optional,
    DNS_RRs additional optional
  };
  
  type record DNS_Header {      // RFC 1035 clause 4.1.1
    O2_Type id,
    B1_Type qr,                 // 0 message is a query, 1 message is a response
    B4_Type opcode,             // 0 QUERY, 1 IQUERY, 2 STATUS, 3-15 reserved
    B1_Type aa,                 // Authoritative Answer
    B1_Type tc,                 // Truncation
    B1_Type rd,                 // Recursion desired
    B1_Type ra,                 // Recursion available
    B3_Type z,                  // reserved, must be 0 in all queries and responses
    B4_Type rcode,              // Response code
    UInt16_Type qdcount,        // number of entries in the question section
    UInt16_Type ancount,        // number of RRs in the answer section
    UInt16_Type nscount,        // number of RRs in the auth. records section
    UInt16_Type arcount         // number of RRs in the additional records section
  };

  type record of DNS_Question DNS_Questions;
  
  type record DNS_Question {    // RFC 1035 clause 4.1.2
    octetstring qname,
    O2_Type qtype,
    O2_Type qclass
  };

  type record of DNS_RR DNS_RRs;
  
  type record DNS_RR {          // RFC 1035 clause 4.1.3
    octetstring name,
    O2_Type     rrtype,
    O2_Type     class,
    UInt32_Type ttl,
    UInt16_Type rdlength,
    DNS_RDATAu  rdata   // union of different types
  };


  type union DNS_RDATAu {
    DNS_A_RR            a,      // RFC 1035 clause 3.4.1, A 32 bit Internet Address
/*     DNS_PTR_RR          ptr,    // RFC 1035 clause 3.3.12 */
/*     DNS_NAPTR_RR        naptr,  // RFC 3403 clause 4.1 */
    DNS_AAAA_RR         aaaa    // RFC 3596
/*     DNS_SRV_RR          srv     // RFC 2782 */
  };

/*   type record DNS_PTR_RR {      //  RFC 1035 clause 3.3.12 */
/*     charstring  ptrdname */
/*   }; */
  
/*   type record DNS_NAPTR_RR {    //  RFC 3403 clause 4.1 */
/*     UInt16_Type         order, */
/*     UInt16_Type         preference, */
/*     charstring          flags optional, */
/*     charstring          services optional, */
/*     charstring          dnrr_regexp optional, */
/*     charstring          replacement optional */
/*   }; */

  type record DNS_AAAA_RR {     //  RFC 3596 clause 2.2
    O16_Type            ipv6addr
  };

  type record DNS_A_RR {        // RFC 1035 clause 3.4.1, A 32 bit Internet Address
    O4_Type             ipv4addr
  };

/*   type record DNS_SRV_RR {      // RFC 2782 (but where exactly is the format specification?) */
/*     UInt16_Type         priority, */
/*     UInt16_Type         weight, */
/*     UInt16_Type         srvport, */
/*     charstring          target */
/*   }; */

} with {encode "DNS-Codec"}




