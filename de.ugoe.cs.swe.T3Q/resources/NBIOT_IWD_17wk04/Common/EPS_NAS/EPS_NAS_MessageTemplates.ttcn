/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2017, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_17wk04
// $Date: 2017-01-25 20:44:24 +0100 (Wed, 25 Jan 2017) $
// $Rev: 17830 $
/******************************************************************************/

module EPS_NAS_MessageTemplates {
  
  import from CommonDefs all;
  import from NAS_CommonTypeDefs all;
  import from EPS_NAS_TypeDefs all;
  import from EPS_NAS_Constants all;
  import from EPS_NAS_MsgContainers all;
  import from NAS_CommonTemplates all;
  import from EPS_NAS_Templates all;
  import from Parameters all;
  import from NAS_AuxiliaryDefsAndFunctions all;
  
  template (value) PDN_Address cs_IPv4_PDNAddressDHCP :=
  {
    iei := omit,       // Mandatory
    iel := '05'O,
    spare := tsc_Spare5,
    typeValue := '001'B,  // IPv4
    adressInfo := '00000000'O  // DHCP is to be used
  };
  
  template (value) PDN_Address cs_IPv6_PDNAddressDHCP :=
  {
    iei := omit,       // Mandatory
    iel := '09'O,
    spare := tsc_Spare5,
    typeValue := '010'B,  // IPv4v6
    adressInfo := int2oct(0, 8) & '00000000'O  // what is the IP6 identifier?
  };
  
  template (value) PDN_Address cs_IPv4v6_PDNAddressDHCP :=
  {
    iei := omit,       // Mandatory
    iel := '0C'O,
    spare := tsc_Spare5,
    typeValue := '011'B,  // IPv4v6
    adressInfo := int2oct(0, 8) & '00000000'O  // DHCP is to be used
  };
  
  template (present) NAS_UL_Message_Type cdr_ATTACH_REQUEST_AttachType(template (present) NAS_AttDetValue_Type p_AttachTypeValue,
                                                                       template AdditionalUpdateType p_AdditionalUpdateType,
                                                                       template (present) MobileIdentity p_MobileId,
                                                                       template TrackingAreaId p_Tai,
                                                                       template LocAreaId p_LAI,
                                                                       template TMSI_Status p_TmsiStatus)
    modifies cr_508_ATTACH_REQUEST :=
  { /* @status    APPROVED (LTE, LTE_A_R10_R11, LTE_IRAT) */
    aTTACH_REQUEST := {
      epsAttachType            := cr_EPS_AttachType(p_AttachTypeValue),
      epsMobileIdentity        := p_MobileId,  // @sic R5s110176 Baseline Moving sic@
      lastVisitedRegisteredTai := p_Tai,
      oldLai                   := p_LAI,
      tmsiStatus               := p_TmsiStatus
    }
  };

  template (present) NAS_UL_Message_Type cdr_ATTACH_REQUEST_AttachTypeAndEPSnativeSecurityCtx(template (present) NAS_AttDetValue_Type p_AttachTypeValue,
                                                                                              template AdditionalUpdateType p_AdditionalUpdateType,
                                                                                              template (present) NAS_KsiValue p_KsiValue,
                                                                                              template (present) MobileIdentity p_MobileId,
                                                                                              template TrackingAreaId p_Tai,
                                                                                              template LocAreaId p_LAI := omit,
                                                                                              template TMSI_Status p_TmsiStatus := omit)
    modifies cr_508_ATTACH_REQUEST :=
  { /* @status    APPROVED (LTE, LTE_IRAT) */
    aTTACH_REQUEST := {
      nasKeySetId              := cr_NAS_KeySetIdentifier(p_KsiValue, tsc_NasKsi_NativeSecurityContext),
      epsAttachType            := cr_EPS_AttachType(p_AttachTypeValue),
      epsMobileIdentity        := p_MobileId,  // @sic R5s110176 Baseline Moving sic@
      lastVisitedRegisteredTai := p_Tai,
      oldLai                   := p_LAI,
      tmsiStatus               := p_TmsiStatus
    }
  };
    
  template (present) NAS_UL_Message_Type cdr_ATTACH_REQUEST_WithGuti(template (present) NAS_AttDetValue_Type p_AttachTypeValue,
                                                                     template AdditionalUpdateType p_AdditionalUpdateType,
                                                                     template (present) NAS_KsiValue p_KsiValue,
                                                                     template (present) MobileIdentity p_MobileId,
                                                                     template TrackingAreaId p_Tai)
    modifies cr_508_ATTACH_REQUEST :=
  { /* @status    APPROVED (LTE, LTE_A_R10_R11, LTE_IRAT) */
    aTTACH_REQUEST := {
      nasKeySetId              := cr_NAS_KeySetIdentifier(p_KsiValue, tsc_Spare1),
      epsAttachType            := cr_EPS_AttachType(p_AttachTypeValue),
      epsMobileIdentity        := p_MobileId,  // @sic R5s110176 Baseline Moving sic@
      lastVisitedRegisteredTai := p_Tai
    }
  };
  
  template (present) NAS_UL_Message_Type cdr_ATTACH_REQUEST_WithEPSnativeSecurityCtx(template (present) NAS_AttDetValue_Type p_AttachTypeValue,
                                                                                     template AdditionalUpdateType p_AdditionalUpdateType,
                                                                                     template (present) NAS_KsiValue p_KsiValue)
    modifies cr_508_ATTACH_REQUEST :=
  { /* @status    APPROVED (LTE) */
    aTTACH_REQUEST := {
      nasKeySetId              := cr_NAS_KeySetIdentifier(p_KsiValue, tsc_NasKsi_NativeSecurityContext),
      epsAttachType            := cr_EPS_AttachType(p_AttachTypeValue)
    }
  };
  
  template (present) NAS_UL_Message_Type cdr_ATTACH_REQUEST_WithOldMobileIdNoTAI(template (present) NAS_AttDetValue_Type p_AttachTypeValue,
                                                                                 template AdditionalUpdateType p_AdditionalUpdateType,
                                                                                 template MobileIdentity p_MobileId)
    modifies cr_508_ATTACH_REQUEST :=
  { /* 24.301 cl. 8.2.4 */
    /* @status    APPROVED (LTE) */
    aTTACH_REQUEST := {
      nasKeySetId              := cr_NAS_KeySetIdentifier(tsc_NasKsi_NoKey, tsc_Spare1),
      epsMobileIdentity        := p_MobileId,  // @sic R5s110176 Baseline Moving sic@
      lastVisitedRegisteredTai := omit
      
    }
  };
  
  template (present) NAS_UL_Message_Type cdr_ATTACH_REQUEST_IMSIonly(template (present) NAS_AttDetValue_Type p_AttachTypeValue,
                                                                     template AdditionalUpdateType p_AdditionalUpdateType,
                                                                     template TrackingAreaId p_Tai := omit)
    modifies cr_508_ATTACH_REQUEST  :=
  { /* @status    APPROVED (LTE, LTE_A_R10_R11) */
    aTTACH_REQUEST := {
      epsMobileIdentity   := f_Imsi2MobileIdentity(px_IMSI_Def),  // @sic R5s110176 Baseline Moving sic@
      lastVisitedRegisteredTai := p_Tai  // @sic R5s140041 sic@
    }
  };
  
  template (value) NAS_DL_Message_Type cs_508_AttachReject(template (value) EMM_Cause p_Cause,
                                                           template (omit) GPRS_Timer2 p_T3402 := omit,
                                                           template (omit) Extd_EMM_Cause p_Extd_EMM_Cause := omit):=
  { /* @status    APPROVED (LTE, LTE_A_R10_R11, LTE_IRAT) */
    aTTACH_REJECT := {
      securityHeaderType := tsc_SHT_NoSecurityProtection,
      protocolDiscriminator := tsc_PD_EMM,
      messageType := tsc_MT_AttachReject,
      emmCause := p_Cause,
      esmMessage := omit,   /* always omit as being added by the NAS emulator (when an ESM message is handed over by using cs_NAS_RequestWithPiggybacking) */
      t3346    := omit,      /* cl. 9.9.3.16A O TLV 3     IEI=0x5F @sic R5s120178 Baseline Moving sic@*/
      t3402    := p_T3402,   /* cl. 9.9.3.16A O TLV 3     IEI=0x16 @sic R5s120178 Baseline Moving sic@*/
      extdEMMCause   := p_Extd_EMM_Cause // @sic R5s150329 Baseline Moving sic@
    }
  };
  
  template (value) NAS_DL_Message_Type cs_508_AUTHENTICATION_REJECT :=
  { /* @status    APPROVED (LTE, NBIOT) */
    aUTHENTICATION_REJECT := {
      securityHeaderType          := tsc_SHT_NoSecurityProtection,
      protocolDiscriminator       := tsc_PD_EMM,
      messageType                 := tsc_MT_AuthenticationReject
    }
  };
  
  template (present) NAS_UL_Message_Type cdr_TAU_RequestOldGutiLaiTmsiStatus(template (present) EPS_UpdateTypeValue p_EpsUpdate_TypeValue,
                                                                             NAS_KsiValue p_KsiValue,
                                                                             template DRXparameter p_DRXparameter, // @sic R5s120210 sic@
                                                                             template AdditionalUpdateType p_AdditionalUpdateType,
                                                                             template (present) MobileIdentity p_OldGuti,
                                                                             template LocAreaId p_LAI,
                                                                             template TrackingAreaId p_TAI,
                                                                             template TMSI_Status p_TmsiStatus)
    modifies cr_508_TAU_Request :=
  { /* @status    APPROVED (LTE, LTE_IRAT) */
    tRACKING_AREA_UPDATE_REQUEST := {
      epsUpdateType := cr_EPS_UpdateType('0'B, p_EpsUpdate_TypeValue),
      oldGuti       := p_OldGuti,
      lastVisitedRegisteredTai   := p_TAI,
      oldLai                     := p_LAI,
      tmsiStatus                 := p_TmsiStatus
    }
  };
   
  template (present) NAS_UL_Message_Type cdr_TAU_Request_OldGuti_LAI(template (present) EPS_UpdateTypeValue p_EpsUpdate_TypeValue,
                                                                     NAS_KsiValue p_KsiValue,
                                                                     template DRXparameter p_DRXparameter, // @sic R5s120210 sic@
                                                                     template AdditionalUpdateType p_AdditionalUpdateType,
                                                                     template (present) MobileIdentity p_OldGuti,
                                                                     template  LocAreaId p_LAI)
    
    modifies cr_508_TAU_Request :=
  {  /* @status    APPROVED (LTE, LTE_IRAT) */
    tRACKING_AREA_UPDATE_REQUEST := {
      oldGuti            := p_OldGuti,
      oldLai             := p_LAI,
      tmsiStatus         := omit,
      addUpdateType      := p_AdditionalUpdateType
    }
  };
    
  template (present) NAS_UL_Message_Type cdr_TAU_RequestAnyActiveFlag(template (present) EPS_UpdateTypeValue p_EpsUpdate_TypeValue,
                                                                      NAS_KsiValue p_KsiValue,
                                                                      template DRXparameter p_DRXparameter, // @sic R5s120210 sic@
                                                                      template AdditionalUpdateType p_AdditionalUpdateType)
    modifies cr_508_TAU_Request :=
  { /* @status    APPROVED (LTE) */
    tRACKING_AREA_UPDATE_REQUEST := {
      epsUpdateType          := cr_EPS_UpdateType('?'B, p_EpsUpdate_TypeValue),
      epsBearerContextStatus := cr_508_EPS_BearerContextStatusTAUReq(('000'B, '001'B)) ifpresent  // @sic R5-113240 sic@
    }
  };
    
  template (present) NAS_UL_Message_Type cdr_TAU_Request_OldGutiPTMSISig(template (present) EPS_UpdateTypeValue p_EpsUpdate_TypeValue,
                                                                         NAS_KsiValue p_KsiValue,
                                                                         template DRXparameter p_DRXparameter, // @sic R5s120210 sic@
                                                                         template AdditionalUpdateType p_AdditionalUpdateType,
                                                                         template (present) MobileIdentity p_OldGuti,
                                                                         template PTMSI_Signature p_Sig)
    modifies cr_508_TAU_Request :=
  { /* @status    APPROVED (LTE, LTE_IRAT) */
    tRACKING_AREA_UPDATE_REQUEST := {
      oldGuti := p_OldGuti,
      oldPtmsiSignature  := p_Sig
    }
  };
  
  template (value) Nonce cs_Nonce (template (omit) IEI8_Type p_IEI,
                                   O4_Type p_NonceValue) :=
  { /* 24.301 cl. 9.9.3.25 */
    /* @status    APPROVED (LTE_A_IRAT, LTE_IRAT) */
    iei       := p_IEI, /* present in case of TV; omit in case of V */
    nonceValue := p_NonceValue
  };
  
  template (value) NAS_DL_Message_Type cds_SECURITY_MODE_COMMAND_MappedContext(B3_Type p_CipheringType,
                                                                               B3_Type p_IntegrityType,
                                                                               NAS_KsiValue p_AsmeKeySetId,
                                                                               UE_SecurityCapability p_SecurityCap,
                                                                               B32_Type p_NonceUE,
                                                                               B32_Type p_NonceMME ) // @sic R5s100135 sic@
    modifies cs_508_SECURITY_MODE_COMMAND :=
  { /* @status    APPROVED (LTE_A_IRAT, LTE_IRAT) */
    sECURITY_MODE_COMMAND := {
      asmeNasKeySetId            := cs_NAS_KeySetIdentifier_lv(p_AsmeKeySetId, tsc_NasKsi_MappedSecurityContext),
      replayedNonceUe            := cs_Nonce ( '55'O, bit2oct (p_NonceUE)),
      nonceMme                   := cs_Nonce ( '56'O, bit2oct (p_NonceMME))
    }
  };

  template (present) NAS_UL_Message_Type cr_DeactivateEPSBearerCxtAccept(EPS_BearerIdentity p_Id,
                                                                         ProcedureTransactionIdentifier p_ProcTi) :=
  { /* @status    APPROVED (IMS_IRAT, LTE) */
    dEACTIVATE_EPS_BEARER_CONTEXT_ACCEPT := {
      epsBearerId                    := p_Id,
      protocolDiscriminator          := tsc_PD_ESM,
      procedureTransactionIdentifier := p_ProcTi,
      messageType                    := tsc_MT_DeactivateEpsBearerContextAccept,
      protocolConfigurationOptions   := cr_ProtocolConfigOptionsAny ifpresent,
      extdProtocolConfigurationOptions  := cr_ExtdPCOAny ifpresent  // @sic R5s160711 Baseline Moving sic@
    }
  };
  
  //============================================================================
  // Packet filters
  //----------------------------------------------------------------------------
  
  /*
   * @desc      local function to calculate single remote port acc. to reference packet filters in 36.508
   * @param     p_PortNumber
   * @param     p_EPS_BearerId
   * @return    UInt16_Type
   * @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT)
   */
  function fl_SingleRemoteport508(UInt16_Type p_PortNumber,
                               HalfOctet_Type p_EPS_BearerId) return UInt16_Type
  {
    return (p_PortNumber + hex2int(p_EPS_BearerId) - 6);
  }

  template (value) PacketFilterComponent cs_PktFilterCompSingleRemotePort61000(HalfOctet_Type p_EPS_BearerId) :=
  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT) */
    cs_PktFilterCompSingleRemotePort(fl_SingleRemoteport508(61000, p_EPS_BearerId));

  template (value) PacketFilterComponent cs_PktFilterCompSingleRemotePort31160(HalfOctet_Type p_EPS_BearerId) :=
  /* @status    APPROVED (IMS, IMS_IRAT, LTE, LTE_A_IRAT, LTE_A_R10_R11, LTE_A_R12, LTE_IRAT) */
    cs_PktFilterCompSingleRemotePort(fl_SingleRemoteport508(31160, p_EPS_BearerId));
    

}
