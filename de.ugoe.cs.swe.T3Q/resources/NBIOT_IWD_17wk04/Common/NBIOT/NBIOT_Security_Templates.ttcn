/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2017, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_17wk04
// $Date: 2016-12-27 15:21:25 +0100 (Tue, 27 Dec 2016) $
// $Rev: 17691 $
/******************************************************************************/

module NBIOT_Security_Templates {
  import from NBIOT_RRC_ASN1_Definitions language "ASN.1:2002" all;
  import from NBIOT_Imported_EUTRA_ASN1_Types all;
  import from CommonDefs all;
  import from NBIOT_CommonDefs all;
  import from EUTRA_NB_CommonDefs all;
  import from NBIOT_ASP_TypeDefs all;
  import from NBIOT_AspCommon_Templates all;
  
  //****************************************************************************
  // Configuration of AS security
  //----------------------------------------------------------------------------
  
  template (value) NB_PdcpSQN_Type cs_PdcpSQN_SRB(integer p_Value) :=
  {
    Format := PdcpCount_Srb,
    Value  := p_Value
  };
  
  template (value) NB_PDCP_ActTime_Type cs_PDCP_ActTime_SQN (template (value) NB_PdcpSQN_Type p_PDCP_SQN) :=
  {
    SQN := p_PDCP_SQN
  };
  
  template (value) NB_AS_CipheringInfo_Type cs_NB_AS_CipheringInfo(EUTRA_ASN1_CipheringAlgorithm_r12_Type p_Algo,
                                                                   B128_Key_Type p_KeyRRC,
                                                                   B128_Key_Type p_KeyUP,
                                                                   template (value) NB_SecurityActTimeList_Type p_ActList) :=
  { /* @status    APPROVED (NBIOT) */
    Algorithm   := p_Algo,
    KRRCenc     := p_KeyRRC,
    KUPenc      := p_KeyUP,
    ActTimeList := p_ActList
  };
  
  template (value) NB_AS_SecStartRestart_Type cs_NB_AS_IntProtActivate(template (value) NB_AS_IntegrityInfo_Type p_IntInfo) :=
  {
    Integrity := p_IntInfo,
    Ciphering := omit
  };

  template (value) NB_AS_SecStartRestart_Type cs_NB_AS_IntProt_CiphActivate(template (value) NB_AS_IntegrityInfo_Type p_IntInfo,
                                                                            template (value) NB_AS_CipheringInfo_Type p_CiphInfo) :=
  { /* @status    APPROVED (NBIOT) */
    Integrity := p_IntInfo,
    Ciphering := p_CiphInfo
  };
  
  template (value) NB_AS_SecStartRestart_Type cs_NB_AS_CipheringActivate(template (value) NB_AS_CipheringInfo_Type p_CiphInfo) :=
  {
    Integrity := omit,
    Ciphering := p_CiphInfo
  };
  
  template (value) NB_SYSTEM_CTRL_REQ cas_NB_AsSecurityStart_REQ(NBIOT_CellId_Type p_CellId,
                                                                 template (value) NB_AS_SecStartRestart_Type p_AS_SecStartRestart,
                                                                 template (value) TimingInfo_Type p_TimingInfo := cs_TimingInfo_Now,
                                                                 template (omit) boolean p_CnfFlag := omit) :=
  { /* @status    APPROVED (NBIOT) */
    /* Note: when p_TimingInfo is not cs_TimingInfo_Now p_CnfFlag shall be set to tsc_NoCnfReq */
    Common := cs_NB_ReqAspCommonPart_CellCfg(p_CellId, p_TimingInfo, p_CnfFlag),
    Request := {
      AS_Security := {
        StartRestart := p_AS_SecStartRestart
      }
    }
  };
  
  template (value) NB_SYSTEM_CTRL_REQ cas_NB_AsSecurityRelease_REQ(NBIOT_CellId_Type p_CellId,
                                                             template (value) TimingInfo_Type p_TimingInfo := cs_TimingInfo_Now,
                                                             template (omit) boolean p_CnfFlag := omit) :=
  { /* @status    APPROVED (NBIOT) */
    Common := cs_NB_ReqAspCommonPart_CellCfg(p_CellId, p_TimingInfo, p_CnfFlag),
    Request := {
      AS_Security := {
        Release := true
      }
    }
  };
  
  template (present) NB_SYSTEM_CTRL_CNF car_NB_AsSecurity_CNF(NBIOT_CellId_Type p_CellId) :=
  { /* @status    APPROVED (NBIOT) */
    Common := cr_NB_CnfAspCommonPart_CellCfg(p_CellId),
    Confirm := {
      AS_Security := true
    }
  };
  
  //----------------------------------------------------------------------------
  // Enquiry of PDCP COUNT values
  
  template (value) NB_PDCP_CountReq_Type cs_NB_PDCP_CountSet(NB_PdcpCountInfoList_Type p_PdcpCountInfoList) :=
  {
    Set := p_PdcpCountInfoList
  };

  template (value) NB_PDCP_CountReq_Type cs_NB_PDCP_CountGet_AllRBs  :=
  { /* @status    APPROVED (NBIOT) */
    Get := {
      AllRBs := true
    }
  };

  template (value) NB_SYSTEM_CTRL_REQ cas_NB_PdcpCount_REQ(NBIOT_CellId_Type p_CellId,
                                                           template (value) NB_PDCP_CountReq_Type p_CountReq,
                                                           template (value) TimingInfo_Type p_TimingInfo := cs_TimingInfo_Now) :=
  { /* @status    APPROVED (NBIOT) */
    Common  := cs_NB_ReqAspCommonPart_CellCfg(p_CellId, p_TimingInfo),
    Request := {
      PdcpCount := p_CountReq
    }
  };

  template (present) NB_SYSTEM_CTRL_CNF car_NB_PdcpCount_CNF(NBIOT_CellId_Type p_CellId,
                                                             template (present) NB_PDCP_CountCnf_Type p_PDCP_CountCnf) :=
  { /* @status    APPROVED (NBIOT) */
    Common := cr_NB_CnfAspCommonPart_CellCfg(p_CellId),
    Confirm := {
      PdcpCount := p_PDCP_CountCnf
    }
  };
  
  template (present) NB_PdcpCountInfo_Type cr_NB_PdcpCountInfo_DRB(template (present) EUTRA_ASN1_DRB_Identity_Type p_Drb) :=
  {
    RadioBearerId := {Drb := p_Drb},
    UL := *,
    DL := *
  };
  template (present) NB_PdcpCountInfo_Type cr_NB_PdcpCountInfo_SRB(template (present) NB_SRB_Identity_Type p_Srb) :=
  {
    RadioBearerId := {Srb := p_Srb},
    UL := *,
    DL := *
  };
  //****************************************************************************
  // Configuration of NAS security
  //----------------------------------------------------------------------------
  

}
