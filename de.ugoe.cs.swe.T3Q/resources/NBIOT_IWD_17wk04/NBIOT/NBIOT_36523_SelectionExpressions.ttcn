/******************************************************************************/
// @copyright   Copyright Notification
//   No part may be reproduced except as authorized by written permission.
//   The copyright and the foregoing restriction extend to reproduction in all media.
//   Trademark 2017, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
//   All rights reserved.
// @version: IWD_17wk04
// $Date: 2016-12-24 13:03:54 +0100 (Sat, 24 Dec 2016) $
// $Rev: 17690 $
/******************************************************************************/

module NBIOT_36523_SelectionExpressions {
  import from NBIOT_Parameters all;
  import from EUTRA_NB_Parameters all;
  
  type enumerated NBIOT_36523_Condition_Type {  /* @status    APPROVED (NBIOT) */
    //TCSE from 36.523-2 specific to NBIOT
    C266,
    C272
  }

  /*
   * @desc      Set the applicability conditions according to 36.523-2 Table 4-1a: Applicability of tests Conditions
   *            Function specific to NBIOT  TCSE
   * @param     p_Condition
   * @return    boolean
   * @status    APPROVED (NBIOT)
   */
  function f_SelectionExpr(NBIOT_36523_Condition_Type p_Condition) return boolean
  {
    var boolean v_ApplCond;
   
    select (p_Condition) {
      //UEs supporting NB-IoT
      case(C266){
        v_ApplCond := pc_NB;
      }
      //UEs supporting NB-IoT and ZUC algorithms
      case(C272){
        v_ApplCond := pc_NB and pc_ZUC;
      }
    }
    return v_ApplCond;
  }
  
}
