package de.ugoe.cs.swe.T3Q;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Diagnostic;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.validation.FeatureBasedDiagnostic;

import com.google.common.base.Stopwatch;

import de.ugoe.cs.swe.common.MiscTools;
import de.ugoe.cs.swe.common.logging.LoggingInterface;
import de.ugoe.cs.swe.common.logging.LoggingInterface.MessageClass;
import de.ugoe.cs.swe.validation.TTCN3StatisticsProvider;

public class Analyzer implements Callable<TTCN3Output> {
	private final Resource resource;
	private final LoggingInterface logger;
	public static boolean printUnresolvedObjects = false;

	public Analyzer(Resource resource, LoggingInterface logger) {
		this.resource = resource;
		this.logger = logger;
	}

	private TTCN3Output analyze() {
		final TTCN3Output output = new TTCN3Output(this.resource);
		final Stopwatch stopwatch = Stopwatch.createUnstarted();

		stopwatch.start();

		// validate the resource
		EcoreUtil.resolveAll(this.resource);
		EObject model = this.resource.getContents().get(0);
		org.eclipse.emf.common.util.Diagnostic diagnostic = null;
		diagnostic = Diagnostician.INSTANCE.validate(model);
		for (org.eclipse.emf.common.util.Diagnostic d : diagnostic.getChildren()) {
			if (d instanceof FeatureBasedDiagnostic) {
				output.getOutput().add((FeatureBasedDiagnostic) d);
			} else {
				//System.out.println(d);
			}
		}

		// TODO: make this configurable for debug purposes
		if (printUnresolvedObjects) {
			for (Diagnostic d : this.resource.getErrors()) {
				TTCN3StatisticsProvider.getInstance().incrementCountUniversal();
				logger.logInformation(this.resource.getURI(), d.getLine(), d.getLine(), MessageClass.UNIVERSAL,
						d.getMessage());
			}
			for (Diagnostic d : this.resource.getWarnings()) {
				TTCN3StatisticsProvider.getInstance().incrementCountUniversal();
				logger.logInformation(this.resource.getURI(), d.getLine(), d.getLine(), MessageClass.UNIVERSAL,
						d.getMessage());
			}
		}

		stopwatch.stop();

		System.out.println("Analyzing  file: " + this.resource.getURI().devicePath().replaceFirst("///", "") + '\n'
				+ "       ...done in " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + " ms ("
				+ MiscTools.secondsToString(stopwatch.elapsed(TimeUnit.SECONDS)) + " minutes).");

		return output;
	}

	@Override
	public TTCN3Output call() throws Exception {
		return analyze();
	}
}
