package de.ugoe.cs.swe.T3Q;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Diagnostic;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.ISelectable;
import org.eclipse.xtext.resource.SynchronizedXtextResourceSet;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.scoping.Scopes;
import org.eclipse.xtext.scoping.impl.MultimapBasedSelectable;

import com.google.common.collect.Lists;

import de.ugoe.cs.swe.common.MiscTools;
import de.ugoe.cs.swe.common.logging.LoggingInterface;
import de.ugoe.cs.swe.common.logging.LoggingInterface.MessageClass;
import de.ugoe.cs.swe.scoping.TTCN3GlobalScopeProvider;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.validation.TTCN3StatisticsProvider;

public class FileParser implements Callable<Resource> {
	private final File file;
	private  final IQualifiedNameProvider qualifiedNameProvider;
	private final LoggingInterface logger;
	public static boolean printParsingErrors = false;

	public FileParser(final File file, final IQualifiedNameProvider qualifiedNameProvider, LoggingInterface logger) {
		this.file = file;
		this.qualifiedNameProvider = qualifiedNameProvider;
		this.logger = logger;
	}

	private Resource parseFile() {
		final SynchronizedXtextResourceSet resourceSet = new SynchronizedXtextResourceSet();
		resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.FALSE);

		final long millisStart = System.currentTimeMillis();
		
		Resource res;
		int lines = 0;
		try {
			lines = T3Q.countLines(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		TTCN3StatisticsProvider.getInstance().addParsedLines(lines);

		String path = file.getAbsolutePath();
		res = resourceSet.getResource(URI.createURI("file:///" + path), true);
				
		EList<Diagnostic> errors = res.getErrors();
		if (printParsingErrors) {
			for (Diagnostic d : errors) {
				TTCN3StatisticsProvider.getInstance().incrementCountUniversal();
				logger.logFix(res.getURI(), d.getLine(), d.getLine(), MessageClass.UNIVERSAL,
						d.getMessage());
			}
		}

		initExportedObjects(res);
		
		final long millisEnd = System.currentTimeMillis();
		
		final long millis = millisEnd -  millisStart;
		
		System.out.println("Parsing file: " + file.getPath() + " (LOC: " + lines + ")" + '\n' +"       ...done in "
						+ millis + "ms (" + MiscTools.msToString(millis) + " minutes).");
		
		return res;
	}
	
	public ISelectable internalGetAllDescriptions(final Resource resource) {
		Iterable<EObject> allContents = new Iterable<EObject>() {
			public Iterator<EObject> iterator() {
				return EcoreUtil.getAllContents(resource, false);
			}
		};
		Iterable<IEObjectDescription> allDescriptions = Scopes.scopedElementsFor(allContents, qualifiedNameProvider);
		return new MultimapBasedSelectable(allDescriptions);
	}		
	
	private void initExportedObjects(Resource resource) {
		List<IEObjectDescription> objects = Lists.newArrayList();
		
		List<IEObjectDescription> exports = Lists.newArrayList(internalGetAllDescriptions(resource).getExportedObjects());
		int size = exports.size();
		
		for (int i = 0; i < size; i++) {
			IEObjectDescription e = exports.get(i);
			IEObjectDescription d = null;
			
			if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getTTCN3Module(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getGroupDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getEnumDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getEnumeration(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getComponentDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getPortDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getRecordDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getSetDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getSubTypeDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getUnionDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getRecordOfDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getSetOfDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getSignatureDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getBaseTemplate(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getFunctionRef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getAltstepDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getTestcaseDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getSingleConstDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getModuleParameter(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			}
		}	
		
		synchronized (TTCN3GlobalScopeProvider.EXPORTED_OBJECTS) {
			TTCN3GlobalScopeProvider.EXPORTED_OBJECTS.putAll(resource, objects);
		}
		
	}
	
	@Override
	public Resource call() throws Exception {
		return parseFile();
	}
}
