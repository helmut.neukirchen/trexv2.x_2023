package de.ugoe.cs.swe.T3Q;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.ParseException;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;

import de.ugoe.cs.swe.TTCN3Configuration.QualityCheckProfile;
import de.ugoe.cs.swe.common.ConfigTools;
import de.ugoe.cs.swe.common.MiscTools;
import de.ugoe.cs.swe.common.T3QOptionsHandler;
import de.ugoe.cs.swe.common.exceptions.TerminationException;
import de.ugoe.cs.swe.common.logging.LoggingInterface;
import de.ugoe.cs.swe.common.logging.LoggingInterface.LogLevel;
import de.ugoe.cs.swe.scoping.TTCN3GlobalScopeProvider;

public class T3Q {
	private static String versionNumber = "v2.0.0b30";
	private static String supportedTTCN3Version = "4.6.1";
	// set during automated server builds
	private static String buildStamp = "---BUILD_STAMP---";
	public static QualityCheckProfile activeProfile = null;
	private String configurationFilename;
	private String selectedProfileName = null;
	private static LogLevel logLevel = LogLevel.INFORMATION;
	private ArrayList<String> inputPaths = new ArrayList<String>();
	private String destinationPath = null;
	private boolean generateNewConfiguration = false;
	private boolean generateLocalDependencies = false;
	private final Stopwatch stopwatch = Stopwatch.createUnstarted();
	
	// private boolean formattingEnabled = false;

	// TODO: also add exception checking for the profiles
	public T3Q() {
	}

	public void showHelp() {
		System.out.println("\nHelp:");
		// TODO: update help
		// System.out.println("  t3q[.bat] [options] (path | filename)+");
		// System.out.println("");
		// System.out.println("  Options (specify in any order): ");
		// System.out.println("    --help - prints this screen");
		// System.out
		// .println("    --profile [profilename] - allows manual profile selection, overriding the selected default profile");
		// // System.out.println("    --output [path] ");
		// System.out.println("");

		HelpFormatter formatter = new HelpFormatter();
		formatter.setOptionComparator(new Comparator<Option>() {

			@Override
			public int compare(Option o1, Option o2) {
				if (o1.getId() > o2.getId()) {
					return 1;
				} else {
					return -1;
				}
			}
		});
		// formatter.setWidth(120);
		formatter.setSyntaxPrefix("  Usage: ");
		formatter.printHelp("t3q [options] (filename | path)+", "  Options: (in any order, config is required)",
				new T3QOptionsHandler().getOptions(), "");
		System.out.println("");
	}

	public void run(String[] args) {
		stopwatch.start();
		
		System.out.println("T3Q " + getVersionNumber());
		//System.out.println("Build " + getBuildStamp());
		System.out.println("TTCN-3 version supported: " + supportedTTCN3Version);
		System.out.println("==========================================");
		try {
			if (handleCommandLineArguments(args) == false) {
				return;
			}
			showDebugInfo(args);
			if (selectedProfileName != null) {
				handleConfig(selectedProfileName);
			} else {
				handleConfig(null);
			}
		} catch (TerminationException e) {
			// TODO: handle exception
			System.out.println("ERRORING OUT!");
		}

		LoggingInterface logger = new LoggingInterface(activeProfile.getLoggingConfiguration());
		logger.setMaximumLogLevel(logLevel);
		TTCN3ResourceProvider resourceProvider = new TTCN3ResourceProvider(inputPaths, logger, activeProfile);
		TTCN3GlobalScopeProvider.LIVE = false;
		resourceProvider.loadResources();
		//TODO: check if it has to be exclusive
		if (!this.isGenerateLocalDependencies()) {
			resourceProvider.analyzeResources();
		} else {
			resourceProvider.analyzeDependencies();
		}

		if (activeProfile.isGenerateXMI())
			resourceProvider.createXMI();

		resourceProvider.printStatistics();
		
		stopwatch.stop();
		System.out.println("Total processing time: "
				+ MiscTools.msToString(stopwatch.elapsed(TimeUnit.MILLISECONDS)) + " minutes." + '\n');			
	}

	public static void main(String[] args) {
		try {
			T3Q tool = new T3Q();
			tool.run(args);
		} catch (Exception e) {
			if (getLogLevel() == LogLevel.DEBUG) {
				e.printStackTrace();
			} else {
				String stacktrace = "";
				for (StackTraceElement ste : e.getStackTrace()) {
					stacktrace += "\n    " + ste.toString();
				}
				System.err.println("ERROR: A problem occurred while running T3Q" + "\n  Problem type: " + e + "\n  Stacktrace:"
						+ stacktrace + "\n  Run T3Q with --verbosity=DEBUG for a more detailed report");
			}
		}
	}

	private boolean handleCommandLineArguments(String[] args) throws TerminationException {
		// parseCommandLineArguments(args);
		CommandLine commandLine = parseCommandLineArguments(args);
		if (commandLine == null) {
			return false;
		}
		String arguments[] = evaluateCommandLineOptions(commandLine);

		if (commandLine.hasOption("help")) {
			showHelp();
			return false;
		}

		if (commandLine.hasOption("config") && arguments.length < 1) {
			System.out.println("ERROR: Missing input location(s)");
			showHelp();
			return false;
		}

		for (String arg : arguments) {
			// TODO: add validity checks
			inputPaths.add(arg);
		}
		return true;
	}

	private CommandLine parseCommandLineArguments(String[] args) {
		CommandLineParser parser = new GnuParser();
		T3QOptionsHandler optionsHandler = new T3QOptionsHandler();
		CommandLine commandLine = null;
		try {
			commandLine = parser.parse(optionsHandler.getOptions(), args);
		} catch (ParseException e) {
			System.out.println("ERROR: " + e.getMessage());
			showHelp();
		}
		return commandLine;
	}

	private String[] evaluateCommandLineOptions(CommandLine commandLine) throws TerminationException {
		if (commandLine.hasOption("generate-config")) {
			this.setConfigurationFilename(commandLine.getOptionValue("generate-config"));
			this.setGenerateNewConfiguration(true);
		} else if (commandLine.hasOption("config")) {
			this.setConfigurationFilename(commandLine.getOptionValue("config"));
		} else {
			System.out.println("ERROR: No configuration file selected!");
			showHelp();
			throw new TerminationException("");
		}

		if (commandLine.hasOption("profile")) {
			this.setSelectedProfileName(commandLine.getOptionValue("profile"));
		}
		if (commandLine.hasOption("verbosity")) {
			this.selectLogLevel(commandLine.getOptionValue("verbosity"));
		}
		if (commandLine.hasOption("output-path")) {
			this.setDestinationPath(commandLine.getOptionValue("output-path"));
		}
		if (commandLine.hasOption("local-dependencies")) {
			this.setGenerateLocalDependencies(true);
		} 
		return commandLine.getArgs();
	}

	private void handleConfig(String specifiedProfile) throws TerminationException {
		ConfigTools configTools = ConfigTools.getInstance();
		configTools.setToolVersion(getVersionNumber());

		try {
			if (isGenerateNewConfiguration()) {
				configTools.initializeNewDefaultConfig(getConfigurationFilename());
				System.exit(0);
			} else {
				configTools.loadConfig(getConfigurationFilename());
				activeProfile = (QualityCheckProfile) configTools.selectProfile(specifiedProfile);
				if (this.getDestinationPath() == null) {
					if (this.isGenerateLocalDependencies()) {
						setDestinationPath(T3Q.activeProfile.getDependencyOutputPath());
					} else {
						setDestinationPath(T3Q.activeProfile.getPathFormattedOutputPath());
					}
				} else {
					if (this.isGenerateLocalDependencies()) {
						T3Q.activeProfile.setDependencyOutputPath(this.getDestinationPath());
					} else {
						T3Q.activeProfile.setPathFormattedOutputPath(this.getDestinationPath());
					}
				}
			}
		} catch (InstantiationException e) {
			throw new TerminationException("ERROR: Instantiation problems encountered while loading configuration profile. "
					+ e.getMessage());
		} catch (IllegalAccessException e) {
			throw new TerminationException("ERROR: Instantiation problems encountered while loading configuration profile. "
					+ e.getMessage());
		} catch (ClassNotFoundException e) {
			throw new TerminationException("ERROR: Instantiation problems encountered while loading configuration profile. "
					+ e.getMessage());
		}

		if (!isProfileVersionValid()) {
			System.out
					.println("\nERROR: Selected profile \""
							+ activeProfile.getProfileName()
							+ "\" has a mismatching or no version (required: \""
							+ getVersionNumber()
							+ "\").\n"
							+ "  Consider upgrading the profile by transfering the relevant parts to an auto-generated profile or selecting a different profile.\n");
			throw new TerminationException("");
		}
	}

	private boolean isProfileVersionValid() {
		if (activeProfile.getProfileVersion() != null && activeProfile.getProfileVersion().equals(getVersionNumber())) {
			return true;
		} else {
			return false;
		}
	}

	private void selectLogLevel(String logLevel) throws TerminationException {
		boolean selected = false;
		String possibleValues = "";
		for (LogLevel l : LogLevel.values()) {
			if (l.toString().equals(logLevel)) {
				setLogLevel(l);
				selected = true;
				System.out.println("Selected log level \"" + logLevel + "\"");
			}
			if (!l.toString().equals("FIXME") && !l.toString().equals("DEBUG"))
				possibleValues += l.toString() + ", ";
				FileParser.printParsingErrors = true;
				Analyzer.printUnresolvedObjects = true;
		}
		if (!selected) {
			System.out.println("\nERROR: No valid log level provided! Possible values are (in ascending inclusive order): "
					+ possibleValues.substring(0, possibleValues.length() - 2) + ".");
			throw new TerminationException("");
		}

	}

	private void showDebugInfo(String[] args) {
		if (getLogLevel().equals(LogLevel.DEBUG)) {
			System.out.println("==================ARGS:===================");
			for (int a = 0; a < args.length; a++) {
				System.out.println("   " + args[a]);
			}
			System.out.println("==================PROPERTIES:=============");
			for (Object key : System.getProperties().keySet()) {
				System.out.println("   " + key.toString() + " : " + System.getProperty(key.toString()));
			}

			System.out.println("==========================================");
		}
	}

	public static void setVersionNumber(String versionNumber) {
		T3Q.versionNumber = versionNumber;
	}

	public static String getVersionNumber() {
		return versionNumber;
	}

	public void setConfigurationFilename(String configurationFilename) {
		this.configurationFilename = configurationFilename;
	}

	public String getConfigurationFilename() {
		return configurationFilename;
	}

	public static void setLogLevel(LogLevel logLevel) {
		T3Q.logLevel = logLevel;
	}

	public static LogLevel getLogLevel() {
		return logLevel;
	}

	public void setSelectedProfileName(String selectedProfileName) {
		this.selectedProfileName = selectedProfileName;
	}

	public String getSelectedProfileName() {
		return selectedProfileName;
	}

	public void setGenerateNewConfiguration(boolean generateNewConfiguration) {
		this.generateNewConfiguration = generateNewConfiguration;
	}

	public boolean isGenerateNewConfiguration() {
		return generateNewConfiguration;
	}

	public void setDestinationPath(String destinationPath) {
		this.destinationPath = destinationPath;
	}

	public String getDestinationPath() {
		return destinationPath;
	}

	public static void setBuildStamp(String buildStamp) {
		T3Q.buildStamp = buildStamp;
	}

	public static String getBuildStamp() {
		return buildStamp;
	}
	
	private static List<String> readFile(String filename) throws FileNotFoundException, IOException {	
		List<String> file = Lists.newArrayList();
		FileInputStream stream = new FileInputStream(filename);
		//InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
		InputStreamReader reader = new InputStreamReader(stream);
		BufferedReader buffer = new BufferedReader(reader);
		String line = (buffer.readLine());

		while (line != null) {
			file.add(line);
			line = (buffer.readLine());
		}

		buffer.close();
		reader.close();
		stream.close();
		
		return file;
	}	
	
	private static void findTabs(String filename, List<String> file) {
		if (!activeProfile.isCheckNoTabs()) {
			return;
		}
		
		for (int i = 0; i < file.size(); i++) {
			String line = file.get(i);
			for (int j = 0; j < line.length(); j++) {
				if (line.charAt(j) == '\t') {
					String[] position = {Integer.toString(i+1), Integer.toString(j)};
					if (TTCN3GlobalScopeProvider.FOUND_TABS.containsKey(filename)) {
						TTCN3GlobalScopeProvider.FOUND_TABS.get(filename).add(position);
					} else {
						List<String[]> list = Lists.newArrayList();
						list.add(position);
						TTCN3GlobalScopeProvider.FOUND_TABS.put(filename, list);
					}
				}
			}
		}
	}
	
	/**
	 * This method counts the lines of a file. Furthermore, it executes methods to find tabs
	 * in the current file. Found tabs will be stored in TTCN3GlobalScopeProvider.FOUND_TABS.
	 */
	public static int countLines(File data) throws IOException {
		List<String> file = readFile(data.getPath());
		// This only works with unique file names, but this assumption should be fulfilled
		// otherwise the path should be hashed
		findTabs(data.getName(), file);
		if (file != null) {
			return file.size();
		} else {
			return 0;
		}
	}

	public boolean isGenerateLocalDependencies() {
		return generateLocalDependencies;
	}

	public void setGenerateLocalDependencies(boolean generateLocalDependencies) {
		this.generateLocalDependencies = generateLocalDependencies;
	}
}
