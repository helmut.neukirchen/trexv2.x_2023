package de.ugoe.cs.swe.T3Q;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.validation.FeatureBasedDiagnostic;

import com.google.common.base.Stopwatch;
import com.google.inject.Inject;
import com.google.inject.Injector;

import de.ugoe.cs.swe.TTCN3StandaloneSetup;
import de.ugoe.cs.swe.TTCN3Configuration.QualityCheckProfile;
import de.ugoe.cs.swe.common.CommonHelper;
import de.ugoe.cs.swe.common.MiscTools;
import de.ugoe.cs.swe.common.PreAnalyzer;
import de.ugoe.cs.swe.common.logging.LoggingInterface;
import de.ugoe.cs.swe.common.logging.LoggingInterface.LogLevel;
import de.ugoe.cs.swe.common.logging.LoggingInterface.MessageClass;
import de.ugoe.cs.swe.scoping.TTCN3GlobalScopeProvider;
import de.ugoe.cs.swe.tTCN3.TTCN3Module;
import de.ugoe.cs.swe.validation.TTCN3StatisticsProvider;

public class TTCN3ResourceProvider {
	private ArrayList<String> paths;
	private LoggingInterface logger;
	private QualityCheckProfile activeProfile;
	private final int cores = Runtime.getRuntime().availableProcessors();
	
	@Inject
	private IQualifiedNameProvider qualifiedNameProvider;
	
	private final Stopwatch stopwatch = Stopwatch.createUnstarted();

	public TTCN3ResourceProvider(final ArrayList<String> paths,
			LoggingInterface logger, QualityCheckProfile activeProfile) {
		super();
		this.paths = paths;
		this.logger = logger;
		this.activeProfile = activeProfile;
	}

	public void loadResources() {
		// new org.eclipse.emf.mwe.utils.StandaloneSetup().setPlatformUri(path);
		Injector injector = new TTCN3StandaloneSetup()
				.createInjectorAndDoEMFRegistration();
		injector.injectMembers(this);
		
		stopwatch.start();
		
		ExecutorService pool = Executors.newFixedThreadPool(cores);
		ArrayList<FileParser> parser = new ArrayList<FileParser>();
		
		for (String path : paths) {
			File file = new File(path);
						
			if (file.isFile()) {
				parser.add(new FileParser(file, qualifiedNameProvider, logger));
			} else if (file.isDirectory()) {
				Collection<File> files = FileUtils.listFiles(file,
						new RegexFileFilter("^.*ttcn3$|^.*ttcn$|^.*3mp$"),
						DirectoryFileFilter.DIRECTORY);
				for (File f : files) {
					parser.add(new FileParser(f, qualifiedNameProvider, logger));
				}
			}
		}
		
		try {
			List<Future<Resource>> list = pool.invokeAll(parser);
			for (Future<Resource> f : list) {
				Resource r = f.get();
				TTCN3GlobalScopeProvider.RESOURCES.add(r);
				TTCN3Module module = CommonHelper.getModule(r);
				TTCN3GlobalScopeProvider.NAMED_MODULES.put(module.getName(), module);
			}
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// TODO: extract this to own step
		ArrayList<PreAnalyzer> preAnalyzer = new ArrayList<PreAnalyzer>();
		for (int i = 0; i < TTCN3GlobalScopeProvider.RESOURCES.size(); i++) {
			preAnalyzer.add(new PreAnalyzer(TTCN3GlobalScopeProvider.RESOURCES.get(i)));
		}
		try {
			pool.invokeAll(preAnalyzer);
			pool.shutdown();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		TTCN3StatisticsProvider.getInstance().setPreAnalyzingCompleted(true);
		
		stopwatch.stop();
		
		TTCN3StatisticsProvider.getInstance().setParsingCompleted(true);
		System.out.println("Test suite parsed in "
				+ MiscTools.msToString(stopwatch.elapsed(TimeUnit.MILLISECONDS)) + " minutes." + '\n');
	}

	public void analyzeResources() {
		Stopwatch watchAnalyzing = Stopwatch.createStarted();
		ExecutorService pool = Executors.newFixedThreadPool(cores);
		
		// Check if resource is a ignored one and if so, do not analyze it.
		Pattern pattern = Pattern.compile(activeProfile.getIgnoredResourceRegExp());
		Matcher matcher = null;		
		
		ArrayList<Analyzer> analyzer = new ArrayList<Analyzer>();
		for (int i = 0; i < TTCN3GlobalScopeProvider.RESOURCES.size(); i++) {
			Resource resource = TTCN3GlobalScopeProvider.RESOURCES.get(i);
			matcher = pattern.matcher(resource.getURI().toFileString());
			if(!matcher.matches()) {
				analyzer.add(new Analyzer(TTCN3GlobalScopeProvider.RESOURCES.get(i), logger));
			}
		}
		try {
			List<Future<TTCN3Output>> output = pool.invokeAll(analyzer);
			pool.shutdown();
			System.out.print('\n');
			for (Future<TTCN3Output> f : output) {
				TTCN3Output o = f.get();
				System.out.println("Validation output of " + o.getResource().getURI().lastSegment());
				for (FeatureBasedDiagnostic d : o.getOutput()) {
					printOutput(o.getResource(), d);
				}
			}
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
		
		if (activeProfile.isFeatureListImportedModuleNames()) {
			TTCN3GlobalScopeProvider.printImportedModuleNames(false, logger);
		}

		if (activeProfile.isFeatureListImportedModuleFileNames()) {
			TTCN3GlobalScopeProvider.printImportedModuleNames(true, logger);
		}

		if (activeProfile.isFeatureListImportingModuleNames()) {
			TTCN3GlobalScopeProvider.printImportingModuleNames(false, logger);
		}

		if (activeProfile.isFeatureListImportingModuleFileNames()) {
			TTCN3GlobalScopeProvider.printImportingModuleNames(true, logger);
		}
		
		watchAnalyzing.stop();
		System.out.println("Test suite analyzed in "
				+ MiscTools.msToString(watchAnalyzing.elapsed(TimeUnit.MILLISECONDS)) + " minutes." + '\n');
	}

	public void analyzeDependencies() {
		Stopwatch watchAnalyzing = Stopwatch.createStarted();
		ExecutorService pool = Executors.newFixedThreadPool(cores);
		
		// Check if resource is a ignored one and if so, do not analyze it.
		Pattern pattern = Pattern.compile(activeProfile.getIgnoredResourceRegExp());
		Matcher matcher = null;		
		
		ArrayList<DependencyAnalyzer> analyzer = new ArrayList<DependencyAnalyzer>();
		for (int i = 0; i < TTCN3GlobalScopeProvider.RESOURCES.size(); i++) {
			Resource resource = TTCN3GlobalScopeProvider.RESOURCES.get(i);
			matcher = pattern.matcher(resource.getURI().toFileString());
			if(!matcher.matches()) {
				analyzer.add(new DependencyAnalyzer(TTCN3GlobalScopeProvider.RESOURCES.get(i), logger));
			}
		}
		try {
			List<Future<TTCN3Dependency>> output = pool.invokeAll(analyzer);
			pool.shutdown();
			System.out.print('\n');
			
			for (Future<TTCN3Dependency> f : output) {
				TTCN3Dependency o = f.get();
				System.out.println("Dependency analysis output of " + o.getResource().getURI().lastSegment());
				for (String d : o.getMessages()) {
					printOutput(o.getResource(), d);
				}
			}

			String xmlpath = "dependencies.xml";
			File parent = new File(this.activeProfile.getDependencyOutputPath());
			parent.mkdirs();
			xmlpath = parent.getAbsolutePath()+File.separator+xmlpath;
			FileOutputStream file;
			try {
				file = new FileOutputStream(xmlpath);
				PrintStream stream = new PrintStream(file);
				stream.print("<?xml-stylesheet type=\"text/xsl\" href=\"../dependencies.xsl\"?>\n<dependencies>\n");
				//TODO: check if it is faster to do this all at once
				for (Future<TTCN3Dependency> f : output) {
					for (String d : f.get().getOutput()) {
						stream.print(d);
					}
				}
				stream.print("</dependencies>");
				stream.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		watchAnalyzing.stop();
		System.out.println("Test suite analyzed in "
				+ MiscTools.msToString(watchAnalyzing.elapsed(TimeUnit.MILLISECONDS)) + " minutes." + '\n');

	}

	private void printOutput(Resource resource, String message) {
		System.out.println(message);
	}
	
	
	
	private void printOutput(Resource resource, FeatureBasedDiagnostic featureDiagnostic) {
			switch (featureDiagnostic.getSeverity()) {
			case org.eclipse.emf.common.util.Diagnostic.ERROR:
				logger.logError(resource.getURI(),
						Integer.parseInt(featureDiagnostic.getIssueData()[0]),
						Integer.parseInt(featureDiagnostic.getIssueData()[1]),
						MessageClass.valueOf(featureDiagnostic.getIssueCode()),
						featureDiagnostic.getMessage());
				break;
			case org.eclipse.emf.common.util.Diagnostic.WARNING:
				logger.logWarning(resource.getURI(),
						Integer.parseInt(featureDiagnostic.getIssueData()[0]),
						Integer.parseInt(featureDiagnostic.getIssueData()[1]),
						MessageClass.valueOf(featureDiagnostic.getIssueCode()),
						featureDiagnostic.getMessage(),
						featureDiagnostic.getIssueData()[2]);
				break;
			case org.eclipse.emf.common.util.Diagnostic.INFO:
				switch (LogLevel.valueOf(featureDiagnostic.getIssueData()[3])) {
				case INFORMATION:
					logger.logInformation(
							resource.getURI(),
							Integer.parseInt(featureDiagnostic.getIssueData()[0]),
							Integer.parseInt(featureDiagnostic.getIssueData()[1]),
							MessageClass.valueOf(featureDiagnostic
									.getIssueCode()), featureDiagnostic
									.getMessage(), featureDiagnostic
									.getIssueData()[2]);
					break;
				case FIXME:
					logger.logFix(
							resource.getURI(),
							Integer.parseInt(featureDiagnostic.getIssueData()[0]),
							Integer.parseInt(featureDiagnostic.getIssueData()[1]),
							MessageClass.valueOf(featureDiagnostic
									.getIssueCode()), featureDiagnostic
									.getMessage(), featureDiagnostic
									.getIssueData()[2]);
					break;
				case DEBUG:
					logger.logDebug(
							resource.getURI(),
							Integer.parseInt(featureDiagnostic.getIssueData()[0]),
							Integer.parseInt(featureDiagnostic.getIssueData()[1]),
							MessageClass.valueOf(featureDiagnostic
									.getIssueCode()), featureDiagnostic
									.getMessage(), featureDiagnostic
									.getIssueData()[2]);
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
		}
	

	public void printStatistics() {
		if (activeProfile.isStatShowLOC())
			System.out.println("Total lines of code processed: "
					+ TTCN3StatisticsProvider.getInstance().getParsedLines());

		if (activeProfile.isStatShowSummary()) {
			System.out
					.println("Brief statistics summary of occurences in message classes:");
			System.out
					.println('\t'
							+ "Universal: "
							+ TTCN3StatisticsProvider.getInstance()
									.getCountUniversal());
			System.out.println('\t' + "General: "
					+ TTCN3StatisticsProvider.getInstance().getCountGeneral());
			System.out.println('\t' + "Naming Conventions: "
					+ TTCN3StatisticsProvider.getInstance().getCountNaming());
			System.out.println('\t'
					+ "Code Documentation: "
					+ TTCN3StatisticsProvider.getInstance()
							.getCountDocumentation());
			System.out.println('\t' + "Log Statements: "
					+ TTCN3StatisticsProvider.getInstance().getCountLog());
			System.out
					.println('\t'
							+ "Structure of Data: "
							+ TTCN3StatisticsProvider.getInstance()
									.getCountStructure());
			System.out.println('\t' + "Code Style: "
					+ TTCN3StatisticsProvider.getInstance().getCountStyle());
			System.out.println('\t'
					+ "Test Suite Modularization: "
					+ TTCN3StatisticsProvider.getInstance()
							.getCountModularization());
		}
	}

	public void createXMI() {
		for (Resource r : TTCN3GlobalScopeProvider.RESOURCES) {
			String extension = fileExtensionOrPath(r.getURI().devicePath(),
					true) + "m";
			storeResourceContents(r.getContents(), r.getURI().devicePath()
					+ "m", extension);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void storeResourceContents(EList<EObject> contents,
			String outputPath, String extension) {
		// TODO: duplicated from loadResourceFromXMI => move to a more
		// appropriate location
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put(extension, new XMIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();
		Resource outputResource = resSet.createResource(URI
				.createURI(outputPath));
		outputResource.getContents().addAll(contents);
		try {
			Map options = new HashMap<>();
			options.put(XMIResourceImpl.OPTION_ENCODING, "UTF-8");
			// options.put(XMIResourceImpl.OPTION_PROCESS_DANGLING_HREF,
			// XMIResourceImpl.OPTION_PROCESS_DANGLING_HREF_DISCARD);
			outputResource.save(options);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String fileExtensionOrPath(String filename, boolean extension) {
		String[] res = filename.split("\\.");
		int size = res.length;

		if (extension) {
			return res[size - 1];
		} else {
			return res[0];
		}
	}
}
