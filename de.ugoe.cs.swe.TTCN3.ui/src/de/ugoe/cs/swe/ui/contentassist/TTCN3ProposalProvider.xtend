/*
 * generated by Xtext
 */
package de.ugoe.cs.swe.ui.contentassist

import de.ugoe.cs.swe.ui.contentassist.AbstractTTCN3ProposalProvider

/**
 * see http://www.eclipse.org/Xtext/documentation.html#contentAssist on how to customize content assistant
 */
class TTCN3ProposalProvider extends AbstractTTCN3ProposalProvider {
}
