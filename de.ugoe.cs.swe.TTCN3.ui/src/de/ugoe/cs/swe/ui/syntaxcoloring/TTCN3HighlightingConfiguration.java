package de.ugoe.cs.swe.ui.syntaxcoloring;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration;
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfigurationAcceptor;
import org.eclipse.xtext.ui.editor.utils.TextStyle;

public class TTCN3HighlightingConfiguration extends DefaultHighlightingConfiguration {

	public static final String KEYWORD_LAYOUT = "keywords";
	private static final RGB   KEYWORD_COLOUR = new RGB(150, 100, 100);
	public static final String BEHAVIOUR_LAYOUT = "behaviour";
	private static final RGB   BEHAVIOUR_COLOUR = new RGB(150, 140, 120);
	public static final String CONFIGURATION_LAYOUT = "configuration";
    private static final RGB   CONFIGURATION_COLOUR = new RGB(50, 40, 120);
	public static final String DATA_LAYOUT = "data";
	private static final RGB   DATA_COLOUR = new RGB(110, 140, 150);
	public static final String DATATYPE_LAYOUT = "datatype";
    private static final RGB   DATATYPE_COLOUR = new RGB(150, 40, 150);

    @Override
    public void configure(IHighlightingConfigurationAcceptor acceptor) {
        super.configure(acceptor);
        acceptor.acceptDefaultHighlighting(KEYWORD_LAYOUT, "Keywords", keywordLayoutTextStyle());
        acceptor.acceptDefaultHighlighting(BEHAVIOUR_LAYOUT, "Behaviour", behaviourLayoutTextStyle());
        acceptor.acceptDefaultHighlighting(CONFIGURATION_LAYOUT, "Configuration", configurationLayoutTextStyle());
        acceptor.acceptDefaultHighlighting(DATA_LAYOUT, "Data", dataLayoutTextStyle());
        acceptor.acceptDefaultHighlighting(DATATYPE_LAYOUT, "Data Type", dataTypeLayoutTextStyle());
    }

    public TextStyle keywordLayoutTextStyle() {
        TextStyle textStyle = new TextStyle();
        textStyle.setStyle(SWT.BOLD);
        textStyle.setColor(KEYWORD_COLOUR);
        return textStyle;
    }
    public TextStyle behaviourLayoutTextStyle() {
        TextStyle textStyle = new TextStyle();
        textStyle.setStyle(SWT.BOLD);
        textStyle.setColor(BEHAVIOUR_COLOUR);
        return textStyle;
    }
    public TextStyle configurationLayoutTextStyle() {
        TextStyle textStyle = new TextStyle();
        textStyle.setStyle(SWT.BOLD);
        textStyle.setColor(CONFIGURATION_COLOUR);
        return textStyle;
    }
    public TextStyle dataLayoutTextStyle() {
        TextStyle textStyle = new TextStyle();
        textStyle.setStyle(SWT.ITALIC);
        textStyle.setColor(DATA_COLOUR);
        return textStyle;
    }
    public TextStyle dataTypeLayoutTextStyle() {
        TextStyle textStyle = new TextStyle();
        textStyle.setStyle(SWT.ITALIC | SWT.UNDERLINE_SINGLE);
        textStyle.setColor(DATATYPE_COLOUR);
        return textStyle;
    }

}