package de.ugoe.cs.swe.ui.syntaxcoloring;
import java.util.HashSet;

import org.eclipse.xtext.ide.editor.syntaxcoloring.DefaultAntlrTokenToAttributeIdMapper;


public class TTCN3TokenToAttributeIdMapper extends DefaultAntlrTokenToAttributeIdMapper {

    private HashSet<String> layoutKeywords = new HashSet<String>();
    private HashSet<String> configurationKeywords = new HashSet<String>();

    public TTCN3TokenToAttributeIdMapper() {
        layoutKeywords.add("'template'");
        layoutKeywords.add("'function'");
        layoutKeywords.add("'type'");
        layoutKeywords.add("'testcase'");
        layoutKeywords.add("'send'");
        layoutKeywords.add("'receive'");
//        layoutKeywords.add("'times'");
//        layoutKeywords.add("'out'");
//        layoutKeywords.add("'start'");
//        layoutKeywords.add("'stop'");
//        layoutKeywords.add("'waits'");
//        layoutKeywords.add("'quiet'");
//        layoutKeywords.add("'set verdict'");
//        layoutKeywords.add("'assert'");
//        layoutKeywords.add("'otherwise'");
//        layoutKeywords.add("'returned'");
//        layoutKeywords.add("'from'");
//        layoutKeywords.add("'assigned'");
//        configurationKeywords.add("'connect'");
//        configurationKeywords.add("'create'");
//        configurationKeywords.add("'uses'");
        
        //        for (Color color : Color.values()) {
        //            System.out.println(color.getLiteral() + "::" + color.getName());
        //            layoutKeywords.add("'" + color.getName().toLowerCase() + "'");
        //        }
    }

    @Override
    protected String calculateId(String tokenName, int tokenType) {
        if (layoutKeywords.contains(tokenName)) {
            return TTCN3HighlightingConfiguration.BEHAVIOUR_LAYOUT;
        }
        if (configurationKeywords.contains(tokenName)) {
            return TTCN3HighlightingConfiguration.CONFIGURATION_LAYOUT;
        }
        return super.calculateId(tokenName, tokenType);
    }

    public String getId(int tokenType) {
        return getMappedValue(tokenType);
    }

}
