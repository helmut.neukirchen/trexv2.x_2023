package de.ugoe.cs.swe.common;

import java.util.concurrent.Callable;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.EcoreUtil2;

import de.ugoe.cs.swe.common.CommonHelper;
import de.ugoe.cs.swe.scoping.ImportData;
import de.ugoe.cs.swe.scoping.TTCN3GlobalScopeProvider;
import de.ugoe.cs.swe.tTCN3.ImportDef;
import de.ugoe.cs.swe.tTCN3.TTCN3Module;

public class PreAnalyzer implements Callable<Boolean> {
	private final Resource resource;

	public PreAnalyzer(Resource resource) {
		this.resource = resource;
	}

	private boolean findImports() {
		TTCN3Module currentModule = CommonHelper.getModule(this.resource);

		for (ImportDef i : EcoreUtil2.getAllContentsOfType(currentModule, ImportDef.class)) {
			TTCN3Module importedModule = TTCN3GlobalScopeProvider.NAMED_MODULES.get(i.getName());

			if (importedModule != null) {

				synchronized (TTCN3GlobalScopeProvider.IMPORTS) {
					if (TTCN3GlobalScopeProvider.IMPORTS.containsKey(this.resource)) {
						TTCN3GlobalScopeProvider.IMPORTS.get(this.resource)
								.add(initializeImportedObjects(i, importedModule));
					} else {
						TTCN3GlobalScopeProvider.IMPORTS.put(this.resource,
								initializeImportedObjects(i, importedModule));
					}
				}

				synchronized (TTCN3GlobalScopeProvider.IMPORTED_RESOURCES) {
					if (TTCN3GlobalScopeProvider.IMPORTED_RESOURCES.containsKey(this.resource)) {
						TTCN3GlobalScopeProvider.IMPORTED_RESOURCES.get(this.resource).add(importedModule.eResource());
					} else {
						TTCN3GlobalScopeProvider.IMPORTED_RESOURCES.put(this.resource, importedModule.eResource());
					}
				}

				synchronized (TTCN3GlobalScopeProvider.IMPORTED_FROM) {
					if (TTCN3GlobalScopeProvider.IMPORTED_FROM.containsKey(importedModule)) {
						TTCN3GlobalScopeProvider.IMPORTED_FROM.get(importedModule).add(currentModule);
					} else {
						TTCN3GlobalScopeProvider.IMPORTED_FROM.put(importedModule, currentModule);
					}
				}
				
			} else {
				
				boolean isASN = false;
				if (i.getSpec() != null) {
					for (String s : i.getSpec().getTxt()) {
						if (s.contains("ASN")) {
							isASN = true;
							break;
						}
					}
				}
				
				if (!isASN) {
					synchronized (TTCN3GlobalScopeProvider.IMPORTS) {
						if (TTCN3GlobalScopeProvider.IMPORTS.containsKey(this.resource)) {
							TTCN3GlobalScopeProvider.IMPORTS.get(this.resource)
									.add(initializeImportedObjects(i, importedModule));
						} else {
							TTCN3GlobalScopeProvider.IMPORTS.put(this.resource,
									initializeImportedObjects(i, importedModule));
						}
					}
				}
				
			}
		}
 		return true;
	}
	
	private ImportData initializeImportedObjects(final ImportDef def, final TTCN3Module module) {
		ImportData data = new ImportData(def.getName(), def, module);
		
		if (module != null) {
			data.getExportedObjects().addAll(TTCN3GlobalScopeProvider.EXPORTED_OBJECTS.get(module.eResource()));
		}
		
		return data;
	}

	
	@Override
	public Boolean call() throws Exception {
		return findImports();
	}
}
