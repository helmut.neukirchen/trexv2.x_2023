package de.ugoe.cs.swe.scoping;

import java.util.List;

import org.eclipse.xtext.resource.IEObjectDescription;

import com.google.common.collect.Lists;

import de.ugoe.cs.swe.tTCN3.ImportDef;
import de.ugoe.cs.swe.tTCN3.TTCN3Module;

//TODO: consider restrictions of the imported scope
public class ImportData {
	private final String name;
	private final ImportDef importDef;
	private final TTCN3Module module;
	private List<IEObjectDescription> exportedObjects;
	
	public ImportData(String name, ImportDef importDef, TTCN3Module module) {
		super();
		this.name = name;
		this.importDef = importDef;
		this.module = module;
		exportedObjects = Lists.newArrayList();
	}

	public TTCN3Module getModule() {
		return module;
	}

	public String getName() {
		return name;
	}

	public ImportDef getImportDef() {
		return importDef;
	}

	public List<IEObjectDescription> getExportedObjects() {
		return exportedObjects;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImportData other = (ImportData) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
