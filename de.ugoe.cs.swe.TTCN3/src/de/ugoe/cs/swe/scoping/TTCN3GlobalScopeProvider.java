package de.ugoe.cs.swe.scoping;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.IResourceDescription;
import org.eclipse.xtext.resource.IResourceDescriptionsProvider;
import org.eclipse.xtext.resource.ISelectable;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.Scopes;
import org.eclipse.xtext.scoping.impl.AbstractGlobalScopeProvider;
import org.eclipse.xtext.scoping.impl.MultimapBasedScope;
import org.eclipse.xtext.scoping.impl.MultimapBasedSelectable;

import com.google.common.base.Predicate;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.SetMultimap;
import com.google.inject.Inject;

import de.ugoe.cs.swe.common.CommonHelper;
import de.ugoe.cs.swe.common.MiscTools;
import de.ugoe.cs.swe.common.PreAnalyzer;
import de.ugoe.cs.swe.common.logging.LoggingInterface;
import de.ugoe.cs.swe.common.logging.LoggingInterface.MessageClass;
import de.ugoe.cs.swe.tTCN3.ImportDef;
import de.ugoe.cs.swe.tTCN3.TTCN3Module;
import de.ugoe.cs.swe.tTCN3.TTCN3Package;
import de.ugoe.cs.swe.validation.TTCN3StatisticsProvider;

public class TTCN3GlobalScopeProvider extends AbstractGlobalScopeProvider {

	// TODO: do not use this map when using the xtext eclipse editor
	public static final Map<Resource, IScope> STATIC_SCOPE = Collections.synchronizedMap(Maps.<Resource, IScope> newHashMap());

	public static final SetMultimap<Resource, Resource> IMPORTED_RESOURCES = Multimaps
			.synchronizedSetMultimap(HashMultimap.<Resource, Resource> create());
	public static final SetMultimap<Resource, ImportData> IMPORTS = Multimaps
			.synchronizedSetMultimap(HashMultimap.<Resource, ImportData> create());
	public static final Multimap<TTCN3Module, TTCN3Module> IMPORTED_FROM = Multimaps
			.synchronizedSetMultimap(HashMultimap.<TTCN3Module, TTCN3Module> create());
	public static final SetMultimap<Resource, IEObjectDescription> EXPORTED_OBJECTS = Multimaps
			.synchronizedSetMultimap(HashMultimap.<Resource, IEObjectDescription> create());
	
	public static final List<Resource> RESOURCES = Collections.synchronizedList(Lists.<Resource> newArrayList());
	public static final Map<String, TTCN3Module> NAMED_MODULES = Collections.synchronizedMap(Maps.<String, TTCN3Module> newHashMap());
	
	// array[0] = line, array[1] = column
	public static final Map<String, List<String[]>> FOUND_TABS = Collections.synchronizedMap(Maps.<String, List<String[]>> newHashMap());

	public static boolean LIVE = true;		

	@Inject
	IResourceDescriptionsProvider rdp;

	@Inject
    XtextResourceSet resourceSet;
	
	@Override
	public IScope getScope(Resource resource, final EReference reference, Predicate<IEObjectDescription> filter) {
		return getScope(resource, reference);
	}

	@Override
	public IScope getScope(Resource resource, final EReference reference) {
		return getImportedScope(resource, reference);
	}

	protected IScope getImportedScope(final Resource resource) {
		return getImportedScope(resource, null);
	}

	protected IScope getImportedScope(final Resource resource, final EReference reference) {

		if (TTCN3StatisticsProvider.getInstance().isPreAnalyzingCompleted() && STATIC_SCOPE.containsKey(resource)) {
			return STATIC_SCOPE.get(resource);
		} else if (TTCN3StatisticsProvider.getInstance().isPreAnalyzingCompleted()) {
			IScope scope = IScope.NULLSCOPE;
			List<IEObjectDescription> objects = Lists.newArrayList();
			
			for (ImportData importData : IMPORTS.get(resource)) {
				objects.addAll(importData.getExportedObjects());
			}
			
			if (reference == null)
				return scope;

			synchronized (STATIC_SCOPE) {
				scope = MultimapBasedScope.createScope(IScope.NULLSCOPE, objects, isIgnoreCase(reference));
				if (TTCN3StatisticsProvider.getInstance().isParsingCompleted()) {
					STATIC_SCOPE.put(resource, scope);
				}
			}
			if (LIVE) {
				TTCN3StatisticsProvider.getInstance().setPreAnalyzingCompleted(false);
			}

			return scope;
		} else {
			if (LIVE) {
				reset(resource);
				return getImportedScope(resource, reference);
			} else {
				return IScope.NULLSCOPE;
			}
			
		}
	}

	public static void printImportedModuleNames(boolean fileNames, final LoggingInterface logger) {
		for (String moduleName : NAMED_MODULES.keySet()) {
			TTCN3Module module = NAMED_MODULES.get(moduleName);
			INode node = NodeModelUtils.getNode(module);
			String importList = "";
			int index = 0;
			List<ImportDef> imports = EcoreUtil2.getAllContentsOfType(module, ImportDef.class);
			int size = imports.size();

			for (ImportDef importDef : imports) {
				index++;
				TTCN3Module importModule = NAMED_MODULES.get(importDef.getName());
				if (importModule != null) {
					if (fileNames) {
						importList += importDef.getName() + " located in " + importModule.eResource().getURI().devicePath()
								+ (index < size ? ", " : "");
					} else {
						importList += importDef.getName() + (index < size ? ", " : "");
					}
				} else {
					importList += "(<" + importDef.getName() + "> is unresolved!)" + (index < size ? ", " : "");
					printModuleNotResolvedMessage(module.eResource().getURI(), node, importDef.getName(), logger);
				}
			}

			String message = "Importing from module(s) \"" + importList + "\"";

			logger.logInformation(module.eResource().getURI(), node.getStartLine(), node.getEndLine(), MessageClass.GENERAL, message,
					"1.18, " + MiscTools.getMethodName());

		}
	}

	public static void printImportingModuleNames(boolean fileNames, final LoggingInterface logger) {
		for (TTCN3Module importedFrom : IMPORTED_FROM.keySet()) {
			INode node = NodeModelUtils.getNode(importedFrom);
			String importList = "";
			int index = 0;
			Collection<TTCN3Module> modules = IMPORTED_FROM.get(importedFrom);
			int size = modules.size();
			
			for (TTCN3Module module : modules) {
				index++;
				if (fileNames) {
					importList += module.getName() + " located in " + module.eResource().getURI().devicePath()
							+ (index < size ? ", " : "");
				} else {
					importList += module.getName() + (index < size ? ", " : "");
				}
				
				String message = "Module \"" + module.getName() + "\" is imported in module(s) \"" + importList + "\"";
				
				logger.logInformation(module.eResource().getURI(), node.getStartLine(), node.getEndLine(), MessageClass.GENERAL, message,
						"1.18, " + MiscTools.getMethodName());				
			}
		}
	}

	private static void printModuleNotResolvedMessage(URI uri, INode node, String name, LoggingInterface logger) {
		logger.logInformation(uri, node.getStartLine(), node.getEndLine(), MessageClass.GENERAL,
				"Imported module \"" + name + "\" cannot be resolved!", "1.18, " + MiscTools.getMethodName());
	}
	//Adapted from TTCN3ResourceProvider
	public void reset(final Resource resource) {
		TTCN3GlobalScopeProvider.RESOURCES.clear();
		TTCN3GlobalScopeProvider.NAMED_MODULES.clear();
		TTCN3GlobalScopeProvider.EXPORTED_OBJECTS.clear();
		TTCN3GlobalScopeProvider.IMPORTS.clear();
		TTCN3GlobalScopeProvider.IMPORTED_FROM.clear();
		TTCN3GlobalScopeProvider.IMPORTED_RESOURCES.clear();
		TTCN3GlobalScopeProvider.STATIC_SCOPE.clear();

		for (IResourceDescription d : rdp.getResourceDescriptions(resourceSet).getAllResourceDescriptions()) {
			if (d.getURI().lastSegment().endsWith("ttcn3") || d.getURI().lastSegment().endsWith("ttcn")) {
				Resource r = resource.getResourceSet().getResource(d.getURI(), true);
				TTCN3Module module = CommonHelper.getModule(r);
				if (module !=null && !TTCN3GlobalScopeProvider.NAMED_MODULES.containsKey(module.getName())) {
//					System.out.println(r.getURI());
					TTCN3GlobalScopeProvider.RESOURCES.add(r);
					TTCN3GlobalScopeProvider.NAMED_MODULES.put(module.getName(), module);
				}
			}
		}

		for (IResourceDescription d : rdp.getResourceDescriptions(resourceSet).getAllResourceDescriptions()) {
			if (d.getURI().lastSegment().endsWith("ttcn3") || d.getURI().lastSegment().endsWith("ttcn")) {
				Resource r = resource.getResourceSet().getResource(d.getURI(), true);
				TTCN3GlobalScopeProvider.initExportedObjects(r);
			}
		}
		
		for (int i = 0; i < TTCN3GlobalScopeProvider.RESOURCES.size(); i++) {
			PreAnalyzer p = new PreAnalyzer(TTCN3GlobalScopeProvider.RESOURCES.get(i));
			try {
				p.call();
			} catch (Exception e) { 
				e.printStackTrace();
			}
		} 
			
		TTCN3StatisticsProvider.getInstance().setPreAnalyzingCompleted(true);
	}

	//Adapted from FileParser 
	public static void initExportedObjects(Resource resource) {
		List<IEObjectDescription> objects = Lists.newArrayList();
		
		List<IEObjectDescription> exports = Lists.newArrayList(internalGetAllDescriptions(resource).getExportedObjects());
		int size = exports.size();
		
		for (int i = 0; i < size; i++) {
			IEObjectDescription e = exports.get(i);
			IEObjectDescription d = null;
			
			if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getTTCN3Module(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getGroupDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getEnumDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getEnumeration(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getComponentDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getPortDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getRecordDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getSetDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getSubTypeDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getUnionDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getRecordOfDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getSetOfDefNamed(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getSignatureDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getBaseTemplate(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getFunctionRef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getAltstepDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getTestcaseDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getSingleConstDef(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			} else if (EcoreUtil2.isAssignableFrom(TTCN3Package.eINSTANCE.getModuleParameter(), e.getEClass())) {
				d = EObjectDescription.create(e.getQualifiedName().getLastSegment(), e.getEObjectOrProxy());
				objects.add(d);
			}
		}	
		
		synchronized (TTCN3GlobalScopeProvider.EXPORTED_OBJECTS) {
			TTCN3GlobalScopeProvider.EXPORTED_OBJECTS.putAll(resource, objects);
		}
		
	}
	
	//Adapted from FileParser
	private static ISelectable internalGetAllDescriptions(final Resource resource) {
		Iterable<EObject> allContents = new Iterable<EObject>() {
			public Iterator<EObject> iterator() {
				return EcoreUtil.getAllContents(resource, false);
			}
		};
		Iterable<IEObjectDescription> allDescriptions = Scopes.scopedElementsFor(allContents);
		return new MultimapBasedSelectable(allDescriptions);
	}		
	
}
