package de.ugoe.cs.swe.scoping

import de.ugoe.cs.swe.tTCN3.AltstepDef
import de.ugoe.cs.swe.tTCN3.AltstepLocalDef
import de.ugoe.cs.swe.tTCN3.ArrayExpression
import de.ugoe.cs.swe.tTCN3.BaseTemplate
import de.ugoe.cs.swe.tTCN3.CallStatement
import de.ugoe.cs.swe.tTCN3.ComponentDef
import de.ugoe.cs.swe.tTCN3.ComponentDefList
import de.ugoe.cs.swe.tTCN3.ConstDef
import de.ugoe.cs.swe.tTCN3.FieldConstExpressionSpec
import de.ugoe.cs.swe.tTCN3.FieldExpressionSpec
import de.ugoe.cs.swe.tTCN3.FieldReference
import de.ugoe.cs.swe.tTCN3.FieldSpec
import de.ugoe.cs.swe.tTCN3.ForStatement
import de.ugoe.cs.swe.tTCN3.FormalTemplatePar
import de.ugoe.cs.swe.tTCN3.FormalTimerPar
import de.ugoe.cs.swe.tTCN3.FormalValuePar
import de.ugoe.cs.swe.tTCN3.FunctionDef
import de.ugoe.cs.swe.tTCN3.FunctionFormalParList
import de.ugoe.cs.swe.tTCN3.FunctionInstance
import de.ugoe.cs.swe.tTCN3.GroupDef
import de.ugoe.cs.swe.tTCN3.Head
import de.ugoe.cs.swe.tTCN3.InLineTemplate
import de.ugoe.cs.swe.tTCN3.Initial
import de.ugoe.cs.swe.tTCN3.ModuleControlBody
import de.ugoe.cs.swe.tTCN3.ModuleControlPart
import de.ugoe.cs.swe.tTCN3.ModuleDefinition
import de.ugoe.cs.swe.tTCN3.ModulePar
import de.ugoe.cs.swe.tTCN3.ModuleParDef
import de.ugoe.cs.swe.tTCN3.ModuleParameter
import de.ugoe.cs.swe.tTCN3.PortDef
import de.ugoe.cs.swe.tTCN3.PortOrAny
import de.ugoe.cs.swe.tTCN3.PortRef
import de.ugoe.cs.swe.tTCN3.RaiseStatement
import de.ugoe.cs.swe.tTCN3.RefValue
import de.ugoe.cs.swe.tTCN3.RefValueElement
import de.ugoe.cs.swe.tTCN3.RefValueHead
import de.ugoe.cs.swe.tTCN3.RefValueTail
import de.ugoe.cs.swe.tTCN3.ReferencedType
import de.ugoe.cs.swe.tTCN3.ReplyStatement
import de.ugoe.cs.swe.tTCN3.SendStatement
import de.ugoe.cs.swe.tTCN3.SingleConstDef
import de.ugoe.cs.swe.tTCN3.SingleTempVarInstance
import de.ugoe.cs.swe.tTCN3.SingleVarInstance
import de.ugoe.cs.swe.tTCN3.SpecElement
import de.ugoe.cs.swe.tTCN3.SpecTypeElement
import de.ugoe.cs.swe.tTCN3.StartTimerStatement
import de.ugoe.cs.swe.tTCN3.StatementBlock
import de.ugoe.cs.swe.tTCN3.StructFieldDef
import de.ugoe.cs.swe.tTCN3.TTCN3Module
import de.ugoe.cs.swe.tTCN3.TTCN3Package
import de.ugoe.cs.swe.tTCN3.TemplateDef
import de.ugoe.cs.swe.tTCN3.TestcaseDef
import de.ugoe.cs.swe.tTCN3.TimerRefOrAny
import de.ugoe.cs.swe.tTCN3.TypeReference
import de.ugoe.cs.swe.tTCN3.TypeReferenceTail
import de.ugoe.cs.swe.tTCN3.TypeReferenceTailType
import de.ugoe.cs.swe.tTCN3.UnionFieldDef
import de.ugoe.cs.swe.tTCN3.VarInstance
import java.util.ArrayList
import java.util.List
import org.apache.log4j.Logger
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider

import static de.ugoe.cs.swe.scoping.TTCN3GlobalScopeProvider.*
import static org.eclipse.xtext.scoping.Scopes.*

import static extension de.ugoe.cs.swe.common.TTCN3ScopeHelper.*
import static extension org.eclipse.xtext.EcoreUtil2.*
import de.ugoe.cs.swe.tTCN3.ExtConstDef
import de.ugoe.cs.swe.tTCN3.NamedObject

class TTCN3ScopeProvider extends AbstractDeclarativeScopeProvider {
	final static Logger LOG = Logger.getLogger(TTCN3LocalScopeProvider);

	def IScope scope_FieldSpec_ref(FieldSpec spec, EReference ref) {
		spec.scopeFieldExpressionList
	}

	private def Iterable<EObject> scopeFieldReferences(FieldReference fieldRef) {
		if (fieldRef instanceof StructFieldDef) {
			if (fieldRef.type !== null && fieldRef.type.ref !== null) {
				fieldRef.type.ref.scopeReferencedFields
			} else if (fieldRef.nestedType !== null) {
				if (fieldRef.nestedType !== null) {
					val nType = fieldRef.nestedType.findUsedType
					if (nType !== null) {
						nType.scopeReferencedFields
					}
				}
			}
		} else if (fieldRef instanceof FormalValuePar) {
			if (fieldRef.type !== null && fieldRef.type.ref !== null) {
				fieldRef.type.ref.scopeReferencedFields
			}
		} else if (fieldRef instanceof UnionFieldDef) {
			if (fieldRef.type !== null && fieldRef.type.ref !== null) {
				fieldRef.type.ref.scopeReferencedFields
			} else if (fieldRef.nestedType !== null) {
				if (fieldRef.nestedType !== null) {
					val nType = fieldRef.nestedType.findUsedType
					if (nType !== null) {
						nType.scopeReferencedFields
					}
				}
			}
		}
	}

	private def void scopeFieldReferences(FieldReference fieldRef, ArrayList<EObject> list) {
		if (fieldRef instanceof StructFieldDef) {
			if (fieldRef.type !== null && fieldRef.type.ref !== null) {
				list.addAll(fieldRef.type.ref.scopeReferencedFields)
			} else if (fieldRef.nestedType !== null) {
				if (fieldRef.nestedType !== null) {
					val nType = fieldRef.nestedType.findUsedType
					if (nType !== null) {
						list.addAll(nType.scopeReferencedFields)
					}
				}
			}
		} else if (fieldRef instanceof FormalValuePar) {
			if (fieldRef.type !== null && fieldRef.type.ref !== null) {
				list.addAll(fieldRef.type.ref.scopeReferencedFields)
			}
		} else if (fieldRef instanceof UnionFieldDef) {
			if (fieldRef.type !== null && fieldRef.type.ref !== null) {
				list.addAll(fieldRef.type.ref.scopeReferencedFields)
			} else if (fieldRef.nestedType !== null) {
				if (fieldRef.nestedType !== null) {
					val nType = fieldRef.nestedType.findUsedType
					if (nType !== null) {
						list.addAll(nType.scopeReferencedFields)
					}
				}
			}
		}
	}

	private def IScope scopeFieldExpressionList(EObject field) {
		val ArrayList<EObject> list = newArrayList

		list.addAll(field.scopeListFieldExpressionList)

		val instance = field.findDesiredParent(FunctionInstance)
		if (instance !== null) {
			val reference = instance.ref
			if (reference instanceof BaseTemplate) {
				for (p : reference.parList.params) {
					var TypeReference type = null
					if (p.template !== null) {
						type = p.template.findVariableType
					} else if (p.value !== null) {
						type = p.value.findVariableType
					}
					if (type !== null) {
						list.addAll(type.scopeReferencedFields)
					}
				}
			} else if (reference instanceof FunctionDef) {
				if (reference.parameterList !== null) {
					for (p : reference.parameterList.params) {
						var TypeReference type = null
						if (p.template !== null) {
							type = p.template.findVariableType
						} else if (p.value !== null) {
							type = p.value.findVariableType
						}
						if (type !== null) {
							list.addAll(type.scopeReferencedFields)
						}
					}
				}
			} else if (reference instanceof AltstepDef) {

				// TODO: implement if really needed
				LOG.info("Method is not implemented for Type AltstepDef!")
			}
		}

		scopeFor(list)
	}

	private def Iterable<EObject> scopeListFieldExpressionList(EObject field) {
		val module = field.findDesiredParent(TTCN3Module)
		val inlineTemplate = field.findDesiredParent(InLineTemplate)
		val template = field.findDesiredParent(TemplateDef)
		val variable = field.findDesiredParent(VarInstance)
		val eField = field.findDesiredParent(FieldExpressionSpec)
		val cField = field.findDesiredParent(FieldConstExpressionSpec)
		val sField = field.findDesiredParent(FieldSpec)
		val constDef = field.findDesiredParent(ConstDef)
		val hasArrayAsParent = field.findDesiredParent(ArrayExpression) !== null
		var ArrayList<EObject> list = newArrayList
		var nestedFields = newArrayList

		if (eField !== null) {
			list.addAll(eField.fieldRef.scopeFieldReferences)
		} else if (cField !== null) {
			list.addAll(cField.fieldRef.scopeFieldReferences)
		} else if (sField !== null) {
			if (sField.ref !== null) {
				list.addAll(sField.ref.scopeFieldReferences)
			}
		} else if (constDef !== null) {
			if (constDef.type.ref !== null) {
				list.addAll(constDef.type.ref.scopeReferencedFields)
			}
		} else if (inlineTemplate !== null) {
			if (inlineTemplate.type !== null && inlineTemplate.type.ref !== null) {
				list.addAll(inlineTemplate.type.ref.templateFieldScope)
			}
		} else if (template !== null) {
			if (template.base.type !== null && template.base.type.ref !== null) {
				list.addAll(template.base.type.ref.templateFieldScope)
			}
		} else if (variable !== null) {
			if (variable.type !== null && variable.type.ref !== null) {
				list.addAll(variable.type.ref.scopeReferencedFields)
			} else if (variable.listType !== null && variable.listType.ref !== null) {
				list.addAll(variable.listType.ref.scopeReferencedFields)
			}
		}

		// TODO: search for a more sophisticated condition	
		if (hasArrayAsParent) {
			nestedFields.addAll(list.findNestedFields)
		}
		list.addAll(nestedFields.map[it as RefValueElement])

		//TODO:why is this here and is it ever filtered afterwards? 
		//list.addAll(module.eAllOfType(FieldReference))

		synchronized (IMPORTS) {
			if (IMPORTS.containsKey(module.eResource)) {
				val imported = IMPORTS.get(module.eResource)
				for (i : 0 .. imported.size - 1) {
					val d = imported.get(i)
					for (o : d.exportedObjects) {
						if (TTCN3Package.eINSTANCE.fieldReference.isAssignableFrom(o.EClass)) {
							list.add(o.EObjectOrProxy)
						}
					}
				}
			}
		}
		return list
	}

	private def Iterable<EObject> findNestedFields(Iterable<EObject> scope) {
		val list = newArrayList

		for (d : scope) {
			if (d instanceof FieldReference) {
				d.scopeFieldReferences(list)
			}
		}

		return list
	}

	def IScope scope_FieldExpressionSpec_fieldRef(FieldExpressionSpec field, EReference ref) {
		field.scopeFieldExpressionList
	}

	def IScope scope_StartTimerStatement_ref(StartTimerStatement stm, EReference ref) {
		stm.scopeOfTimer
	}

	// TODO: this method is never called caused by a grammar issue. 
	// The ConstantExpression branches to a SingleExpression or a CompoundConstExpression. The SingleExpression
	// could branch to CompoundExpression. Therefore, FieldExpressionList is always parsed instead of
	// FieldConstExpressionList. 
	def IScope scope_FieldConstExpressionSpec_fieldRef(FieldConstExpressionSpec field, EReference ref) {
		field.scopeFieldExpressionList
	}

	private def IScope scopeOfTimer(EObject variable) {
		val ArrayList<EObject> list = newArrayList
		var parent = variable.eContainer

		while (parent !== null) {
			if (parent instanceof AltstepDef) {
				if (parent.spec !== null) {
					componentScopeValueRefs(parent.spec.component, list, true, false, false)
				}
				if (parent.params !== null) {
					list.addAll(parent.params.eAllOfType(FormalTimerPar))
				}
				for (AltstepLocalDef d : parent.local.defs) {
					if (d.timer !== null) {
						for (SingleVarInstance v : d.timer.list.variables) {
							list.add(v)
						}
					}
				}
			} else if (parent instanceof FunctionDef) {
				if (parent.runsOn !== null) {
					componentScopeValueRefs(parent.runsOn.component, list, true, false, false)
				}
				if (parent.mtc !== null) {
					componentScopeValueRefs(parent.mtc.component, list, true, false, false)
				}
				if (parent.system !== null) {
					componentScopeValueRefs(parent.system.component, list, true, false, false)
				}
				list.addAll(parent.eAllOfType(SingleVarInstance))
				list.addAll(parent.functionLocalParameter)
            } else if (parent instanceof TestcaseDef) {
                if (parent.spec.runsOn !== null) {
                    componentScopeValueRefs(parent.spec.runsOn.component, list, true, false, false)
                }
                if (parent.spec.systemSpec !== null) {
                    componentScopeValueRefs(parent.spec.systemSpec.component, list, true, false, false)
                }
                list.addAll(parent.eAllOfType(SingleVarInstance))
                list.addAll(parent.testcaseLocalParameter)
			} else if (parent instanceof ComponentDef) {
				for (ComponentDefList l : parent.defs) {
					if (l.element.timer !== null) {
						for (SingleVarInstance v : l.element.timer.list.variables) {
							list.add(v)
						}
					}
				}
			} else if (parent instanceof ModuleControlBody) {
				for (i : parent.list.localInst) {
					if (i.timer !== null) {
						for (SingleVarInstance v : i.timer.list.variables) {
							list.add(v)
						}
					}
				}
			}
			parent = parent.eContainer
		}
		//TODO: this is nonsense! parent is already null according to the loop above
		//but why is it even necessary?
		//val function = parent.findDesiredParent(FunctionDef) // - wrong parent is already null
		//val function = variable.findDesiredParent(FunctionDef)
		//if (function !== null) {
			//list.addAll(function.functionLocalParameter)
		//}
		scopeFor(list)
	}

	def IScope scope_TimerRefOrAny_ref(TimerRefOrAny variable, EReference ref) {
		variable.scopeOfTimer
	}

	def IScope scope_TimerRefOrAll_ref(FunctionDef function, EReference ref) {
		val ArrayList<EObject> list = newArrayList
		list.addAll(function.functionLocalParameter)
		list.addAll(function.eAllOfType(SingleVarInstance))
		scopeFor(list, function.scopeTimerSpec)
	}

	def scope_StartTimerStatement_ref(FunctionDef function, EReference ref) {
		//TODO: does this ever hit?
		scopeFor(function.eAllOfType(SingleVarInstance), function.scopeTimerSpec)
	}

	def IScope scope_TimerRefOrAll_ref(AltstepDef altstep, EReference ref) {
		val ArrayList<EObject> list = newArrayList
		//TODO: does this ever hit?
		val function = altstep.findDesiredParent(FunctionDef)
		if (function !== null) {
			list.addAll(function.functionLocalParameter)
		}
		list.addAll(altstep.altstepLocalParameter)
		list.addAll(altstep.eAllOfType(SingleVarInstance))
		scopeFor(list, altstep.scopeTimerSpec)
	}

	def scope_StartTimerStatement_ref(AltstepDef altstep, EReference ref) {
		val ArrayList<EObject> list = newArrayList
		val function = altstep.findDesiredParent(FunctionDef)
		if (function !== null) {
			list.addAll(function.functionLocalParameter)
		}
		list.addAll(altstep.eAllOfType(SingleVarInstance))
		scopeFor(list, altstep.scopeTimerSpec)
	}

	def IScope scope_TimerRefOrAll_ref(TestcaseDef testcase, EReference ref) {
		val ArrayList<EObject> list = newArrayList
		val function = testcase.findDesiredParent(FunctionDef)
		if (function !== null) {
			list.addAll(function.functionLocalParameter)
		}
		list.addAll(testcase.eAllOfType(SingleVarInstance))
		scopeFor(list, testcase.scopeTimerSpec)
	}

	def scope_StartTimerStatement_ref(TestcaseDef testcase, EReference ref) {
		val ArrayList<EObject> list = newArrayList
		val function = testcase.findDesiredParent(FunctionDef)
		if (function !== null) {
			list.addAll(function.functionLocalParameter)
		}
		list.addAll(testcase.eAllOfType(SingleVarInstance))
		scopeFor(list, testcase.scopeTimerSpec)
	}

	def IScope scope_PortOrAny_ref(PortOrAny spec, EReference ref) {
		spec.scope_communicationPort
	}

	def IScope scope_SendStatement_port(SendStatement stm, EReference ref) {
		stm.scope_communicationPort
	}

	def IScope scope_CallStatement_port(CallStatement stm, EReference ref) {
		stm.scope_communicationPort
	}

	def IScope scope_ReplyStatement_port(ReplyStatement stm, EReference ref) {
		stm.scope_communicationPort
	}

	def IScope scope_RaiseStatement_port(RaiseStatement stm, EReference ref) {
		stm.scope_communicationPort
	}

	// TODO: check nested port statements
	private def IScope scope_communicationPort(EObject statement) {
		val list = newArrayList

		// ports from parent altsteps
		val altstep = statement.findDesiredParent(AltstepDef)
		if (altstep !== null) {
			list.addAll(altstep.params.scopeParameterList)
			if (altstep.spec !== null) {
				componentScopePorts(altstep.spec.component, list)
			}
		}

		// ports from parent function
		val function = statement.findDesiredParent(FunctionDef)
		if (function !== null) {
			list.addAll(function.parameterList.scopeParameterList)
			list.addAll(function.functionLocalParameter)
			if (function.runsOn !== null) {
				function.runsOn.component.componentScopePorts(list)
			}
			if (function.mtc !== null) {
				function.mtc.component.componentScopePorts(list)
			}
		}

		// ports from parent testcase
		val testcase = statement.findDesiredParent(TestcaseDef)
		if (testcase !== null) {
			testcase.spec.runsOn.component.componentScopePorts(list)
			if (testcase.spec.systemSpec !== null) {
				testcase.spec.systemSpec.component.componentScopePorts(list)
			}
		}
		scopeFor(list)
	}

	private def Iterable<EObject> scopeParameterList(FunctionFormalParList parList) {
		val List<EObject> res = newArrayList

		if (parList === null)
			return res;

		for (parameter : parList.params) {
			if (parameter.port !== null) {
				res.add(parameter.port)
			} else if (parameter.value !== null) { // TODO: FormatlValuePar and FormalPortPar are ambiguous, this hack resolves FormalValuePars as Ports too
				if (getReferencedType(parameter.value.type.ref) !== null &&
					getReferencedType(parameter.value.type.ref) instanceof PortDef) {
					res.add(parameter.value)
				}
			}
		}
		return res;
	}

	def IScope scope_PortRef_port(PortRef pRef, EReference ref) {

		// TODO: find type of ComponentRef
		val list = newArrayList
		if (pRef.component.ref !== null) {
			val variable = pRef.component.ref.variable.ref.referencedValue
			val reference = variable.findVariableType

			if (reference !== null) {
				list.addAll(reference.scopeReferencedFields)
			}
		} else if (pRef.component.system !== null ||
		    pRef.component.mtc !== null) {
		    //extension for references to system ports
		    //TODO: filter for types actually used as system? 
		    val module = pRef.findDesiredParent(TTCN3Module)

            for (ComponentDef ct : module.eAllOfType(ComponentDef)) {
                list.addAll(ct.scopeReferencedFields)
            }
		    
    	    synchronized (IMPORTS) {
                if (IMPORTS.containsKey(module.eResource)) {
                    val imported = IMPORTS.get(module.eResource)
                    for (i : 0 .. imported.size - 1) {
                        val d = imported.get(i)
                        for (o : d.exportedObjects) {
                            if (TTCN3Package.eINSTANCE.fieldReference.isAssignableFrom(ComponentDef.newInstance.eClass)) {
                                list.add(o.EObjectOrProxy)
                            }
                        }
                    }
                }
            }
		}
		scopeFor(list, pRef.scope_communicationPort)
	}

	def IScope scope_TypeReferenceTail_type(
		TypeReferenceTail it,
		EReference ref
	) {
		scopeFor((eContainer as SpecTypeElement).directTypeElements)
	}

	// TODO: consider all possible cases! Types, Modules(?), Fields, etc.
	// TODO: consider special treatment for SubTypeDefNamed
	private def dispatch Iterable<EObject> directTypeElements(ReferencedType it) {
		switch it {
			TTCN3Module: directModuleTypes
			GroupDef: directGroupTypes
			ReferencedType: scopeReferencedFields
		}
	}

	// TODO: consider special treatment for SubTypeDefNamed
	private def dispatch Iterable<EObject> directTypeElements(TypeReferenceTailType it) {
		switch it {
			GroupDef: directGroupTypes
			FieldReference: directFieldTypeElements
		}
	}

	private def dispatch Iterable<EObject> directTypeElements(SpecTypeElement it) {
		switch it {
			TypeReference: head.directTypeElements
			TypeReferenceTail: type.directTypeElements
		}
	}

	private def Iterable<EObject> directModuleTypes(TTCN3Module it) {
		val List<EObject> list = newArrayList
		list.addAll(eAllOfType(ReferencedType))
		list.addAll(eAllOfType(GroupDef))
		list
	}

	private def Iterable<EObject> directGroupTypes(GroupDef it) {
		val List<EObject> res = newArrayList
		if (list === null)
			return res

		for (ModuleDefinition g : list.definitions) {
			val elt = g.def
			if (elt instanceof GroupDef) {
				res.add(elt)
			} else if (elt instanceof ReferencedType) {
				res.add(elt)
			}
		}
		return res
	}

	private def Iterable<EObject> directFieldTypeElements(FieldReference field) {
		if (field instanceof StructFieldDef) {
			if (field.type !== null) {
				field.type.ref.scopeReferencedFields
			}
		} else if (field instanceof FormalValuePar) {
			if (field.type !== null) {
				field.type.ref.scopeReferencedFields
			}
		} else if (field instanceof FormalTemplatePar) {
			if (field.type !== null) {
				field.type.ref.scopeReferencedFields
			}
		} else if (field instanceof UnionFieldDef) {
			if (field.type !== null) {
				field.type.ref.scopeReferencedFields
			}
		}
	}

//	def IScope scope_TypeReference_field(TypeReference tr, EReference ref) {
//		//TODO: test other nested possibilities
//		if (!(tr.head instanceof TTCN3Module && tr.tail == null))
//			scopeFor(tr.head.scopeReferencedFields)
//	}
//
//	def IScope scope_TypeReference_head(TypeReference tr, EReference ref) {
//		// TODO: fix me!
//	}
	def IScope scope_Head_target(Head value, EReference ref) {
		return value.scope_ReferencedValueHead(ref)
	}

	private def IScope scope_ReferencedValueHead(EObject value, EReference ref) {
		val list = newArrayList();

		if (value === null)
			return IScope.NULLSCOPE
		
		if (value instanceof TTCN3Module) {
			value.defs.scopeModuleVariable(list)
			for (ModuleDefinition g : value.defs.definitions.filter[it.def instanceof GroupDef]) {
				val GroupDef group = g.def as GroupDef
				if (group.list !== null) {
					group.list.scopeModuleVariable(list)
				}
			}
			scopeFor(list, delegate.getScope(value, ref))
		} else if (value instanceof StatementBlock) {
			value.scopeStatementBlock(list)
			scopeFor(list, value.eContainer.scope_ReferencedValueHead(ref))
		} else if (value instanceof Initial) {
			value.scopeInitial(list)
			scopeFor(list, value.eContainer.scope_ReferencedValueHead(ref))
		} else if (value instanceof ForStatement) {
			value.init.scopeInitial(list)
			scopeFor(list, value.eContainer.scope_ReferencedValueHead(ref))
		} else if (value instanceof FunctionDef) {
			if(value.parameterList !== null) value.parameterList.scopeFunctionParameterValue(list)
			if (value.runsOn !== null) {
				value.runsOn.component.componentScopeValueRefs(list, false, true, true)
			}
			scopeFor(list, value.eContainer.scope_ReferencedValueHead(ref))
		} else if (value instanceof ModuleControlPart) {
			value.scopeModuleControlPart(list)
			scopeFor(list, value.eContainer.scope_ReferencedValueHead(ref))
		} else if (value instanceof ComponentDef) {
			value.componentScopeValueRefs(list, false, true, true)
			scopeFor(list, value.eContainer.scope_ReferencedValueHead(ref))
		} else if (value instanceof TestcaseDef) {
			val ComponentDef comp = value.spec.runsOn.component
			if (value.parList !== null) {
				value.parList.scopeTemplateOrValueFormalParList(list)
			}
			comp.componentScopeValueRefs(list, false, true, true)
			scopeFor(list, value.eContainer.scope_ReferencedValueHead(ref))
		} else if (value instanceof AltstepDef) {
			if (value.params !== null) {
				value.params.scopeFunctionParameterValue(list)
			}
			value.scopeAltstepExtendedDefList(list)
			value.local.scopeAltstepLocalDefList(list)
			scopeFor(list, value.eContainer.scope_ReferencedValueHead(ref))
		} else if (value instanceof GroupDef) {
			value.list.scopeModuleVariable(list)
			scopeFor(list, value.eContainer.scope_ReferencedValueHead(ref))
		} else if (value instanceof ModuleParDef) {
			value.scopeModuleParDef(list)
			scopeFor(list, value.eContainer.scope_ReferencedValueHead(ref))
		} else if (value instanceof TemplateDef) {
			value.scopeTemplateParameter(list)
			scopeFor(list, value.eContainer.scope_ReferencedValueHead(ref))
		} else if (value instanceof FunctionInstance) {
			// TODO: implement this
			scopeFor(list, value.eContainer.scope_ReferencedValueHead(ref))
		} else {
			scopeFor(list, value.eContainer.scope_ReferencedValueHead(ref))
		}
	}

	def IScope scope_RefValueTail_value(
		RefValueTail it,
		EReference ref
	) {
		scopeFor((eContainer as SpecElement).directElements)
	}

	private def dispatch Iterable<EObject> directElements(RefValueElement it) {
		switch it {
			FieldReference: directFieldElements
			RefValue: directValueElements
			GroupDef: directGroupElements
		}
	}

	private def dispatch Iterable<EObject> directElements(RefValueHead it) {
		switch it {
			GroupDef: directGroupElements
			RefValue: directValueElements
			TTCN3Module: directModuleElements
		}
	}

	private def dispatch Iterable<EObject> directElements(SpecElement it) {
		switch it {
			Head: target.directElements
			RefValueTail: value.directElements
		}
	}

	private def Iterable<EObject> directValueElements(RefValue value) {
		if (value instanceof SingleTempVarInstance) {
			val variable = value.findDesiredParent(VarInstance)
			if (variable !== null && variable.type.ref !== null) {
				variable.type.ref.scopeReferencedFields
			}
		} else if (value instanceof BaseTemplate) {
			value.type.ref.scopeReferencedFields
		} else if (value instanceof SingleVarInstance) {
			val variable = value.findDesiredParent(VarInstance)
			if (variable.listType !== null && variable.listType.ref !== null) {
				variable.listType.ref.scopeReferencedFields
			}
		} else if (value instanceof StructFieldDef) {
			if (value.type !== null) {
				value.type.ref.scopeReferencedFields
			}
		} else if (value instanceof FormalValuePar) {
			if (value.type !== null) {
				value.type.ref.scopeReferencedFields
			}
		} else if (value instanceof FormalTemplatePar) {
			if (value.type !== null) {
				value.type.ref.scopeReferencedFields
			}
		} else if (value instanceof UnionFieldDef) {
			if (value.type !== null) {
				value.type.ref.scopeReferencedFields
			}
		} else if (value instanceof ModuleParameter) {
			val variable = value.findDesiredParent(ModulePar)
			if (variable.type.ref !== null) {
				variable.type.ref.scopeReferencedFields
			}
		} else if (value instanceof SingleConstDef) {
			val variable = value.findDesiredParent(ConstDef)
			if (variable.type.ref !== null) {
				variable.type.ref.scopeReferencedFields
			}
        } else if (value instanceof NamedObject) {
            val variable = value.findDesiredParent(ExtConstDef)
            if (variable.type.ref !== null) {
                variable.type.ref.scopeReferencedFields
            }
		}
	}

	private def Iterable<EObject> directFieldElements(FieldReference field) {
		if (field instanceof StructFieldDef) {
			if (field.type !== null) {
				field.type.ref.scopeReferencedFields
			}
		} else if (field instanceof FormalValuePar) {
			if (field.type !== null) {
				field.type.ref.scopeReferencedFields
			}
		} else if (field instanceof FormalTemplatePar) {
			if (field.type !== null) {
				field.type.ref.scopeReferencedFields
			}
		} else if (field instanceof UnionFieldDef) {
			if (field.type !== null) {
				field.type.ref.scopeReferencedFields
			}
		}
	}
	
	private def Iterable<EObject> directModuleElements(TTCN3Module module) {
		val ArrayList<EObject> list = newArrayList
		module.defs.scopeModuleVariable(list)
		for (ModuleDefinition g : module.defs.definitions.filter[it.def instanceof GroupDef]) {
			val GroupDef group = g.def as GroupDef
			if (group.list !== null) {
				group.list.scopeModuleVariable(list)
			}
		}		
		return list
	}
	
	private def Iterable<EObject> directGroupElements(GroupDef it) {
		val List<EObject> res = newArrayList
		if (list === null)
			return res

		for (ModuleDefinition g : list.definitions) {
			val elt = g.def
			if (elt instanceof GroupDef) {
				res.add(elt)
			} else if (elt instanceof ConstDef) {
				res.addAll(elt.defs.list)
			} else if (elt instanceof ModuleParDef) {
				res.addAll(elt.directModulePrams)
			} else if (elt instanceof FunctionDef) {
				res.add(elt)
			} else if (elt instanceof TemplateDef) {
				res.add(elt.base)
			}
		}
		return res
	}

	private def Iterable<RefValue> directModulePrams(ModuleParDef it) {
		val list = newArrayList
		if (it.param !== null) {
			for (ModuleParameter p : param.list.params) {
				list.add(p)
			}
		} else if (it.multitypeParam !== null) {
			for (ModulePar p : multitypeParam.params) {
				for (ModuleParameter pa : p.list.params) {
					list.add(pa)
				}
			}
		}
		return list
	}
}
