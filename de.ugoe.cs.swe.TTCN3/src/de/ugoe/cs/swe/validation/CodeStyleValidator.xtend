package de.ugoe.cs.swe.validation

import com.google.common.base.Strings
import com.google.common.collect.Sets
import com.google.inject.Inject
import de.ugoe.cs.swe.TTCN3Configuration.QualityCheckProfile
import de.ugoe.cs.swe.common.ConfigTools
import de.ugoe.cs.swe.common.MiscTools
import de.ugoe.cs.swe.common.logging.LoggingInterface.LogLevel
import de.ugoe.cs.swe.common.logging.LoggingInterface.MessageClass
import de.ugoe.cs.swe.scoping.TTCN3GlobalScopeProvider
import de.ugoe.cs.swe.tTCN3.AltConstruct
import de.ugoe.cs.swe.tTCN3.AltstepDef
import de.ugoe.cs.swe.tTCN3.AltstepInstance
import de.ugoe.cs.swe.tTCN3.BaseTemplate
import de.ugoe.cs.swe.tTCN3.ComponentDef
import de.ugoe.cs.swe.tTCN3.ConstDef
import de.ugoe.cs.swe.tTCN3.ConstList
import de.ugoe.cs.swe.tTCN3.EnumDefNamed
import de.ugoe.cs.swe.tTCN3.ExtFunctionDef
import de.ugoe.cs.swe.tTCN3.FieldExpressionList
import de.ugoe.cs.swe.tTCN3.FormalPortPar
import de.ugoe.cs.swe.tTCN3.FormalTemplatePar
import de.ugoe.cs.swe.tTCN3.FormalTimerPar
import de.ugoe.cs.swe.tTCN3.FormalValuePar
import de.ugoe.cs.swe.tTCN3.FunctionDef
import de.ugoe.cs.swe.tTCN3.FunctionInstance
import de.ugoe.cs.swe.tTCN3.GotoStatement
import de.ugoe.cs.swe.tTCN3.GroupDef
import de.ugoe.cs.swe.tTCN3.ImportDef
import de.ugoe.cs.swe.tTCN3.InLineTemplate
import de.ugoe.cs.swe.tTCN3.LabelStatement
import de.ugoe.cs.swe.tTCN3.ModuleControlBody
import de.ugoe.cs.swe.tTCN3.ModuleDefinition
import de.ugoe.cs.swe.tTCN3.ModulePar
import de.ugoe.cs.swe.tTCN3.ModuleParDef
import de.ugoe.cs.swe.tTCN3.ModuleParameter
import de.ugoe.cs.swe.tTCN3.PermutationMatch
import de.ugoe.cs.swe.tTCN3.PortDef
import de.ugoe.cs.swe.tTCN3.PortElement
import de.ugoe.cs.swe.tTCN3.PredefinedValue
import de.ugoe.cs.swe.tTCN3.RecordDefNamed
import de.ugoe.cs.swe.tTCN3.RecordOfDefNamed
import de.ugoe.cs.swe.tTCN3.RunsOnSpec
import de.ugoe.cs.swe.tTCN3.SetDefNamed
import de.ugoe.cs.swe.tTCN3.SetOfDefNamed
import de.ugoe.cs.swe.tTCN3.SignatureDef
import de.ugoe.cs.swe.tTCN3.SingleConstDef
import de.ugoe.cs.swe.tTCN3.SingleTempVarInstance
import de.ugoe.cs.swe.tTCN3.SingleVarInstance
import de.ugoe.cs.swe.tTCN3.StatementBlock
import de.ugoe.cs.swe.tTCN3.SubTypeDefNamed
import de.ugoe.cs.swe.tTCN3.TTCN3Module
import de.ugoe.cs.swe.tTCN3.TTCN3Package
import de.ugoe.cs.swe.tTCN3.TTCN3Reference
import de.ugoe.cs.swe.tTCN3.TempVarList
import de.ugoe.cs.swe.tTCN3.TemplateDef
import de.ugoe.cs.swe.tTCN3.TestcaseDef
import de.ugoe.cs.swe.tTCN3.TimerRefOrAll
import de.ugoe.cs.swe.tTCN3.TimerRefOrAny
import de.ugoe.cs.swe.tTCN3.Type
import de.ugoe.cs.swe.tTCN3.TypeDef
import de.ugoe.cs.swe.tTCN3.UnionDefNamed
import de.ugoe.cs.swe.tTCN3.VarList
import java.util.ArrayList
import java.util.HashMap
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.Set
import java.util.concurrent.ConcurrentHashMap
import java.util.regex.Matcher
import java.util.regex.Pattern
import org.eclipse.emf.ecore.EClass
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.nodemodel.INode
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.scoping.IGlobalScopeProvider
import org.eclipse.xtext.validation.AbstractDeclarativeValidator
import org.eclipse.xtext.validation.Check
import org.eclipse.xtext.validation.EValidatorRegistrar

import static de.ugoe.cs.swe.scoping.TTCN3GlobalScopeProvider.*

import static extension de.ugoe.cs.swe.common.TTCN3ReferenceHelper.*
import static extension de.ugoe.cs.swe.common.TTCN3ScopeHelper.*
import static extension org.eclipse.emf.ecore.util.EcoreUtil.*
import static extension org.eclipse.xtext.EcoreUtil2.*

class CheckDefinitionComeFirstParameter {
	public boolean hasOtherDefinitions
	public int previousLocalDefinitionsOrderLevel
}

class CodeStyleValidator extends AbstractDeclarativeValidator {
	val static Map<String, HashMap<String, List<TTCN3Reference>>> moduleLevelIdentifiersMap = new ConcurrentHashMap<String, HashMap<String, List<TTCN3Reference>>>();
	val ConfigTools configTools = ConfigTools.getInstance;
	var QualityCheckProfile activeProfile = configTools.selectedProfile as QualityCheckProfile
	final TTCN3StatisticsProvider statistics = TTCN3StatisticsProvider.getInstance

	@Inject
	private IGlobalScopeProvider globalScopeProvider;

	@Check
	def checkNoGotoStatements(GotoStatement goto) {
		goto.checkNoLabelsOrGotoStatements
	}

	@Check
	def checkNoGotoStatements(LabelStatement label) {
		label.checkNoLabelsOrGotoStatements
	}

	def private checkNoLabelsOrGotoStatements(TTCN3Reference ref) {
		if (!activeProfile.checkNoLabelsOrGotoStatements)
			return;

		var message = ""
		var methodName = ""
		val INode node = NodeModelUtils.getNode(ref)

		switch ref {
			GotoStatement: {
				message = "A \"goto\" statement is used!"
				methodName = "checkNoGotoStatements"
			}
			LabelStatement: {
				message = "A \"label\" statement is used!"
				methodName = "checkNoLabelStatements"
			}
		}

		statistics.incrementCountStyle

		warning(
			message,
			TTCN3Package.eINSTANCE.TTCN3Reference_Name,
			MessageClass.STYLE.toString,
			node.startLine.toString,
			node.endLine.toString,
			"6.1, " + methodName
		);
	}

	@Check
	def checkNoNestedAltStatements(AltConstruct alt) {
		if (!activeProfile.checkNoNestedAltStatements)
			return;

		var depth = alt.nestingDepth(AltConstruct)
		if (alt.findDesiredParent(AltstepDef) != null) {
			depth++
		}
		val INode node = NodeModelUtils.getNode(alt)
		if (depth > activeProfile.maximumAllowedNestingDepth) {
			statistics.incrementCountStyle
			val message = "Alt statement nesting depth (" + depth + ") exceeds maximum allowed nesting depth (" +
				activeProfile.maximumAllowedNestingDepth + ")!"
			warning(
				message,
				alt,
				null,
				MessageClass.STYLE.toString,
				node.startLine.toString,
				node.endLine.toString,
				"6.2, " + MiscTools.getMethodName()
			);
		}
	}

	@Check
	def checkLevelOfNestedCalls(FunctionInstance instance) {
		if (!activeProfile.checkLevelOfNestedCalls)
			return;

		val maxLevel = activeProfile.maxLevelOfNestedCalls;
		var depth = 1;
		var parent = instance;

		do {
			parent = parent.findDesiredParent(FunctionInstance)
			if (parent != null && (parent.ref instanceof FunctionDef || parent.ref instanceof BaseTemplate)) {
				depth++
			}
		} while (parent != null)

		val INode node = NodeModelUtils.getNode(instance)
		if (depth > maxLevel) {
			statistics.incrementCountStyle
			val message = "Level of nested functions (" + depth + ") exceeds maximum allowed nesting depth (" +
				maxLevel + ")!"
			warning(
				message,
				instance,
				null,
				MessageClass.STYLE.toString,
				node.startLine.toString,
				node.endLine.toString,
				"6.20, " + MiscTools.getMethodName()
			);
		}
	}

	@Check
	def checkListedVariableDeclarations(TempVarList list) {
		if (!activeProfile.checkListedVariableDeclarations)
			return;

		if (list.variables.size > 1) {
			statistics.incrementCountStyle
			val message = "Listed variable declarations are used!"
			val INode node = NodeModelUtils.getNode(list)
			warning(
				message,
				list,
				null,
				MessageClass.STYLE.toString,
				node.startLine.toString,
				node.endLine.toString,
				"6.21, " + MiscTools.getMethodName()
			);
		}
	}

	@Check
	def checkListedVariableDeclarations(VarList list) {
		if (!activeProfile.checkListedVariableDeclarations)
			return;

		if (list.variables.size > 1) {
			statistics.incrementCountStyle
			val message = "Listed variable declarations are used!"
			val INode node = NodeModelUtils.getNode(list)
			warning(
				message,
				list,
				null,
				MessageClass.STYLE.toString,
				node.startLine.toString,
				node.endLine.toString,
				"6.21, " + MiscTools.getMethodName()
			);
		}
	}

	@Check
	def checkInlineTemplates(InLineTemplate template) {
		if (!activeProfile.checkInlineTemplates)
			return;

		var found = false

		if (template.template.simple != null) {
			if (template.template.simple.expr instanceof FieldExpressionList) {
				found = true
			}
		}

		if (found) {
			statistics.incrementCountStyle
			val message = "In-line template is used!"
			val INode node = NodeModelUtils.getNode(template)
			warning(
				message,
				template,
				null,
				MessageClass.STYLE.toString,
				node.startLine.toString,
				node.startLine.toString,
				"6.22, " + MiscTools.getMethodName()
			);
		}
	}

	private def <T extends EObject> int nestingDepth(EObject o, Class<T> t) {
		var depth = -1
		var parent = o

		do {
			parent = parent.findDesiredParent(t)
			depth++
		} while (parent != null)

		return depth
	}

	@Check
	def checkNoPermutationKeyword(PermutationMatch permutation) {
		if (!activeProfile.checkNoPermutationKeyword)
			return;

		statistics.incrementCountStyle

		val INode node = NodeModelUtils.getNode(permutation)
		val message = "Keyword \"permutation\" is used!"
		warning(
			message,
			permutation,
			null,
			MessageClass.STYLE.toString,
			node.startLine.toString,
			node.endLine.toString,
			"6.4, " + MiscTools.getMethodName()
		);
	}

	@Check
	def checkNoAnyTypeKeyword(Type type) {
		if (!activeProfile.checkNoAnyTypeKeyword)
			return;

		if (type.pre != null && type.pre == 'anytype') {
			statistics.incrementCountStyle
			val INode node = NodeModelUtils.getNode(type)
			val message = "Keyword \"anytype\" is used!"
			warning(
				message,
				type,
				null,
				MessageClass.STYLE.toString,
				node.startLine.toString,
				node.endLine.toString,
				"6.5, " + MiscTools.getMethodName()
			);
		}
	}

	@Check
	def checkNoModifiedTemplateOfModifiedTemplate(TemplateDef template) {
		if (!activeProfile.checkNoModifiedTemplateOfModifiedTemplate)
			return;

		if (template.derived != null) {
			val baseTemplate = template.derived.template.findDesiredParent(TemplateDef)
			val INode node = NodeModelUtils.getNode(template)
			if (baseTemplate.derived != null) {
				statistics.incrementCountStyle
				val message = "Template \"" + template.base.name + "\" modifies another modified template (\"" +
					baseTemplate.base.name + "\")!"
				warning(
					message,
					template.base,
					null,
					MessageClass.STYLE.toString,
					node.startLine.toString,
					node.endLine.toString,
					"6.7, " + MiscTools.getMethodName()
				);
			}
		}
	}

	@Check
	def checkLocalDefinitionsComeFirst(SingleVarInstance variable) { // this considers timers, too
		if (!activeProfile.checkLocalDefinitionsComeFirst)
			return;

		variable.checkLocalDefinitionsComeFirstAll
	}

	@Check
	def checkLocalDefinitionsComeFirst(SingleConstDef variable) {
		if (!activeProfile.checkLocalDefinitionsComeFirst)
			return;

		variable.checkLocalDefinitionsComeFirstAll
	}

	@Check
	def checkLocalDefinitionsComeFirst(PortDef variable) {
		if (!activeProfile.checkLocalDefinitionsComeFirst)
			return;

		variable.checkLocalDefinitionsComeFirstAll
	}

	private def checkLocalDefinitionsComeFirstAll(TTCN3Reference definition) {
		val function = definition.findDesiredParent(FunctionDef)
		val altstep = definition.findDesiredParent(AltstepDef)
		val testcase = definition.findDesiredParent(TestcaseDef)
		val control = definition.findDesiredParent(ModuleControlBody)

		var lastDef = Integer.MIN_VALUE
		val line = NodeModelUtils.getNode(definition).startLine

		if (function != null) {
			if (function.statement != null && function.statement.def != null) {
				lastDef = NodeModelUtils.getNode(function.statement.def.last).startLine
			}
		}

		if (testcase != null) {
			if (testcase.statement != null && testcase.statement.def != null) {
				lastDef = NodeModelUtils.getNode(testcase.statement.def.last).startLine
			}
		}

		if (altstep != null && altstep.local.defs != null) {
			lastDef = NodeModelUtils.getNode(altstep.local.defs.last).startLine
		}

		if (control != null && control.list.localDef != null) {
			lastDef = NodeModelUtils.getNode(control.list.localDef.last).startLine
		}

		if (lastDef > Integer.MIN_VALUE && line > lastDef) {
			val message = "Local definition is not at the beginning!"
			var checkName = "checkLocalDefinitionsComeFirst"
			if (control != null) {
				checkName = "checkLocalDefinitionsComeFirstWithinControlPart"
			}
			statistics.incrementCountStyle
			warning(
				message,
				null,
				MessageClass.STYLE.toString,
				line.toString,
				line.toString,
				"6.8, " + checkName
			);
		}
	}

	@Check
	def checkLocalDefinitionsComeFirst(ModuleDefinition moduleDefinition) {
		if (!activeProfile.checkLocalDefinitionsComeFirst)
			return;

		val definition = moduleDefinition.def

		if (definition instanceof FunctionDef) {
			definition.statement.checkLocalDefinitionsComeFirstStatemanet
		} else if (definition instanceof TestcaseDef) {
			definition.statement.checkLocalDefinitionsComeFirstStatemanet
		} else if (definition instanceof AltstepDef) {
			definition.checkLocalDefinitionsComeFirstAltstepDef
		} else if (definition instanceof TypeDef) {
			if (definition.body.structured != null && definition.body.structured.component != null) {
				definition.body.structured.component.checkLocalDefinitionsComeFirstComponent
			}
		}
	}

	private def checkLocalDefinitionsComeFirstStatemanet(StatementBlock statement) {
		val CheckDefinitionComeFirstParameter params = new CheckDefinitionComeFirstParameter
		for (d : statement.def) {
			if (d.locDef != null && d.locDef.constDef != null) {
				d.locDef.constDef.checkLocalDefinitionsComeFirstOrder(params)
			} else if (d.locInst != null && d.locInst.timer != null) {
				d.locInst.timer.checkLocalDefinitionsComeFirstOrder(params)
			} else if (d.locInst != null && d.locInst.variable != null) {
				d.locInst.variable.checkLocalDefinitionsComeFirstOrder(params)
			}
		}
	}

	private def checkLocalDefinitionsComeFirstAltstepDef(AltstepDef altstep) {
		val CheckDefinitionComeFirstParameter params = new CheckDefinitionComeFirstParameter
		for (d : altstep.local.defs) {
			if (d.variable != null) {
				d.variable.checkLocalDefinitionsComeFirstOrder(params)
			} else if (d.timer != null) {
				d.timer.checkLocalDefinitionsComeFirstOrder(params)
			} else if (d.const != null) {
				d.const.checkLocalDefinitionsComeFirstOrder(params)
			}
		}
	}

	private def checkLocalDefinitionsComeFirstComponent(ComponentDef component) {
		val CheckDefinitionComeFirstParameter params = new CheckDefinitionComeFirstParameter
		for (d : component.defs) {
			if (d.element.port != null)
				d.element.port.checkLocalDefinitionsComeFirstOrder(params)
			else if (d.element.variable != null)
				d.element.variable.checkLocalDefinitionsComeFirstOrder(params)
			else if (d.element.timer != null)
				d.element.timer.checkLocalDefinitionsComeFirstOrder(params)
			else if (d.element.const != null)
				d.element.const.checkLocalDefinitionsComeFirstOrder(params)
			else if (d.element.template != null)
				d.element.template.checkLocalDefinitionsComeFirstOrder(params)
		}
	}

	private def checkLocalDefinitionsComeFirstOrder(EObject o, CheckDefinitionComeFirstParameter params) {
		val configuredOrder = localDefOrderClasses
		val int currentLocalDefinitionTypeOrderLevel = configuredOrder.indexOf(o.eClass);
		val INode node = NodeModelUtils.getNode(o)
		if (configuredOrder.contains(o.eClass)) {
			if (params.hasOtherDefinitions) {
				statistics.incrementCountStyle
				val message = "Local definition is not at the beginning!"
				warning(
					message,
					o,
					null,
					MessageClass.STYLE.toString,
					node.startLine.toString,
					node.endLine.toString,
					"6.8, " + "checkLocalDefinitionsComeFirst"
				);
			} else {
				if ((currentLocalDefinitionTypeOrderLevel < params.previousLocalDefinitionsOrderLevel)) {
					statistics.incrementCountStyle
					val message = "Local definition order is not maintained as configured (" +
						configuredOrder.eClassesToString + ")!"
					warning(
						message,
						o,
						null,
						MessageClass.STYLE.toString,
						node.startLine.toString,
						node.endLine.toString,
						"6.8, " + "checkLocalDefinitionsComeFirst"
					);

				} else {
					params.previousLocalDefinitionsOrderLevel = currentLocalDefinitionTypeOrderLevel
				}

			}
		} else {
			params.hasOtherDefinitions = true
		}
	}

	private def String eClassesToString(List<EClass> order) {
		var String res = ""
		var index = 0
		for (c : order) {
			res += c.name
			index++
			if (index < order.size)
				res += ", "
		}
		res
	}

	private def List<EClass> localDefOrderClasses() {
		val defs = activeProfile.localDefinitionTypes.toList
		val res = newArrayList
		for (d : defs) {
			if (d == "VarInstance") {
				res.add(TTCN3Package.eINSTANCE.varInstance)
			} else if (d == "ConstDef") {
				res.add(TTCN3Package.eINSTANCE.constDef)
			} else if (d == "TimerInstance") {
				res.add(TTCN3Package.eINSTANCE.timerInstance)
			} else if (d == "PortInstance") {
				res.add(TTCN3Package.eINSTANCE.portInstance)
			}
		}
		return res
	}

	@Check
	def checkImportsComeFirst(TTCN3Module module) {
		if (!activeProfile.checkImportsComeFirst)
			return;

		if (module.defs == null)
			return;

		var hasOtherDefs = false
		for (d : module.defs.definitions) {
			if (d.def instanceof ImportDef) {
				if (hasOtherDefs) {
					statistics.incrementCountStyle
					val message = "Import statement is not at the beginning!"
					val INode node = NodeModelUtils.getNode(d)
					warning(
						message,
						d,
						null,
						MessageClass.STYLE.toString,
						node.startLine.toString,
						node.endLine.toString,
						"6.9, " + MiscTools.getMethodName()
					);
				}
			} else {
				hasOtherDefs = true
			}
		}
	}

	@Check
	def checkNoDuplicatedModuleDefinitionIdentifiers(ModuleDefinition definition) {
		if (!activeProfile.checkNoDuplicatedModuleDefinitionIdentifiers)
			return;

		val parentModule = definition.findDesiredParent(TTCN3Module)
		var HashMap<String, List<TTCN3Reference>> identifiers = newHashMap
		var idExists = false
		var TTCN3Reference detailDef = null

		if (moduleLevelIdentifiersMap.containsKey(parentModule.name)) {
			identifiers = moduleLevelIdentifiersMap.get(parentModule.name)
		} else {
			moduleLevelIdentifiersMap.put(parentModule.name, identifiers)
		}

		if (definition.def instanceof ConstDef) {
			val constDefList = (definition.def as ConstDef).defs as ConstList
			for (SingleConstDef d : constDefList.list) {
				detailDef = d
				idExists = d.name.checkIdentifier(detailDef, identifiers)
			}
		} else if (definition.def instanceof ModuleParDef) {
			if ((definition.def as ModuleParDef).param != null) {
				for (ModuleParameter p : (definition.def as ModuleParDef).param.list.params) {
					detailDef = p
					idExists = p.name.checkIdentifier(detailDef, identifiers)
				}
			} else if ((definition.def as ModuleParDef).multitypeParam != null) {
				for (ModulePar p : (definition.def as ModuleParDef).multitypeParam.params) {
					for (ModuleParameter pa : p.list.params) {
						detailDef = pa
						idExists = pa.name.checkIdentifier(detailDef, identifiers)
					}
				}
			}
		} else if (definition.def instanceof FunctionDef) {
			detailDef = definition.def as FunctionDef
			idExists = (definition.def as FunctionDef).name.checkIdentifier(detailDef, identifiers)
		} else if (definition.def instanceof TemplateDef) {
			detailDef = (definition.def as TemplateDef).base
			idExists = (definition.def as TemplateDef).base.name.checkIdentifier(detailDef, identifiers)
		} else if (definition.def instanceof SignatureDef) {
			detailDef = definition.def as SignatureDef
			idExists = (definition.def as SignatureDef).name.checkIdentifier(detailDef, identifiers)
		} else if (definition.def instanceof TestcaseDef) {
			detailDef = definition.def as TestcaseDef
			idExists = (definition.def as TestcaseDef).name.checkIdentifier(detailDef, identifiers)
		} else if (definition.def instanceof AltstepDef) {
			detailDef = definition.def as AltstepDef
			idExists = (definition.def as AltstepDef).name.checkIdentifier(detailDef, identifiers)
		} else if (definition.def instanceof GroupDef) {
			detailDef = definition.def as GroupDef
			idExists = (definition.def as GroupDef).name.checkIdentifier(detailDef, identifiers)
		} else if (definition.def instanceof TypeDef) {
			val typeDef = definition.def as TypeDef
			if (typeDef.body.structured != null) {
				val struct = typeDef.body.structured
				if (struct.record != null && struct.record instanceof RecordDefNamed) {
					detailDef = struct.record as TTCN3Reference
					idExists = (struct.record as TTCN3Reference).name.checkIdentifier(detailDef, identifiers)
				} else if (struct.union != null && struct.union instanceof UnionDefNamed) {
					detailDef = struct.union as TTCN3Reference
					idExists = (struct.union as TTCN3Reference).name.checkIdentifier(detailDef, identifiers)
				} else if (struct.set != null && struct.set instanceof SetDefNamed) {
					detailDef = struct.set as TTCN3Reference
					idExists = (struct.set as TTCN3Reference).name.checkIdentifier(detailDef, identifiers)
				} else if (struct.recordOf != null && struct.recordOf instanceof RecordOfDefNamed) {
					detailDef = struct.recordOf as TTCN3Reference
					idExists = (struct.recordOf as TTCN3Reference).name.checkIdentifier(detailDef, identifiers)
				} else if (struct.setOf != null && struct.setOf instanceof SetOfDefNamed) {
					detailDef = struct.setOf as TTCN3Reference
					idExists = (struct.setOf as TTCN3Reference).name.checkIdentifier(detailDef, identifiers)
				} else if (struct.enumDef != null && struct.enumDef instanceof EnumDefNamed) {
					detailDef = struct.enumDef as TTCN3Reference
					idExists = (struct.enumDef as TTCN3Reference).name.checkIdentifier(detailDef, identifiers)
				} else if (struct.port != null) {
					detailDef = struct.port as TTCN3Reference
					idExists = (struct.port as TTCN3Reference).name.checkIdentifier(detailDef, identifiers)
				} else if (struct.component != null) {
					detailDef = struct.component as TTCN3Reference
					idExists = (struct.component as TTCN3Reference).name.checkIdentifier(detailDef, identifiers)
				}
			} else if (typeDef.body.sub != null) {
				if (typeDef.body.sub instanceof SubTypeDefNamed) {
					detailDef = typeDef.body.sub as TTCN3Reference
					idExists = (typeDef.body.sub as TTCN3Reference).name.checkIdentifier(detailDef, identifiers)
				}
			}
		}

		if (idExists) {
			statistics.incrementCountStyle
			var message = "Identifier \"" + detailDef.name + "\" on the module level has already been used in "
			val size = identifiers.get(detailDef.name).size - 1
			var i = 0;
			for (d : identifiers.get(detailDef.name)) {
				if (d != detailDef) {
					val INode node = NodeModelUtils.getNode(d)
					message += "<\"" + parentModule.name + ": " + node.startLine.toString + "\">"
					if (size > 1 && i < size - 1) {
						message += ", "
					}
					i++
				}
			}
			message += "!"
			val INode node = NodeModelUtils.getNode(detailDef)
			warning(
				message,
				detailDef,
				null,
				MessageClass.STYLE.toString,
				node.startLine.toString,
				node.startLine.toString,
				"6.10, " + MiscTools.getMethodName()
			);
		}
	}

	private def boolean checkIdentifier(String name, TTCN3Reference definition,
		HashMap<String, List<TTCN3Reference>> ids) {
		var res = false
		if (ids.containsKey(name)) {
			ids.get(name).add(definition)
			res = true
		} else {
			val list = newArrayList(definition)
			ids.put(name, list)
		}
		res
	}

	@Check
	def checkZeroReferencedModuleDefinitions(ModuleDefinition definition) {
		if (!activeProfile.checkZeroReferencedModuleDefinitions)
			return;

		// do not consider declarations groups?
		// val parentGroup = definition.findDesiredParent(GroupDef)		
		// if (parentGroup != null)
		// return;
		val parentModule = definition.findDesiredParent(TTCN3Module)

		val Pattern zeroReferencedModuleDefinitionsExcludedPattern = Pattern.compile(
			activeProfile.getZeroReferencedModuleDefinitionsExcludedRegExp())
		val Matcher zeroReferencedModuleDefinitionsExcludedMatcher = zeroReferencedModuleDefinitionsExcludedPattern.
			matcher(parentModule.name)

		if (zeroReferencedModuleDefinitionsExcludedMatcher.matches())
			return;

		var TTCN3Reference detailDef = null
		var notReferenced = false
		var boolean showEndLine = false

		if (definition.def instanceof ConstDef) {
			val constDefList = (definition.def as ConstDef).defs as ConstList
			for (SingleConstDef d : constDefList.list) {
				notReferenced = d.isUnreferenced(parentModule, true)
				detailDef = d
			}
		} else if (definition.def instanceof ModuleParDef) {
			if ((definition.def as ModuleParDef).param != null) {
				for (ModuleParameter p : (definition.def as ModuleParDef).param.list.params) {
					notReferenced = p.isUnreferenced(parentModule, true)
					detailDef = p
				}
			} else if ((definition.def as ModuleParDef).multitypeParam != null) {
				for (ModulePar p : (definition.def as ModuleParDef).multitypeParam.params) {
					for (ModuleParameter pa : p.list.params) {
						notReferenced = pa.isUnreferenced(parentModule, true)
						detailDef = pa
					}
				}
			}
		} else if (definition.def instanceof FunctionDef) {
			notReferenced = definition.def.isUnreferenced(parentModule, true)
			detailDef = definition.def as FunctionDef
		} else if (definition.def instanceof ExtFunctionDef) {
			notReferenced = definition.def.isUnreferenced(parentModule, true)
			detailDef = definition.def as ExtFunctionDef
		} else if (definition.def instanceof TemplateDef) {
			notReferenced = (definition.def as TemplateDef).base.isUnreferenced(parentModule, true)
			detailDef = (definition.def as TemplateDef).base
		} else if (definition.def instanceof SignatureDef) {
			notReferenced = definition.def.isUnreferenced(parentModule, true)
			detailDef = definition.def as SignatureDef
		} else if (definition.def instanceof TestcaseDef) {
			notReferenced = definition.def.isUnreferenced(parentModule, true)
			detailDef = definition.def as TestcaseDef
		} else if (definition.def instanceof AltstepDef) {
			notReferenced = definition.def.isUnreferenced(parentModule, true)
			detailDef = definition.def as AltstepDef
		} else if (definition.def instanceof TypeDef) {
			val body = (definition.def as TypeDef).body

			if (body.structured != null) {
				if (body.structured.recordOf != null && body.structured.recordOf instanceof RecordOfDefNamed) {
					notReferenced = body.structured.recordOf.isUnreferenced(parentModule, true)
					detailDef = body.structured.recordOf as RecordOfDefNamed
				} else if (body.structured.setOf != null && body.structured.setOf instanceof SetOfDefNamed) {
					notReferenced = body.structured.setOf.isUnreferenced(parentModule, true)
					detailDef = body.structured.setOf as SetOfDefNamed
				} else if (body.structured.record != null && body.structured.record instanceof RecordDefNamed) {
					notReferenced = body.structured.record.isUnreferenced(parentModule, true)
					detailDef = body.structured.record as RecordDefNamed
				} else if (body.structured.union != null && body.structured.union instanceof UnionDefNamed) {
					notReferenced = body.structured.union.isUnreferenced(parentModule, true)
					detailDef = body.structured.union as UnionDefNamed
				} else if (body.structured.set != null && body.structured.set instanceof SetDefNamed) {
					notReferenced = body.structured.set.isUnreferenced(parentModule, true)
					detailDef = body.structured.set as SetDefNamed
				} else if (body.structured.port != null && body.structured.port instanceof PortDef) {
					notReferenced = body.structured.port.isUnreferenced(parentModule, true)
					detailDef = body.structured.port as PortDef
					showEndLine = true
				} else if (body.structured.component != null && body.structured.component instanceof ComponentDef) {
					notReferenced = body.structured.component.isUnreferenced(parentModule, true)
					detailDef = body.structured.component as ComponentDef
				} else if (body.structured.enumDef != null && body.structured.enumDef instanceof EnumDefNamed) {
					notReferenced = body.structured.enumDef.isUnreferenced(parentModule, true)
					detailDef = body.structured.enumDef as EnumDefNamed
				}
			} else if (body.sub != null) {
				if (body.sub instanceof SubTypeDefNamed) {
					notReferenced = body.sub.isUnreferenced(parentModule, true)
					detailDef = body.sub as SubTypeDefNamed
				}
			}
		}

		if (notReferenced) {
			statistics.incrementCountStyle
			val message = "Definition for \"" + detailDef.name + "\" is never referenced!"
			val INode node = NodeModelUtils.getNode(detailDef)
			var endLine = ""
			if (showEndLine) {
				endLine = node.endLine.toString
			} else {
				endLine = node.startLine.toString
			}
			warning(
				message,
				detailDef,
				null,
				MessageClass.STYLE.toString,
				node.startLine.toString,
				endLine,
				"6.17, " + MiscTools.getMethodName()
			);
		}
	}

	private def boolean isUnreferenced(EObject type, TTCN3Module parent, boolean searchAllImports) {
		val target = newHashSet(type);
		var boolean found = parent.isReferenced2(target)

		if (searchAllImports && !found) {
			synchronized (IMPORTED_FROM) {
				if (IMPORTED_FROM.containsKey(parent)) {
					val list = IMPORTED_FROM.get(parent)
					for (i : 0 .. list.size - 1) {
						if (list.get(i).isReferenced2(target)) {
							return false
						}
					}
				}
			}
		}

		return !found
	}

	@Check
	def checkNoInlineTemplates(InLineTemplate template) {
		if (!activeProfile.checkNoInlineTemplates)
			return;

		// only consider templates with a type
		if (template.type == null)
			return;

		statistics.incrementCountStyle

		val message = "Inline Template is used!"
		val INode node = NodeModelUtils.getNode(template)
		warning(
			message,
			null,
			MessageClass.STYLE.toString,
			node.startLine.toString,
			node.endLine.toString,
			"6.13, " + MiscTools.getMethodName()
		);
	}

	@Check
	def checkNoOverSpecificRunsOnClauses(RunsOnSpec runsOn) {
		if (!activeProfile.checkNoOverSpecificRunsOnClauses)
			return;

		// As of v1.0.3, test cases are not considered in this check
		val parent = runsOn.eContainer
		var ComponentDef component = null
		val ArrayList<TTCN3Reference> consideredAltOrFunc = newArrayList
		if (parent instanceof AltstepDef) {
			component = parent.spec.component
			if (!parent.specifiedComponentResolved(component)) {
				return;
			}
		} else if (parent instanceof FunctionDef) {
			component = parent.runsOn.component
			if (!parent.specifiedComponentResolved(component)) {
				return;
			}
		} else {
			return;
		}

		val overSpecifiedFirstLevel = !component.isReferencedComponentSpec(parent)
		val message = "Definition for \"" + (parent as TTCN3Reference).name +
			"\" contains an over specific runs on clause!"
		val INode node = NodeModelUtils.getNode(parent)

		if (overSpecifiedFirstLevel) {
			var overSpecified = true
			if (activeProfile.recursionInCheckNoOverSpecificRunsOnClauses) {
				consideredAltOrFunc.add(parent as TTCN3Reference)
				overSpecified = !(parent as TTCN3Reference).isReferencedInHierarchy(consideredAltOrFunc)
			}

			if (overSpecified) {
				statistics.incrementCountStyle
				warning(
					message,
					parent,
					null,
					MessageClass.STYLE.toString,
					node.startLine.toString,
					node.startLine.toString, // only show the starting line
					"6.13, " + "checkNoOverSpecificRunsOnClauses"
				);
			}
		}

	}

	private def boolean specifiedComponentResolved(TTCN3Reference parent, ComponentDef component) {
		if (Strings.isNullOrEmpty(component.name)) {
			statistics.incrementCountStyle
			val message = "" + "Runs on component reference of \"" + parent.name +
				"\" cannot be resolved. It may not be possible to determine whether construct has an over-specific runs on clause!"
			val INode node = NodeModelUtils.getNode(parent)
			info(
				message,
				parent,
				null,
				MessageClass.STYLE.toString,
				node.startLine.toString,
				node.endLine.toString,
				"6.13, " + "checkNoOverSpecificRunsOnClauses",
				LogLevel.INFORMATION.toString
			);
			return false
		}
		return true
	}

	// TODO: check next level functions
	private def ArrayList<TTCN3Reference> nextLevelFunctionsOrAltsteps(TTCN3Reference parent,
		ArrayList<TTCN3Reference> consideredAltOrFunc) {
		val ArrayList<TTCN3Reference> nextLevelCheck = newArrayList

		for (f : parent.getAllContentsOfType(FunctionInstance).map[it.ref]) {
			if (consideredAltOrFunc.contains(f)) {
				statistics.incrementCountStyle
				val message = "" + "Possible cyclic call sequence: \"" + f.name + "\". Skipping..."
				val INode node = NodeModelUtils.getNode(f)
				info(
					message,
					f,
					null,
					MessageClass.STYLE.toString,
					node.startLine.toString,
					node.endLine.toString,
					"6.13, " + "collectReferencedFunctionsAndAltsteps",
					LogLevel.INFORMATION.toString
				);
			} else {
				nextLevelCheck.add(f)
			}
		}

		for (a : parent.getAllContentsOfType(AltstepInstance).map[it.ref]) {
			if (consideredAltOrFunc.contains(a)) {
				statistics.incrementCountStyle
				val message = "" + "Possible cyclic call sequence: \"" + a.name + "\". Skipping..."
				val INode node = NodeModelUtils.getNode(a)
				info(
					message,
					a,
					null,
					MessageClass.STYLE.toString,
					node.startLine.toString,
					node.endLine.toString,
					"6.13, " + "collectReferencedFunctionsAndAltsteps",
					LogLevel.INFORMATION.toString
				);
			} else {
				nextLevelCheck.add(a)
			}
		}
		return nextLevelCheck
	}

	private def boolean isRunningOnSameComponent(TTCN3Reference parent, TTCN3Reference nextLevel) {
		if (nextLevel instanceof FunctionDef) {
			if (parent instanceof FunctionDef) {
				if (nextLevel.runsOn.component == parent.runsOn.component) {
					return true;
				}
			} else if (parent instanceof AltstepDef) {
				if (nextLevel.runsOn.component == parent.spec.component) {
					return true;
				}
			}
		} else if (nextLevel instanceof AltstepDef) {
			if (parent instanceof FunctionDef) {
				if (nextLevel.spec.component == parent.runsOn.component) {
					return true;
				}
			} else if (parent instanceof AltstepDef) {
				if (nextLevel.spec.component == parent.spec.component) {
					return true;
				}
			}
		}
		return false;
	}

	private def boolean isReferencedInHierarchy(TTCN3Reference parent, ArrayList<TTCN3Reference> consideredAltOrFunc) {
		var ArrayList<TTCN3Reference> nextLevelCheck = parent.nextLevelFunctionsOrAltsteps(consideredAltOrFunc)
		do {
			for (o : nextLevelCheck) {
				if (o instanceof FunctionDef) {
					if (o.runsOn != null) {
						if (o.specifiedComponentResolved(o.runsOn.component)) {
							//check if the component is the same as the one from the context!!!
							if (o.isRunningOnSameComponent(parent) && o.runsOn.component.isReferencedComponentSpec(o)) {
								return true
							} else {
								var ArrayList<TTCN3Reference> nlc = new ArrayList<TTCN3Reference>()
								nlc.addAll(nextLevelCheck)
								nlc.remove(o)
								nlc.addAll(o.nextLevelFunctionsOrAltsteps(consideredAltOrFunc))
								nextLevelCheck = nlc
							}
						}
					} else {
						// TODO: error message?
						var ArrayList<TTCN3Reference> nlc = new ArrayList<TTCN3Reference>()
						nlc.addAll(nextLevelCheck)
						nlc.remove(o)
						nextLevelCheck = nlc
					}
				} else if (o instanceof AltstepDef) {
					if (o.spec != null) {
						if (o.specifiedComponentResolved(o.spec.component)) {
							//check if the component is the same as the one from the context!!!
							if (o.isRunningOnSameComponent(parent) && o.spec.component.isReferencedComponentSpec(o)) {
								return true
							} else {
								var ArrayList<TTCN3Reference> nlc = new ArrayList<TTCN3Reference>()
								nlc.addAll(nextLevelCheck)
								nlc.remove(o)
								nlc.addAll(o.nextLevelFunctionsOrAltsteps(consideredAltOrFunc))
								nextLevelCheck = nlc
							}
						}
					} else {
						// TODO: error message?
						var ArrayList<TTCN3Reference> nlc = new ArrayList<TTCN3Reference>()
						nlc.addAll(nextLevelCheck)
						nlc.remove(o)
						nextLevelCheck = nlc
					}
				} else if (o.eIsProxy) {
					var ArrayList<TTCN3Reference> nlc = new ArrayList<TTCN3Reference>()
					nlc.addAll(nextLevelCheck)
					nlc.remove(o)
					nextLevelCheck = nlc
				} else {
					//handle other cases?
					var ArrayList<TTCN3Reference> nlc = new ArrayList<TTCN3Reference>()
					nlc.addAll(nextLevelCheck)
					nlc.remove(o)
					nextLevelCheck = nlc
				}
			}
		} while (nextLevelCheck != null && !nextLevelCheck.empty)
		return false
	}

	private def boolean isReferencedComponentSpec(ComponentDef component, EObject parent) {
		val variables = component.componentMembers
        if (activeProfile.aliasInCheckNoOverSpecificRunsOnClauses) {
            if (variables.empty) { //alias
    		    variables.addAll(component.directlyInheritedComponentMembers)    
            }
		}
		
		if (parent.isReferenced2(variables, false)) {
			return true
		} else {
			return false
		}
	}

    private def HashSet<EObject> getDirectlyInheritedComponentMembers(ComponentDef component) {
        val HashSet<EObject> inheritedVariables = Sets.newHashSet
        for (e : component.extends) {
            inheritedVariables.addAll(e.componentMembers)
        }
        return inheritedVariables
    }


    private def HashSet<EObject> getInheritedComponentMembers(ComponentDef component) {
        val HashSet<EObject> inheritedVariables = Sets.newHashSet
        for (e : component.extends) {
            inheritedVariables.addAll(e.componentMembers)
            inheritedVariables.addAll(e.inheritedComponentMembers)    
        }
        return inheritedVariables
    }

    private def HashSet<EObject> getComponentMembers(ComponentDef component) {
        val HashSet<EObject> variables = Sets.newHashSet
		for (d : component.defs) {
			val element = d.element
			if (element.port !== null) {
				for (p : element.port.instances) {
					variables.add(p)
				}
			} else if (element.const !== null) {
				for (c : element.const.defs.list) {
					variables.add(c)
				}
			} else if (element.timer !== null) {
				for (t : element.timer.list.variables) {
					variables.add(t)
				}
			} else if (element.variable !== null) {
				if (element.variable.list !== null) {
					for (v : element.variable.list.variables) {
						variables.add(v)
					}
				}
				if (element.variable.tempList !== null) {
					for (tv : element.variable.tempList.variables) {
						variables.add(tv)
					}
				}
			}
		}
        return variables
    }

	private def ArrayList<TTCN3Reference> findAllParameters(EObject parent) {
		val ArrayList<TTCN3Reference> paramList = newArrayList
		paramList.addAll(parent.getAllContentsOfType(FormalValuePar))
		paramList.addAll(parent.getAllContentsOfType(FormalTimerPar))
		paramList.addAll(parent.getAllContentsOfType(FormalTemplatePar))
		paramList.addAll(parent.getAllContentsOfType(FormalPortPar))
		return paramList
	}

	@Check
	def checkNoUnusedFormalParameters(AltstepDef altstep) {
		if (activeProfile.checkNoUnusedFormalParameters) {
			if (altstep.params != null) {
				val ArrayList<TTCN3Reference> paramList = altstep.params.findAllParameters
				altstep.checkNoUnusedFormalParameters(paramList)
			}
		}
	}

	@Check
	def checkNoUnusedFormalParameters(TestcaseDef testcase) {
		if (activeProfile.checkNoUnusedFormalParameters) {
			if (testcase.parList != null) {
				val ArrayList<TTCN3Reference> paramList = testcase.parList.findAllParameters
				testcase.checkNoUnusedFormalParameters(paramList)
			}
		}
	}

	@Check
	def checkNoUnusedFormalParameters(FunctionDef function) {
		if (activeProfile.checkNoUnusedFormalParameters) {
			if (function.parameterList != null) {
				val ArrayList<TTCN3Reference> paramList = function.parameterList.findAllParameters
				function.checkNoUnusedFormalParameters(paramList)
			}
		}
	}

	@Check
	def checkNoUnusedFormalParameters(BaseTemplate template) {
		if (!activeProfile.checkNoUnusedFormalParameters)
			return

		var current = template
		val ArrayList<TTCN3Reference> baseParams = newArrayList
		val ArrayList<TTCN3Reference> paramList = newArrayList
		val ArrayList<BaseTemplate> consideredTemplates = newArrayList
		consideredTemplates.add(current)

		do {
			if ((current.eContainer as TemplateDef).derived != null) {
				if (consideredTemplates.contains((current.eContainer as TemplateDef).derived.template)) {
					statistics.incrementCountStyle
					val message = "" + "Possible cyclic modifies sequence: \"" + template.name + "\". Skipping..."
					val INode node = NodeModelUtils.getNode(template)
					info(
						message,
						template,
						null,
						MessageClass.STYLE.toString,
						node.startLine.toString,
						node.endLine.toString,
						"6.13, " + MiscTools.getMethodName(),
						LogLevel.INFORMATION.toString
					);
					current = null
				} else {
					current = (current.eContainer as TemplateDef).derived.template
					if (current != null) {
						consideredTemplates.add(current)
						baseParams.addAll(current.parList.findAllParameters)
					}
				}
			} else {
				current = null
			}
		} while (current != null)

		for (p : template.parList.findAllParameters) {
			if (baseParams.filter[(it as TTCN3Reference).name == (p as TTCN3Reference).name].size == 0) {
				paramList.add(p)
			}
		}

		template.checkNoUnusedFormalParameters(paramList)
	}

	private def checkNoUnusedFormalParameters(EObject parent, ArrayList<TTCN3Reference> params) {
		val module = parent.findDesiredParent(TTCN3Module)
		val listAll = parent.eAllOfType(TimerRefOrAll)
		val listAny = parent.eAllOfType(TimerRefOrAny)
		var referenced = true

		for (p : params) {
			referenced = true
			if (p.isUnreferenced(module, false)) { // TODO: Parameters passed to other functions are not recognized.
				referenced = false
			}
			
			// hack to find timer references in TimeoutStatement statements
			// TODO: improve this...
			for (t : listAll) {
				val INode nodeRef = NodeModelUtils.getNode(t)
				if (p.name.equals(nodeRef.text.trim)) {
					referenced = true
				}
			}
			for (t : listAny) {
				val INode nodeRef = NodeModelUtils.getNode(t)
				if (p.name.equals(nodeRef.text.trim)) {
					referenced = true
				}
			}

			if (!referenced) {
				statistics.incrementCountStyle
				val message = "Formal parameter \"" + (p as TTCN3Reference).name + "\" in definition for \"" +
					(parent as TTCN3Reference).name + "\" is never used!"
				val INode node = NodeModelUtils.getNode(p)
				warning(
					message,
					p,
					null,
					MessageClass.STYLE.toString,
					node.startLine.toString,
					node.endLine.toString,
					"6.11, " + MiscTools.getMethodName()
				);
			}
		}
	}

	@Check
	def checkNoUnusedLocalDefinitions(SingleVarInstance variable) { // this considers timers, too
		if (!activeProfile.checkNoUnusedLocalDefinitions) {
			return
		}
		variable.checkNoUnusedLocalDefinitionsAll
	}

	@Check
	def checkNoUnusedLocalDefinitions(SingleConstDef variable) {
		if (!activeProfile.checkNoUnusedLocalDefinitions) {
			return
		}
		variable.checkNoUnusedLocalDefinitionsAll
	}

	@Check
	def checkNoUnusedLocalDefinitions(SingleTempVarInstance variable) {
		if (!activeProfile.checkNoUnusedLocalDefinitions) {
			return
		}
		variable.checkNoUnusedLocalDefinitionsAll
	}

	@Check
	def checkNoUnusedLocalDefinitions(BaseTemplate variable) {
		if (!activeProfile.checkNoUnusedLocalDefinitions) {
			return
		}
		variable.checkNoUnusedLocalDefinitionsAll
	}

	@Check
	def checkNoUnusedLocalDefinitions(PortElement variable) {
		if (!activeProfile.checkNoUnusedLocalDefinitions) {
			return
		}
		variable.checkNoUnusedLocalDefinitionsAll
	}

	private def checkNoUnusedLocalDefinitionsAll(TTCN3Reference variable) {
		val component = variable.findDesiredParent(ComponentDef)
		val parentControl = variable.findDesiredParent(ModuleControlBody)
		val parent = variable.variableLocal
		if (parent != null || parentControl != null) {
			val module = variable.findDesiredParent(TTCN3Module)
			val isComponentDefinition = (component != null)
			val unreferenced = variable.isUnreferenced(module, isComponentDefinition)
			if (unreferenced) {
				statistics.incrementCountStyle
				var varName = "Module Control Part"
				if (parent != null) {
					varName = parent.name
				}
				val message = "Local definition for \"" + variable.name + "\" in definition of \"" + varName +
					"\" is never used!"
				val INode node = NodeModelUtils.getNode(variable)
				warning(
					message,
					variable,
					null,
					MessageClass.STYLE.toString,
					node.startLine.toString,
					node.endLine.toString,
					"6.11, " + "checkNoUnusedLocalDefinitions"
				);
			}
		}
	}

	private def TTCN3Reference variableLocal(EObject variable) {
		val parentFunction = variable.findDesiredParent(FunctionDef)
		val parentAltStep = variable.findDesiredParent(AltstepDef)
		val parentComponent = variable.findDesiredParent(ComponentDef)
		val parentTestcase = variable.findDesiredParent(TestcaseDef)

		if (parentFunction != null) {
			return parentFunction
		}
		if (parentAltStep != null) {
			return parentAltStep
		}
		if (parentComponent != null) {
			return parentComponent
		}
		if (parentTestcase != null) {
			return parentTestcase
		}

	}

	@Check
	def checkNoLiterals(PredefinedValue value) {
		if (!activeProfile.checkNoLiterals) {
			return
		}

		val parentModulePar = value.findDesiredParent(ModuleParDef)
		val parentConstDef = value.findDesiredParent(ConstDef)
		val parentTemplate = value.findDesiredParent(TemplateDef)

		if (parentModulePar == null && parentConstDef == null && parentTemplate == null) {
			if (value.bstring != null || value.integer != null || value.float != null || value.hstring != null ||
				value.ostring != null || value.charString != null) {
				statistics.incrementCountStyle
				val message = "Literal value is used!"
				val INode node = NodeModelUtils.getNode(value)
				warning(
					message,
					value,
					null,
					MessageClass.STYLE.toString,
					node.startLine.toString,
					node.endLine.toString,
					"6.12, " + MiscTools.getMethodName()
				);
			}
		}
	}

	@Check
	def checkNoUnusedImports(TTCN3Module module) {
		if (!activeProfile.checkNoUnusedImports) {
			return
		}

		val TTCN3GlobalScopeProvider scopeProvider = globalScopeProvider as TTCN3GlobalScopeProvider
		if (scopeProvider == null) {
			return
		}

		synchronized (IMPORTS) {
			if (IMPORTS.containsKey(module.eResource)) {
				val list = IMPORTS.get(module.eResource)
				for (i : 0 .. list.size - 1) {
					val d = list.get(i)
					if (d.module == null) {
						statistics.incrementCountStyle
						val message = "Imported module \"" + d.importDef.name + "\" cannot be resolved!"
						val INode node = NodeModelUtils.getNode(d.importDef)
						info(
							message,
							d.importDef,
							null,
							MessageClass.STYLE.toString,
							node.startLine.toString,
							node.endLine.toString,
							"6.18, " + MiscTools.getMethodName(),
							LogLevel.INFORMATION.toString
						);
					}

					val Set<EObject> target = newHashSet(d.exportedObjects.map[getEObjectOrProxy]);
					var boolean found = module.isReferenced2(target, false)

					if (!found && d.module != null) {
						statistics.incrementCountStyle
						val message = "No definitions from imported module \"" + d.importDef.name + "\" are ever used!"
						val INode node = NodeModelUtils.getNode(d.importDef)
						warning(
							message,
							d.importDef,
							null,
							MessageClass.STYLE.toString,
							node.startLine.toString,
							node.endLine.toString,
							"6.18, " + MiscTools.getMethodName()
						);
					}
				}
			}
		}
	}
	
	@Check
	def checkNoTabs(TTCN3Module module) {
		if (!activeProfile.checkNoTabs) {
			return
		}
		
		var String[] segments = module.URI.path.split("\\\\")

		// This only works with unique file names, but this assumption should be fulfilled
		// otherwise the path should be hashed
		if (TTCN3GlobalScopeProvider.FOUND_TABS.containsKey(segments.last)) {
			for (String[] s : TTCN3GlobalScopeProvider.FOUND_TABS.get(segments.last)) {
				statistics.incrementCountStyle
				val message = "Line contains a tab character! (at column " + s.get(1) + ")"
				warning(
					message,
					null,
					MessageClass.STYLE.toString,
					s.get(0),
					s.get(0),
					MiscTools.getMethodName()
				);
			}				
		}		
	}	
	
	override register(EValidatorRegistrar registrar) {
		// not needed for classes used as ComposedCheck
	}
}
