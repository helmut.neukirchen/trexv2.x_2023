package de.ugoe.cs.swe.validation

import de.ugoe.cs.swe.TTCN3Configuration.QualityCheckProfile
import de.ugoe.cs.swe.common.ConfigTools
import de.ugoe.cs.swe.common.MiscTools
import de.ugoe.cs.swe.common.logging.LoggingInterface.LogLevel
import de.ugoe.cs.swe.common.logging.LoggingInterface.MessageClass
import de.ugoe.cs.swe.tTCN3.AltstepDef
import de.ugoe.cs.swe.tTCN3.ControlStatement
import de.ugoe.cs.swe.tTCN3.ControlStatementOrDefList
import de.ugoe.cs.swe.tTCN3.ExtFunctionDef
import de.ugoe.cs.swe.tTCN3.FunctionDef
import de.ugoe.cs.swe.tTCN3.FunctionInstance
import de.ugoe.cs.swe.tTCN3.FunctionStatement
import de.ugoe.cs.swe.tTCN3.FunctionStatementList
import de.ugoe.cs.swe.tTCN3.LogStatement
import de.ugoe.cs.swe.tTCN3.PredefinedValue
import de.ugoe.cs.swe.tTCN3.SetLocalVerdict
import de.ugoe.cs.swe.tTCN3.TTCN3Reference
import de.ugoe.cs.swe.tTCN3.TestcaseDef
import de.ugoe.cs.swe.tTCN3.Value
import de.ugoe.cs.swe.tTCN3.VerdictTypeValue
import java.util.regex.Matcher
import java.util.regex.Pattern
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.nodemodel.INode
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.validation.AbstractDeclarativeValidator
import org.eclipse.xtext.validation.Check
import org.eclipse.xtext.validation.EValidatorRegistrar

import static extension de.ugoe.cs.swe.common.TTCN3ScopeHelper.*
import static extension org.eclipse.xtext.EcoreUtil2.*

class LogValidator extends AbstractDeclarativeValidator {
	val ConfigTools configTools = ConfigTools.getInstance;
	var QualityCheckProfile activeProfile = configTools.selectedProfile as QualityCheckProfile

	@Check
	def checkLogStatementFormat(FunctionStatementList functionStmList) {
		if (!activeProfile.checkLogStatementFormat) 
			return;
		
		val parentTC = functionStmList.findDesiredParent(TestcaseDef)
		val parentFunc = functionStmList.findDesiredParent(FunctionDef)
		val parentAlt = functionStmList.findDesiredParent(AltstepDef)
		var TTCN3Reference parent = null;

		if (parentTC != null) {
			parent = parentTC;
		} else if (parentFunc != null) {
			parent = parentFunc;
		} else if (parentAlt != null) {
			parent = parentAlt;
		}

		functionStmList.eContents.checkStatementList(parent)
	}

	@Check
	def checkLogStatementFormat(ControlStatementOrDefList ctrlStmList) {
		if (!activeProfile.checkLogStatementFormat) 
			return;

		ctrlStmList.eContents.checkStatementList(null)
	}

	private def checkStatementList(EList<EObject> statements, TTCN3Reference parent) {
		var INode node = null
		var String statementToCheck = null
		var other = false
		var LogStatement log = null
		for (s : statements) {
			if (s instanceof FunctionStatement) {
				log = s.findLogStatement
			} else if (s instanceof ControlStatement) {
				log = s.findLogStatement
			} 
			if (log != null) {
				if (other) {
					if (statementToCheck != null) {
						statementToCheck.checkLogStatementFormat(node, parent)
					}
					other = false
					statementToCheck = null
				}

				node = NodeModelUtils.getNode(log)

				if (statementToCheck == null)
					statementToCheck = "";

				statementToCheck += log.concatLogItems
				if (!activeProfile.processSubsequentLogStatementsAsOne) {
					statementToCheck.checkLogStatementFormat(node, parent)
					statementToCheck = null
				}
			} else {
				other = true
			}
		}
		if (statementToCheck != null) {
			statementToCheck.checkLogStatementFormat(node, parent)
		}
	}

	private def LogStatement findLogStatement(ControlStatement control) {
		if (control.basic != null && control.basic.log != null) {
			return control.basic.log
		} else {
			return null
		}
	}

	private def LogStatement findLogStatement(FunctionStatement stm) {
		if (stm.basic != null && stm.basic.log != null) {
			return stm.basic.log
		} else {
			return null
		}
	}

	private def String concatLogItems(LogStatement log) {
		var logString = ""
		val items = log.getAllContentsOfType(PredefinedValue)
		for (i : items) {
			if (i.charString != null) {
				var String subString = i.charString.toString.subSequence(1, i.charString.toString.length - 1) as String
				logString += subString.replaceAll("\"\"", "\"")
			}
		}
		return logString;
	}

	private def checkLogStatementFormat(String log, INode node, TTCN3Reference parent) {
		var problemOccured = false
		var warning = ""
		val Pattern logPattern = Pattern.compile(activeProfile.getLogFormatRegExp(), Pattern.DOTALL)
		val Matcher logMatcher = logPattern.matcher(log)

		if (!logMatcher.matches()) {
			problemOccured = true;
			warning = "Invalid log format (\"" + log + "\")!";
		} else if (!logMatcher.group(1).equals("") && parent != null) {
			val logItemContainerIdentifier = logMatcher.group(1)
			if (!parent.name.equals(logItemContainerIdentifier)) {
				problemOccured = true;
				warning = "Log statement contains invalid container identifier (\"" + logItemContainerIdentifier +
					"\", should be \"" + parent.name + "\")!";
			}
		}

		if (problemOccured) {
			TTCN3StatisticsProvider.getInstance.incrementCountLog
			warning(
				warning,
				null,
				MessageClass.LOGGING.toString,
				node.startLine.toString,
				node.endLine.toString,
				"5.1, " + "checkLogFormat"
			);
		}
	}

	@Check
	def checkExternalFunctionInvocationPrecededByLogStatement(FunctionInstance function) {
		if (!activeProfile.checkExternalFunctionInvocationPrecededByLogStatement)
			return;

		val INode node = NodeModelUtils.getNode(function)
		var problemsOccured = false
		
		if (function.ref.name == null) {
			TTCN3StatisticsProvider.getInstance.incrementCountLog
			val INode functionNode = NodeModelUtils.getNode(function)
			val message = "Function definition for \"" + functionNode.text.trim +
					"\" cannot be resolved! It cannot be determined if it is an external function call!"
			info(
				message,
				null,
				MessageClass.LOGGING.toString,
				functionNode.startLine.toString,
				functionNode.endLine.toString,
				"5.3, " + MiscTools.getMethodName(),
				LogLevel.INFORMATION.toString
			);			
		}
		
		if (function.ref instanceof ExtFunctionDef) {
			val parent = function.findDesiredParent(FunctionStatement)
			val parentList = function.findDesiredParent(FunctionStatementList)
			if (parent == null) {
				TTCN3StatisticsProvider.getInstance.incrementCountLog
				val message = "Call to external function \"" + function.ref.name +
					"\" within a definition on the module level. It cannot be preceded by a log statement!"
				info(
					message,
					null,
					MessageClass.LOGGING.toString,
					node.startLine.toString,
					node.endLine.toString,
					"5.3, " + MiscTools.getMethodName(),
					LogLevel.INFORMATION.toString
				);
			} else {
				val items = parentList.eContents
				val idx = items.indexOf(parent)
				if (idx > 0 && idx < items.size) {
					val log = (items.get(idx - 1) as FunctionStatement).findLogStatement
					if (log == null) {
						problemsOccured = true
					}
				} else {
					problemsOccured = true
				}
			}
		}

		if (problemsOccured) {
			TTCN3StatisticsProvider.getInstance.incrementCountLog
			val message = "Call to external function \"" + function.ref.name + "\" not preceded by a log statement!"
			warning(
				message,
				null,
				MessageClass.LOGGING.toString,
				node.startLine.toString,
				node.endLine.toString,
				"5.3, " + MiscTools.getMethodName()
			);
		}
	}

	@Check
	def checkInconcOrFailSetverdictPrecededByLog(SetLocalVerdict verdict) {
		if (!activeProfile.checkInconcOrFailSetVerdictPrecededByLog)
			return;

		val parent = verdict.findDesiredParent(FunctionStatement)
		val parentList = verdict.findDesiredParent(FunctionStatementList)
		val INode node = NodeModelUtils.getNode(verdict)
		var problemsOccured = false
		val exp = verdict.expression
		var isInconcOrFail = false

		if (exp  instanceof Value) {
			if (exp.predef != null && exp.predef.verdictType != null) {
				if (exp.predef.verdictType == VerdictTypeValue.INCONC || exp.predef.verdictType == VerdictTypeValue.FAIL) {
					isInconcOrFail = true
				}
			}
		}

		if (!isInconcOrFail)
			return;

		val items = parentList.eContents
		val idx = items.indexOf(parent)
		if (idx > 0 && idx < items.size) {
			val log = (items.get(idx - 1) as FunctionStatement).findLogStatement
			if (log == null) {
				problemsOccured = true
			}
		} else {
			problemsOccured = true
		}

		if (problemsOccured) {
			TTCN3StatisticsProvider.getInstance.incrementCountLog
			val message = "No log statement precedes a fail or inconc setverdict statement!!"
			warning(
				message,
				null,
				MessageClass.LOGGING.toString,
				node.startLine.toString,
				node.endLine.toString,
				"5.4, " + MiscTools.getMethodName()
			);
		}
	}

	override register(EValidatorRegistrar registrar) {
		//not needed for classes used as ComposedCheck
	}
}
