package de.ugoe.cs.swe.validation

import de.ugoe.cs.swe.tTCN3.AltstepDef
import de.ugoe.cs.swe.tTCN3.AltstepLocalDef
import de.ugoe.cs.swe.tTCN3.AltstepLocalDefList
import de.ugoe.cs.swe.tTCN3.ComponentDef
import de.ugoe.cs.swe.tTCN3.ComponentDefList
import de.ugoe.cs.swe.tTCN3.ConstDef
import de.ugoe.cs.swe.tTCN3.ConstList
import de.ugoe.cs.swe.tTCN3.EnumDef
import de.ugoe.cs.swe.tTCN3.EnumDefNamed
import de.ugoe.cs.swe.tTCN3.Enumeration
import de.ugoe.cs.swe.tTCN3.EnumerationList
import de.ugoe.cs.swe.tTCN3.ForStatement
import de.ugoe.cs.swe.tTCN3.FunctionDef
import de.ugoe.cs.swe.tTCN3.FunctionDefList
import de.ugoe.cs.swe.tTCN3.FunctionFormalPar
import de.ugoe.cs.swe.tTCN3.FunctionFormalParList
import de.ugoe.cs.swe.tTCN3.GroupDef
import de.ugoe.cs.swe.tTCN3.Initial
import de.ugoe.cs.swe.tTCN3.ModuleControlPart
import de.ugoe.cs.swe.tTCN3.ModuleDefinition
import de.ugoe.cs.swe.tTCN3.ModuleDefinitionsList
import de.ugoe.cs.swe.tTCN3.ModulePar
import de.ugoe.cs.swe.tTCN3.ModuleParDef
import de.ugoe.cs.swe.tTCN3.ModuleParameter
import de.ugoe.cs.swe.tTCN3.PortDef
import de.ugoe.cs.swe.tTCN3.RecordDefNamed
import de.ugoe.cs.swe.tTCN3.RecordOfDefNamed
import de.ugoe.cs.swe.tTCN3.RefValue
import de.ugoe.cs.swe.tTCN3.ReferencedType
import de.ugoe.cs.swe.tTCN3.SetDefNamed
import de.ugoe.cs.swe.tTCN3.SetOfDefNamed
import de.ugoe.cs.swe.tTCN3.SignatureDef
import de.ugoe.cs.swe.tTCN3.SingleConstDef
import de.ugoe.cs.swe.tTCN3.SingleTempVarInstance
import de.ugoe.cs.swe.tTCN3.SingleVarInstance
import de.ugoe.cs.swe.tTCN3.StatementBlock
import de.ugoe.cs.swe.tTCN3.StructDefBody
import de.ugoe.cs.swe.tTCN3.StructFieldDef
import de.ugoe.cs.swe.tTCN3.SubTypeDefNamed
import de.ugoe.cs.swe.tTCN3.TTCN3Module
import de.ugoe.cs.swe.tTCN3.TTCN3Package
import de.ugoe.cs.swe.tTCN3.TemplateDef
import de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalPar
import de.ugoe.cs.swe.tTCN3.TemplateOrValueFormalParList
import de.ugoe.cs.swe.tTCN3.TestcaseDef
import de.ugoe.cs.swe.tTCN3.TypeDef
import de.ugoe.cs.swe.tTCN3.TypeDefBody
import de.ugoe.cs.swe.tTCN3.UnionDefNamed
import de.ugoe.cs.swe.tTCN3.VarInstance
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.xtext.validation.AbstractDeclarativeValidator
import org.eclipse.xtext.validation.Check
import org.eclipse.xtext.validation.EValidatorRegistrar

import static extension de.ugoe.cs.swe.common.TTCN3ScopeHelper.*

class NameClashValidator extends AbstractDeclarativeValidator {

	@Check
	def checkUniqueNameNestedVariable(RefValue variable) {
		variable.isNameClashInHierarchy
	}

	@Check
	def checkUniqueNameStructFieldDef(StructFieldDef field) {

		// find name clashes with module variables
		val container = field.findDesiredParent(typeof(ModuleDefinitionsList)) as ModuleDefinitionsList
		if (container.checkNameClashModuleDefinitionsList(field, field.name)) {
			error('Name clash with another variable definition in this scope!',
				TTCN3Package.eINSTANCE.TTCN3Reference_Name);
			return;
		}

		// find name clashes with variables in parent body
		val body = field.findDesiredParent(typeof(StructDefBody)) as StructDefBody
		for (StructFieldDef f : body.defs) {
			if (f != field && f.name.equals(field.name)) {
				error('Name clash with another variable definition in this scope!',
					TTCN3Package.eINSTANCE.TTCN3Reference_Name);
				return;
			}
		}
	}

	// TODO: adapt auto-completion (remove extended types)
	// TODO: treat cyclic dependencies??
	@Check
	def checkUniqueComponentExtension(ComponentDef component) {
		var ComponentDef last = null;
		var ComponentDef current = null;
		for (ComponentDef d : component.extends.sortBy[it.name]) {
			current = d;
			if (current == last) {
				error('The extension list contains the type ' + current.name + ' twice!',
					TTCN3Package.eINSTANCE.TTCN3Reference_Name);
				return;
			}
			last = current
		}
	}

	@Check
	def checkTypeNames(ReferencedType type) {
		if (type.checkNameClashesModuleDefs(type.name)) {
			error('Name clash with another variable definition in this scope!',
				TTCN3Package.eINSTANCE.TTCN3Reference_Name);
			return;
		}
	}

	@Check
	def checkTypeNames(TestcaseDef tc) {
		if (tc.checkNameClashesModuleDefs(tc.name)) {
			error('Name clash with another variable definition in this scope!',
				TTCN3Package.eINSTANCE.TTCN3Reference_Name);
			return;
		}
	}

	@Check
	def checkTypeNames(FunctionDef function) {
		if (function.checkNameClashesModuleDefs(function.name)) {
			error('Name clash with another variable definition in this scope!',
				TTCN3Package.eINSTANCE.TTCN3Reference_Name);
			return;
		}
	}

	@Check
	def checkTypeNames(SignatureDef sig) {
		if (sig.checkNameClashesModuleDefs(sig.name)) {
			error('Name clash with another variable definition in this scope!',
				TTCN3Package.eINSTANCE.TTCN3Reference_Name);
			return;
		}
	}

	@Check
	def checkTypeNames(AltstepDef alt) {
		if (alt.checkNameClashesModuleDefs(alt.name)) {
			error('Name clash with another variable definition in this scope!',
				TTCN3Package.eINSTANCE.TTCN3Reference_Name);
			return;
		}
	}

	@Check
	def checkTypeNames(GroupDef group) {
		if (group.checkNameClashesModuleDefs(group.name)) {
			error('Name clash with another variable definition in this scope!',
				TTCN3Package.eINSTANCE.TTCN3Reference_Name);
			return;
		}
	}

	def private isNameClashInHierarchy(RefValue variable) {
		var parent = variable.eContainer
		val EStructuralFeature feature = TTCN3Package.eINSTANCE.TTCN3Reference_Name;

		while (parent != null) {

			if (parent instanceof TTCN3Module) {
				if (variable.checkNameClashesModuleDefs(variable.name)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}
			} else if (parent instanceof StatementBlock) {
				if (parent.checkNameClasStatementBlock(variable)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}
			} else if (parent instanceof Initial) {
				if (parent.checkNameClashInitial(variable)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}
				if (variable.checkNameClashesModuleDefs(variable.name)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}
				return; // only check clashes with module def list
			} else if (parent instanceof ForStatement) {
				if (parent.init.checkNameClashInitial(variable)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}
			} else if (parent instanceof FunctionDef) {
				if (parent.parameterList != null && parent.parameterList.checkNameClashFunctionParameterValue(variable)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}
			} else if (parent instanceof FunctionFormalParList) {
				if (parent.checkNameClashFunctionParameterValue(variable)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}
			} else if (parent instanceof ModuleControlPart) {
//				if (parent.checkNameClashModuleControlPart(variable)) {
//					error('Name clash with another variable definition in this scope!', feature);
//					return;
//				}
			} else if (parent instanceof ComponentDef) {
				if (!parent.checkNameClashComponentDefHierarchy(variable)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}
			} else if (parent instanceof TestcaseDef) {
				if (parent.parList.checkNameClashTemplateOrValueFormalParList(variable)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}
				if (!parent.spec.runsOn.component.checkNameClashComponentDefHierarchy(variable)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}
			} else if (parent instanceof TemplateOrValueFormalParList) {
				if (parent.checkNameClashTemplateOrValueFormalParList(variable)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}
			} else if (parent instanceof AltstepDef) {
				if (parent.params != null && parent.params.checkNameClashFunctionParameterValue(variable)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}
			} else if (parent instanceof AltstepLocalDefList) {
				if (parent.checkNameClashAltstepLocalDefList(variable)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}
			} else if (parent instanceof GroupDef) {
				if (parent.list.checkNameClashModuleDefinitionsList(variable, variable.name)) {
					error('Name clash with another variable definition in this scope!', feature);
					return;
				}

				// Only check the current group definition list if this value is a inner part of a nested group definition
				val boolean isNestedGroup = parent.findDesiredParent(typeof(GroupDef)) as GroupDef != null
				if (isNestedGroup) {
					return;
				}
			}

			parent = parent.eContainer
		}
	}

	def private boolean checkNameClashModuleDefinitionsList(ModuleDefinitionsList list, EObject variable,
		String variableName) {
		for (ModuleDefinition v : list.definitions) {

			if (v.def instanceof ConstDef) {
				val constDefList = (v.def as ConstDef).defs as ConstList
				for (SingleConstDef d : constDefList.list) {
					if (d != variable && d.name == variableName) {
						return true
					}
				}
			}

			if (v.def instanceof TypeDef) {
				val body = (v.def as TypeDef).body
				if (body.checkTypeNameClash(variable, variableName))
					return true;
			}

			if (v.def instanceof ModuleParDef) {
				if ((v.def as ModuleParDef).param != null) {
					for (ModuleParameter p : (v.def as ModuleParDef).param.list.params) {
						if (p != variable && p.name == variableName)
							return true
					}
				} else if ((v.def as ModuleParDef).multitypeParam != null) {
					for (ModulePar p : (v.def as ModuleParDef).multitypeParam.params) {
						for (ModuleParameter pa : p.list.params) {
							if (pa != variable && pa.name == variableName)
								return true
						}
					}
				}
			}

			if (v.def instanceof FunctionDef) {
				if (v.def != variable && (v.def as FunctionDef).name == variableName)
					return true
			}

			if (v.def instanceof TemplateDef) {
				if ((v.def as TemplateDef).base != variable && (v.def as TemplateDef).base.name == variableName)
					return true
			}

			if (v.def instanceof SignatureDef) {
				if (v.def != variable && (v.def as SignatureDef).name == variableName)
					return true
			}

			if (v.def instanceof TestcaseDef) {
				if (v.def != variable && (v.def as TestcaseDef).name == variableName)
					return true
			}

			if (v.def instanceof AltstepDef) {
				if (v.def != variable && (v.def as AltstepDef).name == variableName)
					return true
			}

			if (v.def instanceof GroupDef) {
				if (v.def != variable && (v.def as GroupDef).name == variableName)
					return true
			}

		}
		return false;
	}

	def private boolean checkTypeNameClash(TypeDefBody body, EObject variable, String variableName) {
		if (body.structured != null) {
			if (body.structured.recordOf != null && body.structured.recordOf != variable &&
				body.structured.recordOf instanceof RecordOfDefNamed &&
				(body.structured.recordOf as RecordOfDefNamed).name == variableName)
				return true
			if (body.structured.setOf != null && body.structured.setOf != variable &&
				body.structured.setOf instanceof SetOfDefNamed &&
				(body.structured.setOf as SetOfDefNamed).name == variableName)
				return true
			if (body.structured.record != null && body.structured.record != variable &&
				body.structured.record instanceof RecordDefNamed &&
				(body.structured.record as RecordDefNamed).name == variableName)
				return true
			if (body.structured.union != null && body.structured.union != variable &&
				body.structured.union instanceof UnionDefNamed &&
				(body.structured.union as UnionDefNamed).name == variableName)
				return true
			if (body.structured.set != null && body.structured.set != variable &&
				body.structured.set instanceof SetDefNamed &&
				(body.structured.set as SetDefNamed).name == variableName)
				return true
			if (body.structured.enumDef != null && body.structured.enumDef instanceof EnumDefNamed) {
				if (body.structured.enumDef != variable &&
					(body.structured.enumDef as EnumDefNamed).name == variableName) {
					return true
				}
				if ((body.structured.enumDef as EnumDefNamed).list.checkEnumNameClash(variable, variableName)) {
					return true
				}
			}
			if (body.structured.enumDef != null && body.structured.enumDef instanceof EnumDef) {
				if ((body.structured.enumDef as EnumDef).list.checkEnumNameClash(variable, variableName)) {
					return true
				}
			}
			if (body.structured.port != null && body.structured.port != variable &&
				body.structured.port instanceof PortDef && (body.structured.port as PortDef).name == variableName)
				return true
			if (body.structured.component != null && body.structured.component != variable &&
				body.structured.component instanceof ComponentDef &&
				(body.structured.component as ComponentDef).name == variableName)
				return true
		} else if (body.sub != null) {
			if (body.sub != variable && body.sub instanceof SubTypeDefNamed &&
				(body.sub as SubTypeDefNamed).name == variableName)
				return true
		}
		return false
	}

	def private boolean checkEnumNameClash(EnumerationList list, EObject variable, String variableName) {
		for (Enumeration e : list.enums) {
			if (variable != e && variableName == e.name) {
				return true;
			}
		}
		return false;
	}

	def private boolean checkNameClasStatementBlock(StatementBlock block, RefValue variable) {
		for (FunctionDefList l : block.def.filter[it != null]) {

			if (l.locDef != null && l.locDef.constDef != null) {
				val constDefList = l.locDef.constDef.defs as ConstList
				for (SingleConstDef d : constDefList.list) {
					if (d != variable && d.name == variable.name) {
						return true;
					}
				}
			}
			if (l.locInst != null && l.locInst.variable != null) {
				if (l.locInst.variable.checkNameClashVarInstance(variable))
					return true;
			}
		}
		return false;
	}

	def private boolean checkNameClashVarInstance(VarInstance instance, RefValue variable) {
		if (instance.list != null) {
			for (SingleVarInstance d : instance.list.variables) {
				if (d != variable && d.name == variable.name) {
					return true;
				}
			}
		} else if (instance.tempList != null) {
			for (SingleTempVarInstance d : instance.tempList.variables) {
				if (d != variable && d.name == variable.name) {
					return true;
				}
			}
		}
		return false
	}

	def private boolean checkNameClashInitial(Initial init, RefValue variable) {
		if (init.variable != null && init.variable.checkNameClashVarInstance(variable)) {
			return true;
		}
		return false;
	}

	def private boolean checkNameClashFunctionParameterValue(FunctionFormalParList list, RefValue variable) {
		for (FunctionFormalPar p : list.params.filter[it.value != null]) {
			if (p.value != variable && p.value.name == variable.name)
				return true;
		}
		return false;
	}

//	def private boolean checkNameClashModuleControlPart(ModuleControlPart control, RefValue variable) {
//		for (ControlStatementOrDefList o : control.body.list) {
//			if (o.def.localDef != null && o.def.localDef.constDef != null) {
//				val constDefList = o.def.localDef.constDef.defs as ConstList
//				for (SingleConstDef d : constDefList.list) {
//					if (d != variable && d.name == variable.name) {
//						return true;
//					}
//				}
//			}
//			if (o.def.localInst != null && o.def.localInst.variable != null) {
//				if (o.def.localInst != null && o.def.localInst.variable != null) {
//					if (o.def.localInst.variable.checkNameClashVarInstance(variable))
//						return true;
//				}
//			}
//		}
//		return false;
//	}

	// this method returns true if no name clash occurs and false otherwise
	// the behavior is required by the calling method checkNameClashComponentDefHierarchy
	def private boolean checkNameClashComponentDef(ComponentDef component, RefValue variable) {
		for (ComponentDefList l : component.defs) {
			if (l.element.const != null) {
				val constDefList = l.element.const.defs as ConstList
				for (SingleConstDef d : constDefList.list) {
					if (d != variable && d.name == variable.name) {
						return false;
					}
				}
			}
			if (l.element.variable != null) {
				if (l.element.variable.checkNameClashVarInstance(variable))
					return false;
			}
		}
		return true;
	}

	def private boolean checkNameClashComponentDefHierarchy(ComponentDef component, RefValue variable) {
		var boolean res = component.checkNameClashComponentDef(variable)

		for (ComponentDef d : component.extends)
			res = res && d.checkNameClashComponentDefHierarchy(variable)

		return res;
	}

	def private boolean checkNameClashTemplateOrValueFormalParList(TemplateOrValueFormalParList list, RefValue variable) {
		var RefValue value;

		for (TemplateOrValueFormalPar p : list.params) {
			if (p.value != null)
				value = p.value as RefValue
			else if (p.template != null)
				value = p.template as RefValue;

			if (value != null && value != variable && value.name == variable.name)
				return true;
		}
		return false;
	}

	def private boolean checkNameClashAltstepLocalDefList(AltstepLocalDefList list, RefValue variable) {
		for (AltstepLocalDef a : list.defs.filter[it != null]) {
			if (a.const != null) {
				val constDefList = a.const.defs as ConstList
				for (SingleConstDef d : constDefList.list) {
					if (d != variable && d.name == variable.name) {
						return true;
					}
				}
			}
			if (a.variable != null) {
				if (a.variable.checkNameClashVarInstance(variable))
					return true;
			}
		}
		return false;
	}

	def private boolean checkNameClashesModuleDefs(EObject variable, String variableName) {
		val module = variable.findDesiredParent(typeof(TTCN3Module)) as TTCN3Module
		val group = variable.findDesiredParent(typeof(GroupDef)) as GroupDef

		if (group != null) {
			if (group.list.checkNameClashModuleDefinitionsList(variable, variableName)) {
				return true;
			}
			if (group.name == variableName) {
				return true
			}
		} else if (module != null) {
			if (module.defs != null && module.defs.checkNameClashModuleDefinitionsList(variable, variableName)) {
				return true;
			}
			if (module.name == variableName) {
				return true
			}
		}
		return false;
	}

	override register(EValidatorRegistrar registrar) {
		//not needed for classes used as ComposedCheck
	}
}
