package de.ugoe.cs.swe.validation

import com.google.common.base.Strings
import de.ugoe.cs.swe.TTCN3Configuration.QualityCheckProfile
import de.ugoe.cs.swe.common.ConfigTools
import de.ugoe.cs.swe.common.MiscTools
import de.ugoe.cs.swe.common.logging.LoggingInterface.LogLevel
import de.ugoe.cs.swe.common.logging.LoggingInterface.MessageClass
import de.ugoe.cs.swe.tTCN3.AltstepDef
import de.ugoe.cs.swe.tTCN3.BaseTemplate
import de.ugoe.cs.swe.tTCN3.ComponentDef
import de.ugoe.cs.swe.tTCN3.ComponentElementDef
import de.ugoe.cs.swe.tTCN3.ConstDef
import de.ugoe.cs.swe.tTCN3.EnumDefNamed
import de.ugoe.cs.swe.tTCN3.Enumeration
import de.ugoe.cs.swe.tTCN3.ExtConstDef
import de.ugoe.cs.swe.tTCN3.ExtFunctionDef
import de.ugoe.cs.swe.tTCN3.FormalPortPar
import de.ugoe.cs.swe.tTCN3.FormalTemplatePar
import de.ugoe.cs.swe.tTCN3.FormalTimerPar
import de.ugoe.cs.swe.tTCN3.FormalValuePar
import de.ugoe.cs.swe.tTCN3.FunctionDef
import de.ugoe.cs.swe.tTCN3.GroupDef
import de.ugoe.cs.swe.tTCN3.MatchingSymbol
import de.ugoe.cs.swe.tTCN3.ModuleDefinition
import de.ugoe.cs.swe.tTCN3.ModuleParameter
import de.ugoe.cs.swe.tTCN3.PortDef
import de.ugoe.cs.swe.tTCN3.PortElement
import de.ugoe.cs.swe.tTCN3.RecordDefNamed
import de.ugoe.cs.swe.tTCN3.RecordOfDefNamed
import de.ugoe.cs.swe.tTCN3.SetDefNamed
import de.ugoe.cs.swe.tTCN3.SetOfDefNamed
import de.ugoe.cs.swe.tTCN3.SignatureDef
import de.ugoe.cs.swe.tTCN3.SingleConstDef
import de.ugoe.cs.swe.tTCN3.SingleTempVarInstance
import de.ugoe.cs.swe.tTCN3.SingleVarInstance
import de.ugoe.cs.swe.tTCN3.SubTypeDefNamed
import de.ugoe.cs.swe.tTCN3.TTCN3Module
import de.ugoe.cs.swe.tTCN3.TTCN3Package
import de.ugoe.cs.swe.tTCN3.TTCN3Reference
import de.ugoe.cs.swe.tTCN3.TemplateDef
import de.ugoe.cs.swe.tTCN3.TestcaseDef
import de.ugoe.cs.swe.tTCN3.TimerInstance
import de.ugoe.cs.swe.tTCN3.UnionDefNamed
import java.util.regex.Matcher
import java.util.regex.Pattern
import org.eclipse.xtext.nodemodel.INode
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.validation.AbstractDeclarativeValidator
import org.eclipse.xtext.validation.Check
import org.eclipse.xtext.validation.EValidatorRegistrar

import static extension de.ugoe.cs.swe.common.TTCN3ScopeHelper.*
import static extension org.eclipse.xtext.EcoreUtil2.*

public class NamingConventionsValidator extends AbstractDeclarativeValidator {
	val ConfigTools configTools = ConfigTools.getInstance;
	var QualityCheckProfile activeProfile = configTools.selectedProfile as QualityCheckProfile

	@Check
	def checkModuleName(TTCN3Module module) {
		val regExp = activeProfile.namingConventionsConfig.moduleRegExp;
		module.checkIdentifierForNamingConventionCompliance(regExp, "Module", true)
	}

	@Check
	def checkFunctionName(FunctionDef function) {
		val regExp = activeProfile.namingConventionsConfig.functionRegExp;
		function.checkIdentifierForNamingConventionCompliance(regExp, "Function", false)
	}

	@Check
	def checkExtFunctionName(ExtFunctionDef function) {
		val regExp = activeProfile.namingConventionsConfig.extFunctionRegExp;
		function.checkIdentifierForNamingConventionCompliance(regExp, "ExtFunction", false)
	}

	@Check
	def checkConstName(SingleConstDef const) {
		var message = ""
		var regExp = ""
		val ConstDef cDef = const.findDesiredParent(ConstDef)
		if (cDef.eContainer instanceof ModuleDefinition) {
			message = "Constant"
			regExp = activeProfile.namingConventionsConfig.constantRegExp
		} else {
			message = "Local Constant"
			regExp = activeProfile.namingConventionsConfig.localConstantRegExp
		}
		const.checkIdentifierForNamingConventionCompliance(regExp, message, false)
	}

	 @Check
	 def checkExtConstantnName(ExtConstDef const) {
	 	val regExp = activeProfile.namingConventionsConfig.extConstantRegExp;
	 	val INode node = NodeModelUtils.getNode(const)
	 	for (n : const.id.ids) {
	 		n.name.checkIdentifierForNamingConventionCompliance(node, regExp, "External Constant")
	 	}	 	
	 }
		
	@Check
	def checkAltstepName(AltstepDef altstep) {
		val regExp = activeProfile.namingConventionsConfig.altstepRegExp;
		altstep.checkIdentifierForNamingConventionCompliance(regExp, "Altstep", false)
	}

	@Check
	def checkGroupName(GroupDef group) {
		val regExp = activeProfile.namingConventionsConfig.groupRegExp;
		group.checkIdentifierForNamingConventionCompliance(regExp, "Group", false)
	}

	@Check
	def checkModuleParameterName(ModuleParameter moduleParameter) {
		val regExp = activeProfile.namingConventionsConfig.moduleParameterRegExp;
		moduleParameter.checkIdentifierForNamingConventionCompliance(regExp, "ModuleParameter", false)
	}

	@Check
	def checkTestcaseName(TestcaseDef testcase) {
		val regExp = activeProfile.namingConventionsConfig.testcaseRegExp;
		testcase.checkIdentifierForNamingConventionCompliance(regExp, "Testcase", false)
	}

	@Check
	def checkTestcaseName(SignatureDef signature) {
		val regExp = activeProfile.namingConventionsConfig.signatureTemplateRegExp;
		signature.checkIdentifierForNamingConventionCompliance(regExp, "SignatureTemplate", false)
	}

	@Check
	def checkTestcaseName(FormalValuePar parameter) {
		val regExp = activeProfile.namingConventionsConfig.formalParameterRegExp;
		parameter.checkIdentifierForNamingConventionCompliance(regExp, "FormalParameter", false)
	}

	@Check
	def checkTestcaseName(FormalPortPar parameter) {
		val regExp = activeProfile.namingConventionsConfig.formalParameterRegExp;
		parameter.checkIdentifierForNamingConventionCompliance(regExp, "FormalParameter", false)
	}

	@Check
	def checkTestcaseName(FormalTemplatePar parameter) {
		val regExp = activeProfile.namingConventionsConfig.formalParameterRegExp;
		parameter.checkIdentifierForNamingConventionCompliance(regExp, "FormalParameter", false)
	}

	@Check
	def checkTestcaseName(FormalTimerPar parameter) {
		val regExp = activeProfile.namingConventionsConfig.formalParameterRegExp;
		parameter.checkIdentifierForNamingConventionCompliance(regExp, "FormalParameter", false)
	}

	@Check
	def checkEnumeratedValueName(Enumeration enumeratedValue) {
		val regExp = activeProfile.namingConventionsConfig.enumeratedValueRegExp;
		enumeratedValue.checkIdentifierForNamingConventionCompliance(regExp, "EnumeratedValue", false)
	}

	@Check
	def checkSubTypeValueName(SubTypeDefNamed dataType) {
		val regExp = activeProfile.namingConventionsConfig.dataTypeRegExp;
		dataType.checkIdentifierForNamingConventionCompliance(regExp, "DataType", false)
	}

	@Check
	def checkRecordOfTypeName(RecordOfDefNamed dataType) {
		val regExp = activeProfile.namingConventionsConfig.dataTypeRegExp;
		dataType.checkIdentifierForNamingConventionCompliance(regExp, "DataType", false)
	}

	@Check
	def checkSetOfTypeName(SetOfDefNamed dataType) {
		val regExp = activeProfile.namingConventionsConfig.dataTypeRegExp;
		dataType.checkIdentifierForNamingConventionCompliance(regExp, "DataType", false)
	}

	@Check
	def checkSetOfTypeName(SetDefNamed dataType) {
		val regExp = activeProfile.namingConventionsConfig.dataTypeRegExp;
		dataType.checkIdentifierForNamingConventionCompliance(regExp, "DataType", false)
	}

	@Check
	def checkSetOfTypeName(RecordDefNamed dataType) {
		val regExp = activeProfile.namingConventionsConfig.dataTypeRegExp;
		dataType.checkIdentifierForNamingConventionCompliance(regExp, "DataType", false)
	}

	@Check
	def checkSetOfTypeName(ComponentDef dataType) {
		val regExp = activeProfile.namingConventionsConfig.dataTypeRegExp;
		dataType.checkIdentifierForNamingConventionCompliance(regExp, "DataType", false)
	}

	@Check
	def checkSetOfTypeName(UnionDefNamed dataType) {
		val regExp = activeProfile.namingConventionsConfig.dataTypeRegExp;
		dataType.checkIdentifierForNamingConventionCompliance(regExp, "DataType", false)
	}

	@Check
	def checkSetOfTypeName(PortDef dataType) {
		val regExp = activeProfile.namingConventionsConfig.dataTypeRegExp;
		dataType.checkIdentifierForNamingConventionCompliance(regExp, "DataType", false)
	}
	
	@Check
	def checkSetOfTypeName(EnumDefNamed dataType) {
		val regExp = activeProfile.namingConventionsConfig.dataTypeRegExp;
		dataType.checkIdentifierForNamingConventionCompliance(regExp, "DataType", false)
	}	
	
	@Check
	def checkSetOfTypeName(PortElement port) {
		val regExp = activeProfile.namingConventionsConfig.portInstanceRegExp;
		port.checkIdentifierForNamingConventionCompliance(regExp, "PortInstance", false)
	}	
	
	@Check
	def checkVariableName(SingleVarInstance variable) {
		var regExp = ""
		var message = ""
		val ComponentElementDef componend = variable.findDesiredParent(ComponentElementDef)
		val type = variable.findVariableType

		if (componend != null) {
			regExp = activeProfile.namingConventionsConfig.componentVariableRegExp
			message = "ComponentVariable"
		} else if (type != null) {
			val refType = type.getReferencedType
			if (refType instanceof ComponentDef) {
				regExp = activeProfile.namingConventionsConfig.componentInstanceRegExp
				message = "ComponentInstance"
			} else {
				/*
				if (activeProfile.checkNamingConventions) {			
					if (refType.name == null) {
						TTCN3StatisticsProvider.getInstance.increaseCountNaming
						val INode node = NodeModelUtils.getNode(variable)
						val INode nodeType = NodeModelUtils.getNode(type)
						val infoMessage = "The type declaration for \"" + nodeType.text.trim + "\" could not be resolved. It cannot be determined whether the variable instances of this type are component instances. They will be treated as variable instances."
						info(
							infoMessage,
							TTCN3Package.eINSTANCE.TTCN3Reference_Name,
							MessageClass.NAMING.toString,
							node.startLine.toString,
							node.endLine.toString,
							"2.1, " + "checkVariable",
							LogLevel.INFORMATION.toString
						);	
					}
				}	
				*/		
				regExp = activeProfile.namingConventionsConfig.variableRegExp
				message = "Variable"
			}		
		} else {
			regExp = activeProfile.namingConventionsConfig.variableRegExp
			message = "Variable"
		}
		variable.checkIdentifierForNamingConventionCompliance(regExp, message, false)
	}
	
	@Check
	def checkTimerInstances(TimerInstance timer) {
		val componend = timer.findDesiredParent(ComponentDef)
		var regExp = ""
		var message = ""
				
		for (v : timer.list.variables) {
			if (componend != null) {
				regExp = activeProfile.namingConventionsConfig.componentTimerRegExp
				message = "ComponentTimer"
			} else {
				regExp = activeProfile.namingConventionsConfig.timerRegExp
				message = "Timer"
			}	
			v.checkIdentifierForNamingConventionCompliance(regExp, message, false)
		}
	}
	
	@Check
	def checkVariableName(SingleTempVarInstance variable) {
		var regExp = activeProfile.namingConventionsConfig.variableRegExp
		var message = "Variable"
		variable.checkIdentifierForNamingConventionCompliance(regExp, message, false)
	}

	@Check
	def checkSTF160Template(BaseTemplate template) {
		var regExp = ""
		var message = ""
		val templateDef = template.findDesiredParent(TemplateDef)
		val isDerived = templateDef.derived != null
		var isSendTemplate = false;
		var isAmbiguous = false;

		if (!activeProfile.checkNamingConventions)
			return;

		// check if it is not a present restriction, but a restriction
		if (templateDef.restriction != null && templateDef.restriction.present == null) {
			if (template.parList != null && template.parList.params != null) {
				for (p : template.parList.params) {
					if (p.template != null) {
						if (p.template.restriction == null || p.template.restriction.restriction == null ||
							p.template.restriction.restriction.present != null) {
							isAmbiguous = true // problem occurred, inconsistent definition
						}
					}
					if (isAmbiguous) {
						TTCN3StatisticsProvider.getInstance.increaseCountNaming
						val INode node = NodeModelUtils.getNode(templateDef)
						val infoMessage = "The template definition for \"" + template.name +
							"\" is ambiguous. It cannot be determined whether it is a send or a receive template according to STF160's conventions. It will not be analyzed further for naming conventions compliance."
						info(
							infoMessage,
							TTCN3Package.eINSTANCE.TTCN3Reference_Name,
							MessageClass.NAMING.toString,
							node.startLine.toString,
							node.endLine.toString,
							"2.1, " + MiscTools.getMethodName(),
							LogLevel.INFORMATION.toString
						);
						return;
					}
				}
			}
			// if no problems occurred this far check name for send template
			isSendTemplate = true;
		} // (else) check name for receive template
		if (!isDerived) {
			if (isSendTemplate) {
				message = "STF160SendTemplate";
				regExp = activeProfile.namingConventionsConfig.stf160sendTemplateRegExp
			} else {
				message = "STF160ReceiveTemplate";
				regExp = activeProfile.namingConventionsConfig.stf160receiveTemplateRegExp
			}
		} else {
			if (isSendTemplate) {
				message = "DerivedSTF160SendTemplate";
				regExp = activeProfile.namingConventionsConfig.derivedStf160sendTemplateRegExp
			} else {
				message = "DerivedSTF160ReceiveTemplate";
				regExp = activeProfile.namingConventionsConfig.derivedStf160receiveTemplateRegExp
			}
		}

		// check STF160 template conventions
		template.checkIdentifierForNamingConventionCompliance(regExp, message, true)
	}
	
	@Check
	def checkMessageTemplate(BaseTemplate template) {
		var regExp = ""
		var message = ""
		val templateDef = template.findDesiredParent(TemplateDef)
		val isDerived = templateDef.derived != null
		val matchingSymbols = templateDef.body.eAllOfType(MatchingSymbol)
		matchingSymbols.addAll(templateDef.base.eAllOfType(MatchingSymbol)) // also consider parameter list

		if (!isDerived) {
			if (matchingSymbols.empty) {
				regExp = activeProfile.namingConventionsConfig.messageTemplateRegExp
				message = "MessageTemplate";
			} else {
				regExp = activeProfile.namingConventionsConfig.messageTemplateWithWildcardsRegExp
				message = "MessageTemplateWithMatchingExpression";
			}
		} else {
			if (matchingSymbols.empty) {
				regExp = activeProfile.namingConventionsConfig.derivedMessageTemplateRegExp
				message = "DerivedMessageTemplate";
			} else {
				regExp = activeProfile.namingConventionsConfig.derivedMessageTemplateWithWildcardsRegExp
				message = "DerivedMessageTemplateWithMatchingExpression";
			}
		}

		// check message template conventions
		template.checkIdentifierForNamingConventionCompliance(regExp, message, true)
	}		
	
	def private boolean regExpMatch(String regExp, String subject) {
		val pattern = Pattern.compile(regExp);
		val Matcher matcher = pattern.matcher(subject);
		return matcher.matches
	}
	
	def private checkIdentifierForNamingConventionCompliance(String name, INode node, String regExp, String type) {
		if (Strings.isNullOrEmpty(regExp))
			return;

		if (!activeProfile.checkNamingConventions)
			return;

		if (!regExp.regExpMatch(name)) {
			TTCN3StatisticsProvider.getInstance.increaseCountNaming
			val message = "\"" + name + "\" does not comply to the naming conventions for \"" + type + "\"!"
			warning(
				message,
				null,
				MessageClass.NAMING.toString,
				node.startLine.toString,
				node.endLine.toString,
				"2.1, " + regExp
			);
			return;
		}
	}	
	
	def private checkIdentifierForNamingConventionCompliance(TTCN3Reference object, String regExp, String type, boolean showOnlyStartLine) {
		if (object == null || Strings.isNullOrEmpty(regExp))
			return;

		if (!activeProfile.checkNamingConventions)
			return;

		val INode node = NodeModelUtils.getNode(object)

		if (!regExp.regExpMatch(object.name)) {
			TTCN3StatisticsProvider.getInstance.increaseCountNaming
			val message = "\"" + object.name + "\" does not comply to the naming conventions for \"" + type + "\"!"
			var endLine = ""
			if (showOnlyStartLine) {
				endLine = node.startLine.toString
			} else {
				endLine = node.endLine.toString
			}
			warning(
				message,
				null,
				MessageClass.NAMING.toString,
				node.startLine.toString,
				endLine,
				"2.1, " + regExp
			);
			return;
		}
	}

	override register(EValidatorRegistrar registrar) {
		// not needed for classes used as ComposedCheck
	}

}
