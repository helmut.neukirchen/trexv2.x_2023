package de.ugoe.cs.swe.validation;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class TTCN3StatisticsProvider {
	private AtomicInteger parsedLines = new AtomicInteger(0);
	private AtomicLong parseTime = new AtomicLong(0);
	private AtomicLong analyzeTime = new AtomicLong(0);
	private AtomicInteger countUniversal = new AtomicInteger(0);
	private AtomicInteger countGeneral = new AtomicInteger(0);
	private AtomicInteger countNaming = new AtomicInteger(0);
	private AtomicInteger countDocumentation = new AtomicInteger(0);
	private AtomicInteger countLog = new AtomicInteger(0);
	private AtomicInteger countStructure = new AtomicInteger(0);
	private AtomicInteger countStyle = new AtomicInteger(0);
	private AtomicInteger countModularization = new AtomicInteger(0);
	private AtomicBoolean parsingCompleted = new AtomicBoolean(false);
	private AtomicBoolean preAnalyzingCompleted = new AtomicBoolean(false);

	public int getCountNaming() {
		return countNaming.get();
	}

	public void increaseCountNaming() {
		this.countNaming.incrementAndGet();
	}

	public int getCountUniversal() {
		return countUniversal.get();
	}

	public void incrementCountUniversal() {
		this.countUniversal.incrementAndGet();
	}

	public int getCountGeneral() {
		return countGeneral.get();
	}

	public void incrementCountGeneral() {
		this.countGeneral.incrementAndGet();
	}

	public int getCountDocumentation() {
		return countDocumentation.get();
	}

	public void incrementCountDocumentation() {
		this.countDocumentation.incrementAndGet();
	}

	public int getCountLog() {
		return countLog.get();
	}

	public void incrementCountLog() {
		this.countLog.incrementAndGet();
	}

	public int getCountStructure() {
		return countStructure.get();
	}

	public void incrementCountStructure() {
		this.countStructure.incrementAndGet();
	}

	public int getCountStyle() {
		return countStyle.get();
	}

	public void incrementCountStyle() {
		this.countStyle.incrementAndGet();
	}

	public int getCountModularization() {
		return countModularization.get();
	}

	public void incrementCountModularization() {
		this.countModularization.incrementAndGet();
	}

	public int getParsedLines() {
		return parsedLines.get();
	}

	private static TTCN3StatisticsProvider instance = new TTCN3StatisticsProvider();

	private TTCN3StatisticsProvider() {
	}

	public static TTCN3StatisticsProvider getInstance() {
		return instance;
	}

	public void addParsedLines(int lines) {
		parsedLines.addAndGet(lines);
	}

	public long getParseTime() {
		return parseTime.get();
	}

	public void addParseTime(long pareTime) {
		this.parseTime.addAndGet(pareTime);
	}

	public long getAnalyzeTime() {
		return analyzeTime.get();
	}

	public void addAnalyzeTime(long analyzeTime) {
		this.analyzeTime.addAndGet(analyzeTime);
	}

	public boolean isParsingCompleted() {
		return parsingCompleted.get();
	}

	public void setParsingCompleted(boolean parsingCompleted) {
		this.parsingCompleted.getAndSet(parsingCompleted);
	}
	
	public boolean isPreAnalyzingCompleted() {
		return preAnalyzingCompleted.get();
	}

	public void setPreAnalyzingCompleted(boolean preAnalyzingCompleted) {
		this.preAnalyzingCompleted.getAndSet(preAnalyzingCompleted);
	}	
}
