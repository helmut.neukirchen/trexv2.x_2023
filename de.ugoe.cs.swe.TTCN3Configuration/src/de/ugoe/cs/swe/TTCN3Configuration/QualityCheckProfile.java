package de.ugoe.cs.swe.TTCN3Configuration;

import de.ugoe.cs.swe.common.ConfigurationProfile;

public class QualityCheckProfile extends ConfigurationProfile {
	private boolean featureListImportedModuleNames = false;
	private boolean featureListImportedModuleFileNames = false;
	private boolean featureListImportingModuleNames = false;
	private boolean featureListImportingModuleFileNames = false;

	private boolean checkLogItemFormat = false;
	private boolean checkLogStatementFormat = true;
	private boolean processSubsequentLogStatementsAsOne = true;
	private String logFormatRegExp = "[\\*]{3}\\s([fta]_[a-zA-Z0-9]+?):\\s(INFO|WARNING|ERROR|PASS|FAIL|INCONC|TIMEOUT):\\s.*?[\\*]{3}";

	private boolean checkExternalFunctionInvocationPrecededByLogStatement = true;
	private boolean checkInconcOrFailSetVerdictPrecededByLog = true;
	private boolean checkNoLabelsOrGotoStatements = true;
	private boolean checkNoNestedAltStatements = true;
	private int maximumAllowedNestingDepth = 0;
	private boolean checkNoPermutationKeyword = true;
	private boolean checkNoAnyTypeKeyword = true;
	private boolean checkNoModifiedTemplateOfModifiedTemplate = true;

	private boolean checkNoAllKeywordInPortDefinitions = true;
	private boolean checkImportsComeFirst = true;
	private boolean checkLocalDefinitionsComeFirst = true;
	private String[] localDefinitionTypes = {"VarInstance", "ConstDef", "TimerInstance", "PortInstance"};
	private boolean checkTypeDefOrderInGroup = true;
	private boolean checkPortMessageGrouping = true;

	private boolean checkNoDuplicatedModuleDefinitionIdentifiers = true;
	private boolean checkZeroReferencedModuleDefinitions = true;
	private String zeroReferencedModuleDefinitionsExcludedRegExp = "";
	private boolean checkNoInlineTemplates = true;
	private boolean checkNoOverSpecificRunsOnClauses = true;
	boolean recursionInCheckNoOverSpecificRunsOnClauses = true;
	private boolean aliasInCheckNoOverSpecificRunsOnClauses = true;
	private boolean checkNoUnusedImports = true;
	private boolean checkNoUnusedFormalParameters = true;
	private boolean checkNoUnusedLocalDefinitions = true;
	private boolean checkNoUninitialisedVariables = true;
	private String[] checkNoUninitialisedVariablesExclude = {"enumerated", "union", "record", "record of", "set", "set of"};
	private boolean checkNoLiterals = true;
	private boolean checkLevelOfNestedCalls = true;
	private int maxLevelOfNestedCalls = 4;
	private boolean checkInlineTemplates = false;
	private boolean checkListedVariableDeclarations = true;
	
	private boolean checkTypesAndValuesModuleContainmentCheck = true;
	private boolean checkTemplatesModuleContainmentCheck = true;
	private boolean checkFunctionsModuleContainmentCheck = true;
	private boolean checkFunctionsModuleContainmentCheckAllowExtFunction = true;
	private boolean checkTestcasesModuleContainmentCheck = true;
	private boolean checkModuleParamsModuleContainmentCheck = true;
	private boolean checkInterfaceModuleContainmentCheck = true;
	private boolean checkTestSystemModuleContainmentCheck = true;
	private boolean checkTestControlModuleContainmentCheck = true;

	private boolean checkTypesAndValuesModuleImportsLibNames = true;
	private String typesAndValuesImportsLibNamesRegExp = ".*?LibCommon.*";
	private String typesAndValuesImportsLibNamesExcludedRegExp = "(.*?LibCommon.*)";
	private boolean checkTestcasesModuleImportsLibCommon_Sync = true;

	private boolean checkModuleSize = true;
	private int maximumAllowedModuleSizeInBytes = 10000;

	private boolean checkNoTabs = true;
	
	private boolean checkNamingConventions = true;
	private NamingConventionsConfig namingConventionsConfig = new NamingConventionsConfig();

//	private boolean featureCodeFormatting = false;
	private String pathFormattedOutputPath = "FORMATTED";
	private String dependencyOutputPath = "DOCUMENTATION";
//	private TTCN3FormatterParameters formattingParameters = new TTCN3FormatterParameters();

	public String getLogFormatRegExp() {
		return logFormatRegExp;
	}

	public void setLogFormatRegExp(String logFormatRegExp) {
		this.logFormatRegExp = logFormatRegExp;
	}

	public boolean isCheckExternalFunctionInvocationPrecededByLogStatement() {
		return checkExternalFunctionInvocationPrecededByLogStatement;
	}

	public void setCheckExternalFunctionInvocationPrecededByLogStatement(
			boolean checkExternalFunctionInvocationPrecededByLogStatement) {
		this.checkExternalFunctionInvocationPrecededByLogStatement = checkExternalFunctionInvocationPrecededByLogStatement;
	}

	public boolean isCheckInconcOrFailSetVerdictPrecededByLog() {
		return checkInconcOrFailSetVerdictPrecededByLog;
	}

	public void setCheckInconcOrFailSetVerdictPrecededByLog(
			boolean checkInconcOrFailSetVerdictPrecededByLog) {
		this.checkInconcOrFailSetVerdictPrecededByLog = checkInconcOrFailSetVerdictPrecededByLog;
	}

	public boolean isCheckNoLabelsOrGotoStatements() {
		return checkNoLabelsOrGotoStatements;
	}

	public void setCheckNoLabelsOrGotoStatements(
			boolean checkNoLabelsOrGotoStatements) {
		this.checkNoLabelsOrGotoStatements = checkNoLabelsOrGotoStatements;
	}

	public boolean isCheckNoNestedAltStatements() {
		return checkNoNestedAltStatements;
	}

	public void setCheckNoNestedAltStatements(boolean checkNoNestedAltStatements) {
		this.checkNoNestedAltStatements = checkNoNestedAltStatements;
	}

	public boolean isCheckNoPermutationKeyword() {
		return checkNoPermutationKeyword;
	}

	public void setCheckNoPermutationKeyword(boolean checkNoPermutationKeyword) {
		this.checkNoPermutationKeyword = checkNoPermutationKeyword;
	}

	public boolean isCheckNoAnyTypeKeyword() {
		return checkNoAnyTypeKeyword;
	}

	public void setCheckNoAnyTypeKeyword(boolean checkNoAnyTypeKeyword) {
		this.checkNoAnyTypeKeyword = checkNoAnyTypeKeyword;
	}

	public boolean isCheckNoModifiedTemplateOfModifiedTemplate() {
		return checkNoModifiedTemplateOfModifiedTemplate;
	}

	public void setCheckNoModifiedTemplateOfModifiedTemplate(
			boolean checkNoModifiedTemplateOfModifiedTemplate) {
		this.checkNoModifiedTemplateOfModifiedTemplate = checkNoModifiedTemplateOfModifiedTemplate;
	}

	public boolean isCheckTypesAndValuesModuleContainmentCheck() {
		return checkTypesAndValuesModuleContainmentCheck;
	}

	public void setCheckTypesAndValuesModuleContainmentCheck(
			boolean checkTypesAndValuesModuleContainmentCheck) {
		this.checkTypesAndValuesModuleContainmentCheck = checkTypesAndValuesModuleContainmentCheck;
	}

	public boolean isCheckTypesAndValuesModuleImportsLibNames() {
		return checkTypesAndValuesModuleImportsLibNames;
	}

	public void setCheckTypesAndValuesModuleImportsLibCommon(
			boolean checkTypesAndValuesModuleImportsLib) {
		this.checkTypesAndValuesModuleImportsLibNames = checkTypesAndValuesModuleImportsLib;
	}

	public boolean isCheckLogItemFormat() {
		return checkLogItemFormat;
	}

	public void setCheckLogItemFormat(boolean checkLogItemFormat) {
		this.checkLogItemFormat = checkLogItemFormat;
	}

//	public void setFeatureCodeFormatting(boolean featureCodeFormatting) {
//		this.featureCodeFormatting = featureCodeFormatting;
//	}
//
//	public boolean isFeatureCodeFormatting() {
//		return featureCodeFormatting;
//	}

//	public void setFormattingParameters(
//			TTCN3FormatterParameters formattingParameters) {
//		this.formattingParameters = formattingParameters;
//	}
//
//	public TTCN3FormatterParameters getFormattingParameters() {
//		return formattingParameters;
//	}

	public void setPathFormattedOutputPath(String pathFormattedOutputPath) {
		this.pathFormattedOutputPath = pathFormattedOutputPath;
	}

	public String getPathFormattedOutputPath() {
		return pathFormattedOutputPath;
	}

	public void setCheckNoAllKeywordInPortDefinitions(
			boolean checkNoAllKeywordInPortDefinitions) {
		this.checkNoAllKeywordInPortDefinitions = checkNoAllKeywordInPortDefinitions;
	}

	public boolean isCheckNoAllKeywordInPortDefinitions() {
		return checkNoAllKeywordInPortDefinitions;
	}

	public void setCheckImportsComeFirst(boolean checkImportsComeFirst) {
		this.checkImportsComeFirst = checkImportsComeFirst;
	}

	public boolean isCheckImportsComeFirst() {
		return checkImportsComeFirst;
	}

	public void setCheckLocalDefinitionsComeFirst(boolean checkLocalDefinitionsComeFirst) {
		this.checkLocalDefinitionsComeFirst = checkLocalDefinitionsComeFirst;
	}

	public boolean isCheckLocalDefinitionsComeFirst() {
		return checkLocalDefinitionsComeFirst;
	}

	public void setCheckTypeDefOrderInGroup(boolean checkTypeDefOrderInGroup) {
		this.checkTypeDefOrderInGroup = checkTypeDefOrderInGroup;
	}

	public boolean isCheckTypeDefOrderInGroup() {
		return checkTypeDefOrderInGroup;
	}

	public void setCheckPortMessageGrouping(boolean checkPortMessageGrouping) {
		this.checkPortMessageGrouping = checkPortMessageGrouping;
	}

	public boolean isCheckPortMessageGrouping() {
		return checkPortMessageGrouping;
	}

	public void setCheckTemplatesModuleContainmentCheck(
			boolean checkTemplatesModuleContainmentCheck) {
		this.checkTemplatesModuleContainmentCheck = checkTemplatesModuleContainmentCheck;
	}

	public boolean isCheckTemplatesModuleContainmentCheck() {
		return checkTemplatesModuleContainmentCheck;
	}

	public void setCheckFunctionsModuleContainmentCheck(
			boolean checkFunctionsModuleContainmentCheck) {
		this.checkFunctionsModuleContainmentCheck = checkFunctionsModuleContainmentCheck;
	}

	public boolean isCheckFunctionsModuleContainmentCheck() {
		return checkFunctionsModuleContainmentCheck;
	}

	public void setCheckTestcasesModuleContainmentCheck(
			boolean checkTestcasesModuleContainmentCheck) {
		this.checkTestcasesModuleContainmentCheck = checkTestcasesModuleContainmentCheck;
	}

	public boolean isCheckTestcasesModuleContainmentCheck() {
		return checkTestcasesModuleContainmentCheck;
	}

	public void setCheckModuleParamsModuleContainmentCheck(
			boolean checkModuleParamsModuleContainmentCheck) {
		this.checkModuleParamsModuleContainmentCheck = checkModuleParamsModuleContainmentCheck;
	}

	public boolean isCheckModuleParamsModuleContainmentCheck() {
		return checkModuleParamsModuleContainmentCheck;
	}

	public void setCheckInterfaceModuleContainmentCheck(
			boolean checkInterfaceModuleContainmentCheck) {
		this.checkInterfaceModuleContainmentCheck = checkInterfaceModuleContainmentCheck;
	}

	public boolean isCheckInterfaceModuleContainmentCheck() {
		return checkInterfaceModuleContainmentCheck;
	}

	public void setCheckTestSystemModuleContainmentCheck(
			boolean checkTestSystemModuleContainmentCheck) {
		this.checkTestSystemModuleContainmentCheck = checkTestSystemModuleContainmentCheck;
	}

	public boolean isCheckTestSystemModuleContainmentCheck() {
		return checkTestSystemModuleContainmentCheck;
	}

	public void setCheckTestControlModuleContainmentCheck(
			boolean checkTestControlModuleContainmentCheck) {
		this.checkTestControlModuleContainmentCheck = checkTestControlModuleContainmentCheck;
	}

	public boolean isCheckTestControlModuleContainmentCheck() {
		return checkTestControlModuleContainmentCheck;
	}

	public void setCheckTestcasesModuleImportsLibCommon_Sync(
			boolean checkTestcasesModuleImportsLibCommon_Sync) {
		this.checkTestcasesModuleImportsLibCommon_Sync = checkTestcasesModuleImportsLibCommon_Sync;
	}

	public boolean isCheckTestcasesModuleImportsLibCommon_Sync() {
		return checkTestcasesModuleImportsLibCommon_Sync;
	}

	public void setCheckNamingConventions(boolean checkNamingConventions) {
		this.checkNamingConventions = checkNamingConventions;
	}

	public boolean isCheckNamingConventions() {
		return checkNamingConventions;
	}

	public void setNamingConventionsConfig(
			NamingConventionsConfig namingConventionsConfig) {
		this.namingConventionsConfig = namingConventionsConfig;
	}

	public NamingConventionsConfig getNamingConventionsConfig() {
		return namingConventionsConfig;
	}

	public void setCheckModuleSize(boolean checkModuleSize) {
		this.checkModuleSize = checkModuleSize;
	}

	public boolean isCheckModuleSize() {
		return checkModuleSize;
	}

	public void setMaximumAllowedModuleSizeInBytes(
			int maximumAllowedModuleSizeInBytes) {
		this.maximumAllowedModuleSizeInBytes = maximumAllowedModuleSizeInBytes;
	}

	public int getMaximumAllowedModuleSizeInBytes() {
		return maximumAllowedModuleSizeInBytes;
	}

//	public void setProjectFileExtension(String projectFileExtension) {
//		this.projectFileExtension = projectFileExtension;
//	}
//
//	public String getProjectFileExtension() {
//		return projectFileExtension;
//	}

	public void setCheckNoDuplicatedModuleDefinitionIdentifiers(
			boolean checkNoDuplicatedModuleDefinitionIdentifiers) {
		this.checkNoDuplicatedModuleDefinitionIdentifiers = checkNoDuplicatedModuleDefinitionIdentifiers;
	}

	public boolean isCheckNoDuplicatedModuleDefinitionIdentifiers() {
		return checkNoDuplicatedModuleDefinitionIdentifiers;
	}

	public void setCheckZeroReferencedModuleDefinitions(
			boolean checkZeroReferencedModuleDefinitions) {
		this.checkZeroReferencedModuleDefinitions = checkZeroReferencedModuleDefinitions;
	}

	public boolean isCheckZeroReferencedModuleDefinitions() {
		return checkZeroReferencedModuleDefinitions;
	}

	public void setTypesAndValuesImportsNamesRegExp(String typesAndValuesImportsLibNamesRegExp) {
		this.typesAndValuesImportsLibNamesRegExp = typesAndValuesImportsLibNamesRegExp;
	}

	public String getTypesAndValuesImportsLibNamesRegExp() {
		return typesAndValuesImportsLibNamesRegExp;
	}

	public void setTypesAndValuesImportsLibNamesExcludedRegExp(
			String typesAndValuesImportsLibNamesExcludedRegExp) {
		this.typesAndValuesImportsLibNamesExcludedRegExp = typesAndValuesImportsLibNamesExcludedRegExp;
	}

	public String getTypesAndValuesImportsLibNamesExcludedRegExp() {
		return typesAndValuesImportsLibNamesExcludedRegExp;
	}

	public void setCheckNoTabs(boolean checkNoTabs) {
		this.checkNoTabs = checkNoTabs;
	}

	public boolean isCheckNoTabs() {
		return checkNoTabs;
	}


	public void setCheckLogStatementFormat(boolean checkLogStatementFormat) {
		this.checkLogStatementFormat = checkLogStatementFormat;
	}

	public boolean isCheckLogStatementFormat() {
		return checkLogStatementFormat;
	}

	public void setProcessSubsequentLogStatementsAsOne(
			boolean processSubsequentLogStatementsAsOne) {
		this.processSubsequentLogStatementsAsOne = processSubsequentLogStatementsAsOne;
	}

	public boolean isProcessSubsequentLogStatementsAsOne() {
		return processSubsequentLogStatementsAsOne;
	}

	public void setCheckFunctionsModuleContainmentCheckAllowExtFunction(
			boolean checkFunctionsModuleContainmentCheckAllowExtFunction) {
		this.checkFunctionsModuleContainmentCheckAllowExtFunction = checkFunctionsModuleContainmentCheckAllowExtFunction;
	}

	public boolean isCheckFunctionsModuleContainmentCheckAllowExtFunction() {
		return checkFunctionsModuleContainmentCheckAllowExtFunction;
	}

	public void setMaximumAllowedNestingDepth(int maximumAllowedNestingDepth) {
		this.maximumAllowedNestingDepth = maximumAllowedNestingDepth;
	}

	public int getMaximumAllowedNestingDepth() {
		return maximumAllowedNestingDepth;
	}

	public void setZeroReferencedModuleDefinitionsExcludedRegExp(
			String zeroReferencedModuleDefinitionsExcludedRegExp) {
		this.zeroReferencedModuleDefinitionsExcludedRegExp = zeroReferencedModuleDefinitionsExcludedRegExp;
	}

	public String getZeroReferencedModuleDefinitionsExcludedRegExp() {
		return zeroReferencedModuleDefinitionsExcludedRegExp;
	}

	public void setCheckNoInlineTemplates(boolean checkNoInlineTemplates) {
		this.checkNoInlineTemplates = checkNoInlineTemplates;
	}

	public boolean isCheckNoInlineTemplates() {
		return checkNoInlineTemplates;
	}

	public void setCheckNoOverSpecificRunsOnClauses(
			boolean checkNoOverSpecificRunsOnClauses) {
		this.checkNoOverSpecificRunsOnClauses = checkNoOverSpecificRunsOnClauses;
	}

	public boolean isCheckNoOverSpecificRunsOnClauses() {
		return checkNoOverSpecificRunsOnClauses;
	}

	public void setCheckNoUnusedImports(boolean checkNoUnusedImports) {
		this.checkNoUnusedImports = checkNoUnusedImports;
	}

	public boolean isCheckNoUnusedImports() {
		return checkNoUnusedImports;
	}

	public void setCheckNoUnusedFormalParameters(
			boolean checkNoUnusedFormalParameters) {
		this.checkNoUnusedFormalParameters = checkNoUnusedFormalParameters;
	}

	public boolean isCheckNoUnusedFormalParameters() {
		return checkNoUnusedFormalParameters;
	}

	public void setCheckNoLiterals(boolean checkNoLiterals) {
		this.checkNoLiterals = checkNoLiterals;
	}

	public boolean isCheckNoLiterals() {
		return checkNoLiterals;
	}

	public void setLocalDefinitionTypes(String[] localDefinitionTypes) {
		this.localDefinitionTypes = localDefinitionTypes;
	}

	public String[] getLocalDefinitionTypes() {
		return localDefinitionTypes;
	}

	public void setFeatureListImportedModuleNames(
			boolean featureListImportedModuleNames) {
		this.featureListImportedModuleNames = featureListImportedModuleNames;
	}

	public boolean isFeatureListImportedModuleNames() {
		return featureListImportedModuleNames;
	}

	public void setFeatureListImportedModuleFileNames(
			boolean featureListImportedModuleFileNames) {
		this.featureListImportedModuleFileNames = featureListImportedModuleFileNames;
	}

	public boolean isFeatureListImportedModuleFileNames() {
		return featureListImportedModuleFileNames;
	}

	public void setFeatureListImportingModuleNames(
			boolean featureListImportingModuleNames) {
		this.featureListImportingModuleNames = featureListImportingModuleNames;
	}

	public boolean isFeatureListImportingModuleNames() {
		return featureListImportingModuleNames;
	}

	public void setFeatureListImportingModuleFileNames(
			boolean featureListImportingModuleFileNames) {
		this.featureListImportingModuleFileNames = featureListImportingModuleFileNames;
	}

	public boolean isFeatureListImportingModuleFileNames() {
		return featureListImportingModuleFileNames;
	}

	public void setCheckNoUnusedLocalDefinitions(
			boolean checkNoUnusedLocalDefinitions) {
		this.checkNoUnusedLocalDefinitions = checkNoUnusedLocalDefinitions;
	}

	public boolean isCheckNoUnusedLocalDefinitions() {
		return checkNoUnusedLocalDefinitions;
	}

	public void setRecursionInCheckNoOverSpecificRunsOnClauses(boolean recursionInCheckNoOverSpecificRunsOnClauses) {
		this.recursionInCheckNoOverSpecificRunsOnClauses = recursionInCheckNoOverSpecificRunsOnClauses;
	}

	public boolean isRecursionInCheckNoOverSpecificRunsOnClauses() {
		return recursionInCheckNoOverSpecificRunsOnClauses;
	}

	public boolean isCheckLevelOfNestedCalls() {
		return checkLevelOfNestedCalls;
	}

	public void setCheckLevelOfNestedCalls(boolean checkLevelOfNestedCalls) {
		this.checkLevelOfNestedCalls = checkLevelOfNestedCalls;
	}

	public int getMaxLevelOfNestedCalls() {
		return maxLevelOfNestedCalls;
	}

	public void setMaxLevelOfNestedCalls(int maxLevelOfNestedCalls) {
		this.maxLevelOfNestedCalls = maxLevelOfNestedCalls;
	}

	public boolean isCheckInlineTemplates() {
		return checkInlineTemplates;
	}

	public void setCheckInlineTemplates(boolean checkInlineTemplates) {
		this.checkInlineTemplates = checkInlineTemplates;
	}

	public boolean isCheckListedVariableDeclarations() {
		return checkListedVariableDeclarations;
	}

	public void setCheckListedVariableDeclarations(boolean checkListedVariableDeclarations) {
		this.checkListedVariableDeclarations = checkListedVariableDeclarations;
	}

	public void setCheckTypesAndValuesModuleImportsLibNames(boolean checkTypesAndValuesModuleImportsLibNames) {
		this.checkTypesAndValuesModuleImportsLibNames = checkTypesAndValuesModuleImportsLibNames;
	}

	public void setTypesAndValuesImportsLibNamesRegExp(String typesAndValuesImportsLibNamesRegExp) {
		this.typesAndValuesImportsLibNamesRegExp = typesAndValuesImportsLibNamesRegExp;
	}

	public boolean isCheckNoUninitialisedVariables() {
		return checkNoUninitialisedVariables;
	}

	public void setCheckNoUninitialisedVariables(boolean checkNoUninitialisedVariables) {
		this.checkNoUninitialisedVariables = checkNoUninitialisedVariables;
	}

	public String getDependencyOutputPath() {
		return dependencyOutputPath;
	}

	public void setDependencyOutputPath(String dependencyOutputPath) {
		this.dependencyOutputPath = dependencyOutputPath;
	}

	public String[] getCheckNoUninitialisedVariablesExclude() {
		return checkNoUninitialisedVariablesExclude;
	}

	public void setCheckNoUninitialisedVariablesExclude(String[] checkNoUninitialisedVariablesExclude) {
		this.checkNoUninitialisedVariablesExclude = checkNoUninitialisedVariablesExclude;
	}

	public boolean isAliasInCheckNoOverSpecificRunsOnClauses() {
		return aliasInCheckNoOverSpecificRunsOnClauses;
	}

	public void setAliasInCheckNoOverSpecificRunsOnClauses(boolean aliasInCheckNoOverSpecificRunsOnClauses) {
		this.aliasInCheckNoOverSpecificRunsOnClauses = aliasInCheckNoOverSpecificRunsOnClauses;
	}

	

}
