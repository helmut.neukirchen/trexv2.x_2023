package de.ugoe.cs.swe.common;

import de.ugoe.cs.swe.common.logging.LoggingConfiguration;

public abstract class ConfigurationProfile {

	protected String profileName;
	protected String profileVersion;
	private String ignoredResourceRegExp = ".*IGNORED.*";
	protected boolean settingAbortOnError = true;
	protected LoggingConfiguration loggingConfiguration = new LoggingConfiguration();
	protected boolean statShowSummary = true;
	protected boolean statShowLOC = true;
	protected boolean generateXMI = false;

	public ConfigurationProfile() {
		super();
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public void setStatShowLOC(boolean statShowLOC) {
		this.statShowLOC = statShowLOC;
	}

	public boolean isStatShowLOC() {
		return statShowLOC;
	}

	public void setSettingAbortOnError(boolean settingAbortOnError) {
		this.settingAbortOnError = settingAbortOnError;
	}

	public boolean isSettingAbortOnError() {
		return settingAbortOnError;
	}

	public void setProfileVersion(String profileVersion) {
		this.profileVersion = profileVersion;
	}

	public String getProfileVersion() {
		return profileVersion;
	}

	public void setLoggingConfiguration(LoggingConfiguration loggingConfiguration) {
		this.loggingConfiguration = loggingConfiguration;
	}

	public LoggingConfiguration getLoggingConfiguration() {
		return loggingConfiguration;
	}

	public void setStatShowSummary(boolean statShowSummary) {
		this.statShowSummary = statShowSummary;
	}

	public boolean isStatShowSummary() {
		return statShowSummary;
	}

	public boolean isGenerateXMI() {
		return generateXMI;
	}

	public void setGenerateXMI(boolean generateXMI) {
		this.generateXMI = generateXMI;
	}

	public String getIgnoredResourceRegExp() {
		return ignoredResourceRegExp;
	}

	public void setIgnoredResourceRegExp(String ignoredResourceRegExp) {
		this.ignoredResourceRegExp = ignoredResourceRegExp;
	}
}