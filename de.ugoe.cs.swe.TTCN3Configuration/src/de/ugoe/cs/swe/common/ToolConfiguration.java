package de.ugoe.cs.swe.common;

import java.util.ArrayList;

public abstract class ToolConfiguration {

	private ArrayList<ConfigurationProfile> ConfigurationProfiles;
	private String defaultConfigurationProfile;

	public ToolConfiguration() {
		super();
	}

	public ArrayList<ConfigurationProfile> getConfigurationProfiles() {
		return ConfigurationProfiles;
	}

	public void setConfigurationProfiles(ArrayList<ConfigurationProfile> configurationProfiles) {
		this.ConfigurationProfiles = configurationProfiles;
	}

	public String getDefaultConfigurationProfile() {
		return defaultConfigurationProfile;
	}

	public void setDefaultConfigurationProfile(String defaultConfigurationProfile) {
		this.defaultConfigurationProfile = defaultConfigurationProfile;
	}

}